package stdlib;

import java.util.Iterator;
import java.util.NoSuchElementException;

public interface IDeque<T> extends Iterable<T> {

    boolean isEmpty();
    int size();
    void pushLeft(T item);
    void pushRight(T item);
    T popLeft();
    T popRight();

}
