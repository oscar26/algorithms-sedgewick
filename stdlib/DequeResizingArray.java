package stdlib;

import java.util.Iterator;
import java.util.NoSuchElementException;
import stdlib.IDeque;

class DequeResizingArray<Item> implements IDeque<Item> {

    private Item[] items;
    private int n;
    private int left;

    public DequeResizingArray() {
        this.items = (Item[]) new Object[1];
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private int right() {
        return (left + n - 1) % items.length;
    }

    private void resize(int cap) {
        Item[] newArray = (Item[]) new Object[cap];
        int i = left;
        for (int j = 0; j < size(); j++) {
            newArray[j] = items[i];
            i = (i + 1) % items.length;
        }
        items = newArray;
        left = 0;
    }

    public void pushLeft(Item item) {
        n++;
        left--;
        if (left == -1)
            left = items.length - 1;
        items[left] = item;
        if (size() == items.length)
            resize(2*items.length);
    }

    public void pushRight(Item item) {
        n++;
        items[right()] = item;
        if (size() == items.length)
            resize(2*items.length);
    }

    public Item popLeft() {
        if (isEmpty())
            throw new NoSuchElementException("deque is empty");
        Item item = items[left];
        items[left] = null;
        left = (left + 1) % items.length;
        n--;
        if (size() > 0 && size() == items.length/4)
            resize(items.length/2);
        return item;
    }

    public Item popRight() {
        if (isEmpty())
            throw new NoSuchElementException("deque is empty");
        Item item = items[right()];
        items[right()] = null;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new DequeResizingArrayIterator();
    }

    private class DequeResizingArrayIterator implements Iterator<Item> {

        private int current = left;  // or current = right;
        private int counter = 0;

        public boolean hasNext() {
            return size() > 0 && counter < size();
        }

        public Item next() {
            Item item = items[current];
            current = (current + 1) % items.length;
            // if starting from the right, then we do this instead
            //     current--;
            //     if (current == -1) current = items.length - 1;
            counter++;
            return item;
        }

        public void remove() {}

    }

    private static class Tests {

        static void testPushLeft(IDeque<Integer> deque) {
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushLeft(4);
            for (int item : deque)
                assert item == 4;
            assert deque.size() == 1;
            assert !deque.isEmpty();

            for (int i = 3; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();

            int counter = 0;
            for (int item : deque) {
                assert item == counter : String.format("Expected: %d, got: %d", counter, item);
                counter++;
            }
            assert counter == 5;
        }

        static void testPushRight(IDeque<Integer> deque) {
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushRight(4);
            for (int item : deque)
                assert item == 4;
            assert deque.size() == 1;
            assert !deque.isEmpty();

            for (int i = 3; i >= 0; i--)
                deque.pushRight(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();

            int counter = 4;
            for (int item : deque) {
                assert item == counter : String.format("Expected: %d, got: %d", counter, item);
                counter--;
            }
            assert counter == -1;
        }

        static void testPopLeft(IDeque<Integer> deque) {
            assert deque.size() == 0;
            boolean exceptionThrown = false;
            try {
                deque.popLeft();
            } catch (NoSuchElementException e) {
                exceptionThrown = true;
            }
            assert exceptionThrown;

            for (int i = 4; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();
            for (int i = 0; i < 5; i++) {
                int item = deque.popLeft();
                assert item == i : String.format("Expected: %d, got: %d", i, item);
            }
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushRight(1);
            assert deque.popLeft() == 1;
            assert deque.size() == 0;
            assert deque.isEmpty();
        }

        static void testPopRight(IDeque<Integer> deque) {
            assert deque.size() == 0;
            boolean exceptionThrown = false;
            try {
                deque.popRight();
            } catch (NoSuchElementException e) {
                exceptionThrown = true;
            }
            assert exceptionThrown;

            for (int i = 4; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();
            for (int i = 4; i >= 0; i--) {
                int item = deque.popRight();
                assert item == i : String.format("Expected: %d, got: %d", i, item);
            }
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushLeft(1);
            assert deque.popRight() == 1;
            assert deque.size() == 0;
            assert deque.isEmpty();
        }

    }

    public static void main(String[] args) {
        Tests.testPushLeft(new DequeResizingArray<Integer>());
        Tests.testPushRight(new DequeResizingArray<Integer>());
        Tests.testPopLeft(new DequeResizingArray<Integer>());
        Tests.testPopRight(new DequeResizingArray<Integer>());
    }

}
