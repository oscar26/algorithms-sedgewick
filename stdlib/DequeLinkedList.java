package stdlib;

import java.util.Iterator;
import java.util.NoSuchElementException;
import stdlib.IDeque;

public class DequeLinkedList<Item> implements IDeque<Item> {

    private int n;
    private DoubleNode<Item> left;
    private DoubleNode<Item> right;

    private class DoubleNode<Item> {

        Item item;
        DoubleNode previous;
        DoubleNode next;

        public DoubleNode(Item item) {
            this.item = item;
        }
    }

    public boolean isEmpty() {
        // Either one of these conditions suffices, but we check all of them
        // for testing purposes
        return n == 0 && left == null && right == null;
    }

    public int size() {
        return n;
    }

    public void pushLeft(Item item) {
        DoubleNode newNode = new DoubleNode(item);
        if (isEmpty()) {
            left = newNode;
            right = newNode;
        } else {
            newNode.next = left;
            left.previous = newNode;
            left = newNode;
        }
        n++;
    }

    public void pushRight(Item item) {
        DoubleNode newNode = new DoubleNode(item);
        if (isEmpty()) {
            left = newNode;
            right = newNode;
        } else {
            newNode.previous = right;
            right.next = newNode;
            right = newNode;
        }
        n++;
    }

    public Item popLeft() {
        if (isEmpty())
            throw new NoSuchElementException("deque is empty");
        Item item = left.item;
        left = left.next;
        if (left == null)
            right = null;
        n--;
        return item;
    }

    public Item popRight() {
        if (isEmpty())
            throw new NoSuchElementException("deque is empty");
        Item item = right.item;
        right = right.previous;
        if (right == null)
            left = null;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new DequeLinkedListIterator();
    }

    private class DequeLinkedListIterator implements Iterator<Item> {

        private DoubleNode<Item> current = left;  // or right

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;  // or current.previous if starting
                                     // with current = right
            return item;
        }

        public void remove() {}

    }

    private static class Tests {
        static void testPushLeft(IDeque<Integer> deque) {
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushLeft(4);
            for (int item : deque)
                assert item == 4;
            assert deque.size() == 1;
            assert !deque.isEmpty();

            for (int i = 3; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();

            int counter = 0;
            for (int item : deque) {
                assert item == counter : String.format("Expected: %d, got: %d", counter, item);
                counter++;
            }
            assert counter == 5;
        }

        static void testPushRight(IDeque<Integer> deque) {
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushRight(4);
            for (int item : deque)
                assert item == 4;
            assert deque.size() == 1;
            assert !deque.isEmpty();

            for (int i = 3; i >= 0; i--)
                deque.pushRight(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();

            int counter = 4;
            for (int item : deque) {
                assert item == counter : String.format("Expected: %d, got: %d", counter, item);
                counter--;
            }
            assert counter == -1;
        }

        static void testPopLeft(IDeque<Integer> deque) {
            assert deque.size() == 0;
            boolean exceptionThrown = false;
            try {
                deque.popLeft();
            } catch (NoSuchElementException e) {
                exceptionThrown = true;
            }
            assert exceptionThrown;

            for (int i = 4; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();
            for (int i = 0; i < 5; i++) {
                int item = deque.popLeft();
                assert item == i : String.format("Expected: %d, got: %d", i, item);
            }
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushRight(1);
            assert deque.popLeft() == 1;
            assert deque.size() == 0;
            assert deque.isEmpty();
        }

        static void testPopRight(IDeque<Integer> deque) {
            assert deque.size() == 0;
            boolean exceptionThrown = false;
            try {
                deque.popRight();
            } catch (NoSuchElementException e) {
                exceptionThrown = true;
            }
            assert exceptionThrown;

            for (int i = 4; i >= 0; i--)
                deque.pushLeft(i);
            assert deque.size() == 5;
            assert !deque.isEmpty();
            for (int i = 4; i >= 0; i--) {
                int item = deque.popRight();
                assert item == i : String.format("Expected: %d, got: %d", i, item);
            }
            assert deque.size() == 0;
            assert deque.isEmpty();

            deque.pushLeft(1);
            assert deque.popRight() == 1;
            assert deque.size() == 0;
            assert deque.isEmpty();
        }
    }

    public static void main(String[] args) {
        Tests.testPushLeft(new DequeLinkedList<Integer>());
        Tests.testPushRight(new DequeLinkedList<Integer>());
        Tests.testPopLeft(new DequeLinkedList<Integer>());
        Tests.testPopRight(new DequeLinkedList<Integer>());
    }

}
