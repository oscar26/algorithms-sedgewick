# Script used to compile and execute Java programs. Must be put alongside the
# "algs4.jar" file. Assumes there's a class called Main.

classname="Main"
libflag="-cp .:$(dirname "$0")/algs4.jar"
source_code=$1  # Store source file before shifting the arguments
shift
# Some programs also expect redirections from stdin, these are inherited by any
# program that required it.
javac $libflag $source_code && java -enableassertions $libflag:$(dirname "$source_code") $classname "$@"
