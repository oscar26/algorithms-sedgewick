/*
 * class TwoStacks<Item> implements Iterable<Item>
 *
 *         int  size1()
 *         int  size2()
 *     boolean  isEmpty1()
 *     boolean  isEmpty2()
 *        void  push1()
 *        void  push2()
 *        Item  pop1()
 *        Item  pop2()
 *
 * */

import java.util.Iterator;
import stdlib.DequeLinkedList;

class TwoStacks<Item> implements Iterable<Item> {

    private DequeLinkedList<Item> deque;
    private int n1;
    private int n2;

    public TwoStacks() {
        deque = new DequeLinkedList<>();
    }

    public int size1() {
        return n1;
    }

    public int size2() {
        return n2;
    }

    public boolean isEmpty1() {
        return n1 == 0;
    }

    public boolean isEmpty2() {
        return n2 == 0;
    }

    public void push1(Item item) {
        deque.pushLeft(item);
        n1++;
    }

    public void push2(Item item) {
        deque.pushRight(item);
        n2++;
    }

    public Item pop1() {
        Item item = deque.popLeft();
        n1--;
        return item;
    }

    public Item pop2() {
        Item item = deque.popRight();
        n2--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new TwoStacksIterator();
    }

    private class TwoStacksIterator implements Iterator<Item> {

        private Iterator<Item> dequeIterator;

        public TwoStacksIterator() {
            dequeIterator = deque.iterator();
        }

        public boolean hasNext() {
            return dequeIterator.hasNext();
        }

        public Item next() {
            return dequeIterator.next();
        }

        public void remove() {}

    }

}

class Main {
    public static void main(String[] args) {
        TwoStacks<Integer> twoStacks = new TwoStacks<>();
        assert twoStacks.size1() == 0;
        assert twoStacks.size2() == 0;
        assert twoStacks.isEmpty1();
        assert twoStacks.isEmpty2();
        for (int i = 0; i < 5; i++)
            twoStacks.push1(i);
        for (int i = -1; i > -7; i--)
            twoStacks.push2(i);
        assert twoStacks.size1() == 5;
        assert twoStacks.size2() == 6;
        assert !twoStacks.isEmpty1();
        assert !twoStacks.isEmpty2();
        // Second stack is printed in reverse but it doesn't matter as there's
        // no API that specifies in which order the TwoStack must be traversed
        int index = 4;
        for (int item : twoStacks)
            assert item == index--;
        assert index == -7;
        index = 4;
        while (!twoStacks.isEmpty1())
            assert twoStacks.pop1() == index--;
        assert index == -1;
        index = -6;
        while (!twoStacks.isEmpty2())
            assert twoStacks.pop2() == index++;
        assert index == 0;
    }
}
