/* At first I thought that there exists a number N of stacks such that the
 * operations of a queue are constant with respect the number of stack
 * operations and queue elements. But after looking in the book site, there's a
 * similar exercise whose solution uses two stacks and it just swaps the
 * elements between them to correct for the reversal of the stack, but still
 * loops through all elements. The cost of the dequeue operation is amortized
 * over the number of stacks defined.
 *
 * The solution provided here is a generalization of the aforementioned
 * solution. It amortizes the operations over a number N > 1 of stacks
 * specified at initialization and an additional one used as swap. Of course,
 * it is not a true constant complexity solution.
 *
 * The discussion here in [1,2] points to a potential constant-time solution in
 * a paper that uses 6 stacks, albeit the math is way over my head for now.
 *
 * [1] https://stackoverflow.com/questions/5538192/how-to-implement-a-queue-with-three-stacks/
 * [2] https://cstheory.stackexchange.com/questions/5886/how-many-ephemeral-stacks-are-required-to-simulate-a-queue-in-worst-case-o1
 *
 * Note: this surely has a high degree of difficulty.
 *
 * */

import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.Stack;

class StackedQueue<Item> {

    private Stack<Item>[] stacks;
    private Stack<Item> swapStack;
    private int firstItemStack;
    private int nextInsertStack;

    public StackedQueue(int numStacks) {
        swapStack = new Stack<Item>();
        stacks = new Stack[numStacks];
        for (int i = 0; i < numStacks; i++)
            stacks[i] = new Stack<Item>();
    }

    public boolean isEmpty() {
        for (Stack<Item> stack : stacks)
            if (!stack.isEmpty())
                return false;
        return true;
    }

    public int size() {
        int totalSize = 0;
        for (Stack<Item> stack : stacks)
            totalSize += stack.size();
        return totalSize;
    }

    public void enqueue(Item item) {
        stacks[nextInsertStack].push(item);
        nextInsertStack = (nextInsertStack + 1) % stacks.length;
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("queue is empty");
        while (!stacks[firstItemStack].isEmpty())
            swapStack.push(stacks[firstItemStack].pop());
        Item item = swapStack.pop();
        while (!swapStack.isEmpty())
            stacks[firstItemStack].push(swapStack.pop());
        firstItemStack = (firstItemStack + 1) % stacks.length;
        return item;
    }

    public Item peek() {
        if (isEmpty())
            throw new NoSuchElementException("queue is empty");
        while (!stacks[firstItemStack].isEmpty())
            swapStack.push(stacks[firstItemStack].pop());
        Item item = swapStack.peek();
        while (!swapStack.isEmpty())
            stacks[firstItemStack].push(swapStack.pop());
        return item;
    }

}

class Main {
    public static void main(String[] args) {
        int nStacks = 10;
        int n = 10;
        for (int i = 2; i <= nStacks; i++) {
            System.out.print("Testing with " + i + " stacks...");
            StackedQueue<Integer> queue = new StackedQueue<>(i);
            for (int j = 0; j < n; j++)
                queue.enqueue(j);
            for (int j = 0; j < n; j++)
                assert j == queue.dequeue();
            System.out.println(" OK");
        }
    }
}
