/*
 * Output would be:
 *
 *     was
 *     best
 *     times
 *     of
 *     the
 *     was
 *     the
 *     it
 *
 * */

class FixedCapacityStackOfStrings {

    private String[] a;
    private int N;

    public FixedCapacityStackOfStrings(int cap) {
        a = new String[cap];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public boolean isFull() {
        return N == a.length;
    }

    public int size() {
        return N;
    }

    public void push(String item) {
        a[N++] = item;
    }

    public String pop() {
        return a[--N];
    }

}

class Main {
    public static void main(String[] args) {
        FixedCapacityStackOfStrings stack;
        stack = new FixedCapacityStackOfStrings(20);
        String message = "it was - the best - of times - - - it was - the - -";
        for (String token : message.split(" ")) {
            if (token.equals("-"))
                System.out.println(stack.pop());
            else
                stack.push(token);
        }
    }
}

