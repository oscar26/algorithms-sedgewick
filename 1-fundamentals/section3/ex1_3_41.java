import java.util.Iterator;
import java.util.NoSuchElementException;

class Queue<Item> implements Iterable<Item> {

    private int n;
    private Node first;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public Queue() {}

    public Queue(Queue<Item> original) {
        Queue<Item> copy = new Queue<Item>();
        for (Item item : original)
            copy.enqueue(item);
        first = copy.first;
        n = copy.n;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return n;
    }

    public void enqueue(Item item) {
        Node newNode = new Node(item);
        if (isEmpty()) {
            first = newNode;
        } else {
            Node current = first;
            while (current.next != null)
                current = current.next;
            current.next = newNode;
        }
        n++;
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("queue is empty");
        Item item = first.item;
        first = first.next;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new QueueIterator();
    }

    private class QueueIterator implements Iterator<Item> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}

    }
}

class Main {

    public static void main(String[] args) {
        int n = 10;
        Queue<String> original = new Queue<>();
        assert original.isEmpty();
        for (int i = 0; i < n; i++)
            original.enqueue(String.valueOf(i));
        assert !original.isEmpty();
        assert original.size() == n;
        Queue<String> copy = new Queue<>(original);
        assert !copy.isEmpty();
        assert copy.size() == n;
        assert original != copy;
        for (int i = 0; i < n; i++) {
            String originalStr = original.dequeue();
            assert original.size() == copy.size() - 1;
            String copyStr = copy.dequeue();
            assert original.size() == copy.size();
            assert originalStr.equals(copyStr);
            assert originalStr.equals(String.valueOf(i));
        }
        assert original.isEmpty();
        assert copy.isEmpty();
    }
}
