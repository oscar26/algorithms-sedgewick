class FixedCapacityStackOfStrings {

    private String[] a;
    private int N;

    public FixedCapacityStackOfStrings(int cap) {
        a = new String[cap];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public boolean isFull() {
        return N == a.length;
    }

    public int size() {
        return N;
    }

    public void push(String item) {
        a[N++] = item;
    }

    public String pop() {
        return a[--N];
    }

}

class Main {
    public static void main(String[] args) {
        FixedCapacityStackOfStrings stringsStack;
        stringsStack = new FixedCapacityStackOfStrings(5);
        assert stringsStack.isEmpty();
        stringsStack.push("1");
        stringsStack.push("2");
        stringsStack.push("3");
        stringsStack.push("4");
        stringsStack.push("5");
        assert stringsStack.isFull();
        System.out.println("Stack size: " + stringsStack.size());
    }
}
