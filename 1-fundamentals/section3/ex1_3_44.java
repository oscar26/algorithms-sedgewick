import edu.princeton.cs.algs4.Stack;

class Buffer {

    private Stack<Character> leftBuffer;
    private Stack<Character> rightBuffer;

    public Buffer() {
        leftBuffer = new Stack<>();
        rightBuffer = new Stack<>();
    }

    public int size() {
        return leftBuffer.size() + rightBuffer.size();
    }

    public void insert(char c) {
        leftBuffer.push(c);
    }

    public char delete() {
        // Delete characters to the left of the cursor first
        if (!leftBuffer.isEmpty())
            return leftBuffer.pop();
        if (!rightBuffer.isEmpty())
            return rightBuffer.pop();
        throw new RuntimeException("buffer is empty");
    }

    public void left(int k) {
        if (k <= 0)
            throw new IllegalArgumentException("k must be greater than 0");
        int i = 0;
        while (!leftBuffer.isEmpty() && i < k) {
            rightBuffer.push(leftBuffer.pop());
            i++;
        }
    }

    public void right(int k) {
        if (k <= 0)
            throw new IllegalArgumentException("k must be greater than 0");
        int i = 0;
        while (!rightBuffer.isEmpty() && i < k) {
            leftBuffer.push(rightBuffer.pop());
            i++;
        }
    }

}

class Main {
    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        assert buffer.size() == 0;
        String typing = "This is a simulated typed message.";
        for (char c : typing.toCharArray())
            buffer.insert(c);
        assert buffer.size() == typing.length();
        char currentChar = buffer.delete();
        assert currentChar == '.';
        assert buffer.size() == typing.length() - 1;
        buffer.left(1000);
        currentChar = buffer.delete();
        assert currentChar == 'T';
        assert buffer.size() == typing.length() - 2;
        buffer.insert(currentChar);
        buffer.right(3);
        currentChar = buffer.delete();
        assert currentChar == 's';
        assert buffer.size() == typing.length() - 2;
        buffer.insert(currentChar);
        buffer.right(buffer.size());
        buffer.insert('.');
        buffer.left(buffer.size());
        StringBuilder rebuiltMessage = new StringBuilder();
        while (buffer.size() > 0)
            rebuiltMessage.append(buffer.delete());
        assert typing.equals(rebuiltMessage.toString());
        System.out.println("Original message: " + typing);
        System.out.println("Rebuilt message: " + rebuiltMessage);
    }
}
