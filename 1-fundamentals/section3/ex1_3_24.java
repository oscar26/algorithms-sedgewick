class Node {
    String item;
    Node next;
}

class LinkedList {

    private Node first;

    public Node append(String item) {
        Node newNode = new Node();
        newNode.item = item;
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
        return newNode;
    }

    public Node removeAfter(Node node) {
        if (node == null || first == null)
            return null;
        Node currentNode = first;
        while (currentNode.next != null && currentNode != node)
            currentNode = currentNode.next;
        if (currentNode.next == null)
            return null;
        Node removedNode = currentNode.next;
        currentNode.next = currentNode.next.next;
        return removedNode;
    }

}

class Main {
    static void test1() {
        Node node1, node2, node3;
        LinkedList list = new LinkedList();
        node1 = list.append("1");
        node2 = list.append("2");
        node3 = list.append("3");
        assert list.removeAfter(node1).item.equals("2");
    }

    static void test2() {
        Node node1, node2, node3;
        LinkedList list = new LinkedList();
        node1 = list.append("1");
        node2 = list.append("2");
        node3 = list.append("3");
        assert list.removeAfter(node2).item.equals("3");
    }

    static void test3() {
        Node node1, node2, node3;
        LinkedList list = new LinkedList();
        node1 = list.append("1");
        node2 = list.append("2");
        node3 = list.append("3");
        assert list.removeAfter(node3) == null;
    }

    static void test4() {
        Node node = new Node();
        LinkedList list = new LinkedList();
        assert list.removeAfter(node) == null;
    }

    static void test5() {
        LinkedList list = new LinkedList();
        assert list.removeAfter(null) == null;
    }

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
    }
}
