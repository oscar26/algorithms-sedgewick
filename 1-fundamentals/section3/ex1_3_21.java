class LinkedList {

    private Node first;

    private class Node {
        String item;
        Node next;
    }

    public void append(String item) {
        Node newNode = new Node();
        newNode.item = item;
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
    }

    public boolean find(String key) {
        if (first == null)
            return false;
        Node currentNode = first;
        while (currentNode.next != null && !currentNode.item.equals(key))
            currentNode = currentNode.next;
        if (currentNode.item.equals(key))
            return true;
        return false;
    }

}

class Main {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        System.out.println(list.find("1"));
        list.append("1");
        list.append("2");
        list.append("3");
        System.out.println(list.find("1"));
        System.out.println(list.find("2"));
        System.out.println(list.find("3"));
        System.out.println(list.find("4"));
    }
}
