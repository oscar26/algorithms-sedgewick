import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdRandom;

class RandomQueue<Item> {

    private Item[] items;
    private int n;

    public RandomQueue() {
        items = (Item[]) new Object[1];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    private void resize(int cap) {
        Item[] newArray = (Item[]) new Object[cap];
        for (int i = 0; i < n; i++)
            newArray[i] = items[i];
        items = newArray;
    }

    public void enqueue(Item item) {
        items[n++] = item;
        if (n == items.length)
            resize(2*items.length);
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("queue is empty");
        int chosen = StdRandom.uniform(n);
        Item item = items[chosen];
        items[chosen] = items[n-1];
        items[--n] = null;
        if (n > 0 && n == items.length/4)
            resize(items.length/2);
        return item;
    }

    public Item sample() {
        return items[StdRandom.uniform(n)];
    }

}

class Card {

    private static String[] CARDS = {
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "J",
        "Q",
        "K",
        "A"
    };
    private static String[] SUITS = {"Diamonds", "Clubs", "Hearts", "Spades"};
    private String card_name;
    private String suit;

    public Card(String card_name, String suit) {
        this.card_name = card_name;
        this.suit = suit;
    }

    public String toString() {
        return card_name + " of " + suit;
    }

    public static Card[][] generateHands() {
        RandomQueue<Card> cards = new RandomQueue<>();
        for (String suit_name : SUITS)
            for (String card : CARDS)
                cards.enqueue(new Card(card, suit_name));
        assert cards.size() == 52;
        Card[][] hands = new Card[4][13];
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 13; j++)
                hands[i][j] = cards.dequeue();
        assert cards.size() == 0;
        return hands;
    }

}

class Main {
    static void testDequeue(int trials, int numbers) {
        RandomQueue<Integer> randomQueue = new RandomQueue<>();
        int[] frequencies = new int[numbers];
        for (int i = 0; i < trials; i++) {
            // Empty the queue before picking one number
            while (!randomQueue.isEmpty())
                randomQueue.dequeue();
            for (int j = 0; j < numbers; j++)
                randomQueue.enqueue(j);
            frequencies[randomQueue.dequeue()]++;
        }
        System.out.println("Checking dequeue() frequencies...");
        verifyFrequencies(trials, frequencies);
    }

    static void testSample(int trials, int numbers) {
        RandomQueue<Integer> randomQueue = new RandomQueue<>();
        for (int i = 0; i < numbers; i++)
            randomQueue.enqueue(i);
        int[] frequencies = new int[numbers];
        for (int i = 0; i < trials; i++)
            frequencies[randomQueue.sample()]++;
        System.out.println("Checking sample() frequencies...");
        verifyFrequencies(trials, frequencies);
    }

    static void verifyFrequencies(int trials, int[] frequencies) {
        double expectedProbability = 1.0/frequencies.length;
        for (int i = 0; i < frequencies.length; i++) {
            double empiricalProbabilty = 1.0*frequencies[i]/trials;
            System.out.printf("Number %d found in position %d: %.2f%% of the times\n",
                i, i, empiricalProbabilty*100);
            assert Math.abs(expectedProbability - empiricalProbabilty) < 1e-2;
        }
    }

    static void dealHands() {
        Card[][] hands = Card.generateHands();
        for (int i = 0; i < hands.length; i++) {
            System.out.println("\nHand for player #" + (i+1));
            for (int j = 0; j < hands[0].length; j++)
                System.out.println(hands[i][j]);
        }
    }

    public static void main(String[] args) {
        int trials = 10000;
        int numbers = 10;
        testDequeue(trials, numbers);
        testSample(trials, numbers);
        dealHands();
    }
}
