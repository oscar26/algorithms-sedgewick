import java.io.File;
import edu.princeton.cs.algs4.Queue;

class Main {
    static void listFiles(File startingFile, int depth) {
        Queue<File> files = new Queue<>();
        for (String name : startingFile.list())
            files.enqueue(new File(startingFile.getAbsoluteFile() + "/" + name));
        while (!files.isEmpty()) {
            File file = files.dequeue();
            String indentation = "";
            for (int i = 0; i < depth; i++)
                indentation += "  ";
            if (file.isDirectory()) {
                System.out.println(indentation + file.getName() + "/");
                listFiles(file, depth+1);
            } else if (file.isFile()) {
                System.out.println(indentation + file.getName());
            }
        }
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass directory path");
        listFiles(new File(args[0]), 0);
    }
}
