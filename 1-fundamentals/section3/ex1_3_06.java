/*
 * The queue is reversed.
 * */

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;

class Main {
    public static void main(String[] args) {
        Queue<String> q = new Queue<String>();
        q.enqueue("1");
        q.enqueue("2");
        q.enqueue("3");
        q.enqueue("4");
        q.enqueue("5");
        Stack<String> stack = new Stack<String>();
        while (!q.isEmpty())
            stack.push(q.dequeue());
        while (!stack.isEmpty())
            q.enqueue(stack.pop());
        for (String item : q)
            System.out.println(item);
    }
}
