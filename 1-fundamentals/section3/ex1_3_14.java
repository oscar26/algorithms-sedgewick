/*
 * This queue implementation uses a circular array to minimize unnecessary
 * resizes and copies.
 * */

class ResizingArrayQueueOfStrings {

    private String[] a;
    private int n;
    private int first;

    public ResizingArrayQueueOfStrings() {
        a = new String[1];
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private int last() {
        return (first + n - 1) % a.length;
    }

    private void resize(int cap) {
        String[] newArray = new String[cap];
        int i = first;
        for (int j = 0; j < n; j++) {
            newArray[j] = a[i];
            i = (i + 1) % a.length;
        }
        a = newArray;
        first = 0;
    }

    public void enqueue(String item) {
        n++;
        a[last()] = item;
        if (size() == a.length)
            resize(2*a.length);
    }

    public String dequeue() {
        String item = a[first];
        a[first] = null;
        first = (first + 1) % a.length;
        n--;
        if (size() > 0 && size() == a.length/4)
            resize(a.length/2);
        return item;
    }

}

class Main {
    public static void main(String[] args) {
        ResizingArrayQueueOfStrings queue = new ResizingArrayQueueOfStrings();
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        queue.enqueue("5");
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println();
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        queue.enqueue("5");
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
    }
}
