/*
 * The solution assumes that tokens are separated by spaces. Otherwise, a
 * tokenizer must be implemented.
 *
 * Cases (only binary subexpressions are allowed):
 *
 *     1 + 2                       1 2 +
 *     1 + ( 2 * 3 )               1 2 3 * +
 *     ( 2 * 3 ) + 1               2 3 * 1 +
 *     ( ( 1 + 2 ) )               1 2 +
 *     ( 1 + ( 2 - ( 3 * 4 ) ) )   1 2 3 4 * - +
 *
 * Note that the relative order of the operands don't change, so when we see an
 * operand we leave it as it. The operators must be appended by nesting level
 * where the outer-most/highest operator must be last, and this is signaled by
 * the parentheses, more specifically the right/closing parenthesis.
 *
 * */

import edu.princeton.cs.algs4.Stack;

class Main {
    static boolean isOperator(String token) {
        return token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/");
    }

    static String infixToPostfix(String expression) {
        // In case the expression has no parentheses, add a pair to trigger the
        // conditional within the loop
        expression = "( " + expression + " )";
        String postfix = "";
        Stack<String> operators = new Stack<>();
        for (String token : expression.split("\\s")) {
            if (token.equals(")")) {
                if (operators.size() > 0)  // this filters out redundant parentheses
                    postfix += " " + operators.pop();
            } else if (isOperator(token)) {
                operators.push(token);
            } else if (!token.equals("(")) {
                postfix += " " + token;
            }
        }
        return postfix.trim();
    }

    public static void main(String[] args) {
        String[][] testCases = {
            {"1 + 2", "1 2 +"},
            {"( 1 + 2 )", "1 2 +"},
            {"( ( 1 + 2 ) )", "1 2 +"},
            {"( ( ( 1 + 2 ) ) )", "1 2 +"},
            {"( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )", "1 2 + 3 4 - 5 6 - * *"},
            {"( ( ( ( 1 + 2 ) * ( 3 - 4 ) ) * ( 5 - 6 ) ) )", "1 2 + 3 4 - * 5 6 - *"},
            {"( ( 1 + 2 ) * ( 3 - 4 ) ) * 5", "1 2 + 3 4 - * 5 *"},
            {"5 * ( ( 1 + 2 ) * ( 3 - 4 ) )", "5 1 2 + 3 4 - * *"},
            {"5 * 6", "5 6 *"}
        };
        for (int i = 0; i < testCases.length; i++) {
            assert infixToPostfix(testCases[i][0]).equals(testCases[i][1])
                : "Failed test case " + i;
            System.out.println(testCases[i][0]);
            System.out.println("    " + infixToPostfix(testCases[i][0]));
        }
    }
}
