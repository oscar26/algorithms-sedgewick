class QueueOfIntegers {
    private Node first;
    private Node last;
    private int N;

    private class Node {
        int item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void enqueue(int item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if (isEmpty())
            first = last;
        else
            oldLast.next = last;
        N++;
    }

    public int dequeue() {
        int item = first.item;
        first = first.next;
        if (isEmpty())
            last = null;
        N--;
        System.out.printf("Item: %d. Queue (first to last):", item);
        Node iterator = first;
        while (iterator != null){
            System.out.printf(" %d", iterator.item);
            iterator = iterator.next;
        }
        System.out.println();
        return item;
    }
}

class Main {
    static void a() {
        // Valid
        System.out.println("\nSequence (a) (valid)");
        QueueOfIntegers queue = new QueueOfIntegers();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
    }

    static void b() {
        // Invalid. To print out the 4, the 0 1 2 and 3 must have been dequeued
        // before and thus printed out first
        System.out.println("\nSequence (b) (invalid)");
        QueueOfIntegers queue = new QueueOfIntegers();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
    }

    static void c() {
        // Invalid. Similar reasoning as in (b)
        System.out.println("\nSequence (c) (invalid)");
        QueueOfIntegers queue = new QueueOfIntegers();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
    }

    static void d() {
        // Invalid. Similar reasoning as in (b)
        System.out.println("\nSequence (d) (invalid)");
        QueueOfIntegers queue = new QueueOfIntegers();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
    }

    public static void main(String[] args) {
        a();
        b();
        c();
        d();
    }
}
