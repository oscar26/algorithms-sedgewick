import java.util.Iterator;

class Node {
    int item;
    Node next;

    public Node(int item) {
        this.item = item;
    }
}

class LinkedList implements Iterable<Integer> {

    private int n;
    private Node first;

    public void append(int item) {
        this.append(new Node(item));
    }

    public void append(Node newNode) {
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
    }

    public Node reverse() {
        Node currentNode = first;
        Node previousNode = null;
        while (currentNode != null) {
            Node nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        first = previousNode;
        return first;
    }

    public Node reverseRecursive() {
        first = reverseLinkedListRecursive(first, null);
        // first = reverseRecursiveBook(first);
        return first;
    }

    public static Node reverseLinkedListRecursive(Node current, Node previous) {
        if (current == null)
            return current;
        Node next = current.next;
        current.next = previous;
        if (next == null)
            return current;
        return reverseLinkedListRecursive(next, current);
    }

    public static Node reverseRecursiveBook(Node first) {
        if (first == null || first.next == null)
            return first;
        Node newFirst = reverseRecursiveBook(first.next);
        first.next.next = first;
        first.next = null;
        return newFirst;
    }

    public Iterator<Integer> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<Integer> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Integer next() {
            int item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}
    }

}

class TestIterative {
    static void test1() {
        LinkedList list = new LinkedList();
        assert list.reverse() == null;
    }

    static void test2() {
        Node first = new Node(0);
        LinkedList list = new LinkedList();
        list.append(first);
        assert list.reverse() == first;
    }

    static void test3() {
        Node first = new Node(0);
        Node last = new Node(1);
        LinkedList list = new LinkedList();
        list.append(first);
        list.append(last);
        assert first.next == last;
        assert last.next == null;
        assert list.reverse() == last;
        assert last.next == first;
        assert first.next == null;
    }

    static void test4() {
        Node first = new Node(0);
        Node last = new Node(4);
        LinkedList list = new LinkedList();
        list.append(first);
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(last);
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert list.reverse() == last;
        assert first.next == null;
        counter = 4;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter--;
        }
    }

    static void test5() {
        LinkedList list = new LinkedList();
        list.append(0);
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.reverse();
        list.reverse();
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
    }

    static void runTests() {
        test1();
        test2();
        test3();
        test4();
        test5();
    }
}

class TestRecursive {
    static void test1() {
        LinkedList list = new LinkedList();
        assert list.reverseRecursive() == null;
    }

    static void test2() {
        Node first = new Node(0);
        LinkedList list = new LinkedList();
        list.append(first);
        assert list.reverseRecursive() == first;
    }

    static void test3() {
        Node first = new Node(0);
        Node last = new Node(1);
        LinkedList list = new LinkedList();
        list.append(first);
        list.append(last);
        assert first.next == last;
        assert last.next == null;
        assert list.reverse() == last;
        assert last.next == first;
        assert first.next == null;
    }

    static void test4() {
        Node first = new Node(0);
        Node last = new Node(4);
        LinkedList list = new LinkedList();
        list.append(first);
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(last);
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert list.reverseRecursive() == last;
        assert first.next == null;
        counter = 4;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter--;
        }
    }

    static void test5() {
        LinkedList list = new LinkedList();
        list.append(0);
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.reverseRecursive();
        list.reverseRecursive();
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
    }

    static void runTests() {
        test1();
        test2();
        test3();
        test4();
        test5();
    }
}

class Main {

    public static void main(String[] args) {
        TestIterative.runTests();
        TestRecursive.runTests();
    }
}
