import edu.princeton.cs.algs4.Stack;

class Main {
    static Stack<String> copyStack(Stack<String> srcStack) {
        // This is a deep copy, meaning the objects are new instances that have
        // the same value
        Stack<String> temp = new Stack<>();
        // Order is reversed
        for (String s : srcStack)
            temp.push(new String(s.toCharArray()));
        Stack<String> copy = new Stack<>();
        // Restore original order
        for (String s : temp)
            copy.push(s);
        return copy;
    }

    public static void main(String[] args) {
        Stack<String> sourceStack = new Stack<>();
        sourceStack.push("1");
        sourceStack.push("2");
        sourceStack.push("3");
        sourceStack.push("4");
        sourceStack.push("5");
        Stack<String> copiedStack = copyStack(sourceStack);
        assert copiedStack.size() == sourceStack.size() : "Sizes are different";
        while (!sourceStack.isEmpty()) {
            String sourceElement = sourceStack.pop();
            String copiedElement = copiedStack.pop();
            assert copiedElement != sourceElement : "Same reference";
            assert copiedElement.equals(sourceElement) : "Different contents";
        }
    }
}
