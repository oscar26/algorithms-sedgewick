import edu.princeton.cs.algs4.Date;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;

class Main {
    static Date[] readDates(String filename) {
        In in = new In(filename);
        Queue<Date> queue = new Queue<>();
        while (!in.isEmpty())
            queue.enqueue(new Date(in.readLine()));
        Date[] dates = new Date[queue.size()];
        int i = 0;
        for (Date date : queue)
            dates[i++] = date;
        return dates;
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the file name");
        Date[] dates = readDates(args[0]);
        for (Date date : dates)
            System.out.println(date);
    }
}
