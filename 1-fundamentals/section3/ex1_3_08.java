/*
 * String: "it was - the best - of times - - - it was - the - -"
 *
 *     it    -> ["it"]
 *     was   -> ["it", "was"]
 *     -     -> ["it",  null]
 *     the   -> ["it", "the"]
 *     best  -> ["it", "the", "best",    null]
 *     -     -> ["it", "the",   null,    null]
 *     of    -> ["it", "the",   "of",    null]
 *     times -> ["it", "the",   "of", "times"]
 *     -     -> ["it", "the",   "of",    null]
 *     -     -> ["it", "the",   null,    null]
 *     -     -> ["it",  null]
 *     it    -> ["it",  "it"]
 *     was   -> ["it",  "it",  "was",    null]
 *     -     -> ["it",  "it",   null,    null]
 *     the   -> ["it",  "it",  "the",    null]
 *     -     -> ["it",  "it",   null,    null]
 *     -     -> ["it",  null]
 *
 * The stack's final size is 1 and the final array is of size 2 and has the
 * word "it" in the first position.
 *
 * */

import java.util.Iterator;
import java.util.NoSuchElementException;

class ResizingArrayStack<Item> implements Iterable<Item> {

    private Item[] a = (Item[]) new Object[1];
    private int N = 0;

    public boolean isEmpty() {
        return N == 0;
    }

    public int size() {
        return N;
    }

    public Item peek() {
        if (isEmpty())
            throw new NoSuchElementException("Stack is empty");
        return a[N-1];
    }

    private void resize(int max) {
        Item[] newArray = (Item[]) new Object[max];
        for (int i = 0; i < N; i++)
            newArray[i] = a[i];
        a = newArray;
        System.out.print("Resizing array. New size: " + max + ". Contents: [");
        for (int i = 0; i < max; i++)
            System.out.printf(" %s ", a[i]);
        System.out.println("]");
    }

    public void push(Item item) {
        if (N == a.length) resize(2*a.length);
        a[N++] = item;
    }

    public Item pop() {
        Item item = a[--N];
        a[N] = null;
        if (N > 0 && N == a.length/4) resize(a.length/2);
        return item;
    }

    public Iterator<Item> iterator() {
        return new ReverseArrayIterator();
    }

    private class ReverseArrayIterator implements Iterator<Item> {
        private int i = N;

        public boolean hasNext() {
            return i > 0;
        }

        public Item next() {
            return a[--i];
        }

        public void remove() {}
    }

}

class Main {

    public static void main(String[] args) {
        ResizingArrayStack<String> stack = new ResizingArrayStack<>();
        String string = "it was - the best - of times - - - it was - the - -";
        for (String token : string.split(" "))
            if (token.equals("-"))
                stack.pop();
            else
                stack.push(token);
        System.out.println(stack.size());
    }

}
