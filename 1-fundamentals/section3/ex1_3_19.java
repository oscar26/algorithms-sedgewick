class LinkedList {

    private Node first;

    private class Node {
        String item;
        Node next;
    }

    public void append(String item) {
        Node newNode = new Node();
        newNode.item = item;
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
    }

    public String removeLast() {
        // We can also keep a reference to the previous node and use it to
        // delete the last node, a .next dereference would be dropped
        String item;
        if (first.next == null) {
            item = first.item;
            first = null;
        } else {
            Node currentNode = first;
            while (currentNode.next.next != null)
                currentNode = currentNode.next;
            item = currentNode.next.item;
            currentNode.next = null;
        }
        return item;
    }

}

class Main {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.append("1");
        list.append("2");
        list.append("3");
        System.out.println(list.removeLast());
        System.out.println(list.removeLast());
        System.out.println(list.removeLast());
    }
}
