class Node {
    String item;
    Node next;

    public Node(String item) {
        this.item = item;
    }
}

class LinkedList {

    private Node first;
    private int n;

    public LinkedList(Node[] nodes) {
        if (nodes.length == 0)
            throw new IllegalArgumentException("at least one node must be passed");
        first = nodes[0];
        for (int i = 1; i < nodes.length; i++)
            nodes[i-1].next = nodes[i];
        n = nodes.length;
    }

    public int size() {
        return n;
    }

    public void insertAfter(Node node, Node nodeToAdd) {
        if (node != null && nodeToAdd != null && first != null) {
            Node currentNode = first;
            while (currentNode.next != null && currentNode != node)
                currentNode = currentNode.next;
            if (currentNode == node) {
                nodeToAdd.next = currentNode.next;
                currentNode.next = nodeToAdd;
                n++;
            }
        }
    }

}

class Main {
    static void test1() {
        Node[] nodes = {new Node("1"), new Node("2"), new Node("3")};
        LinkedList list = new LinkedList(nodes);
        Node nodeToAdd = new Node("1.5");
        list.insertAfter(nodes[0], nodeToAdd);
        assert list.size() == nodes.length + 1;
        assert nodes[0].next == nodeToAdd;
        assert nodeToAdd.next == nodes[1];
    }

    static void test2() {
        Node[] nodes = {new Node("1"), new Node("2"), new Node("3")};
        LinkedList list = new LinkedList(nodes);
        Node nodeToAdd = new Node("4");
        list.insertAfter(nodes[2], nodeToAdd);
        assert list.size() == nodes.length + 1;
        assert nodes[2].next == nodeToAdd;
        assert nodeToAdd.next == null;
    }

    static void test3() {
        Node[] nodes = {new Node("1"), new Node("2"), new Node("3")};
        LinkedList list = new LinkedList(nodes);
        list.insertAfter(nodes[2], null);
        assert list.size() == nodes.length;
        assert nodes[2].next == null;
    }

    static void test4() {
        Node[] nodes = {new Node("1"), new Node("2"), new Node("3")};
        LinkedList list = new LinkedList(nodes);
        Node nodeToAdd = new Node("payload");
        list.insertAfter(null, nodeToAdd);
        assert list.size() == nodes.length;
        assert nodeToAdd.next == null;
    }

    static void test5() {
        Node[] nodes = {new Node("1"), new Node("2"), new Node("3")};
        LinkedList list = new LinkedList(nodes);
        Node nodeToAdd = new Node("4");
        list.insertAfter(new Node("doesn't exist"), nodeToAdd);
        assert list.size() == nodes.length;
        assert nodeToAdd.next == null;
    }

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
    }
}
