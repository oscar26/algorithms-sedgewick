
// 0 1 2 3 4 5 6
// 0 2 3 4 5 6   : 1
// 0 2 4 5 6     : 3
// 0 2 4 6       : 5
// 2 4 6         : 0
// 2 6           : 4
// 6             : 2
//               : 6
//
// 0 1 2 3 4 5 6
// 0 _ 2 3 4 5 6
// 0 _ 2 _ 4 5 6
// 0 _ 2 _ 4 _ 6
// _ _ 2 _ 4 _ 6
// _ _ 2 _ _ _ 6
// _ _ _ _ _ _ 6
// _ _ _ _ _ _ _


import edu.princeton.cs.algs4.Queue;

class Main {
    static Queue<Integer> josephus(int n, int m) {
        // Naive attempt. Checks every position to see whether the person is
        // alive or not
        boolean[] alive = new boolean[n];
        for (int i = 0; i < n; i++)
            alive[i] = true;
        Queue<Integer> order = new Queue<>();
        int peopleLeft = n;
        int i = 0;
        int counter = 0;
        while (peopleLeft > 0) {
            if (alive[i])
                counter++;
            if (counter == m) {
                order.enqueue(i);
                alive[i] = false;
                counter = 0;
                peopleLeft--;
            }
            i = (i + 1) % n;
        }
        return order;
    }

    static Queue<Integer> josephusV2(int n, int m) {
        // Naive attempt 2. After looking for other people's solutions, found a
        // terser one but still not close to the DP solution (although that
        // solution returns the last man to be killed, perhaps it could be
        // adapted to return the sequence of kills)
        Queue<Integer> people = new Queue<>();
        for (int i = 0; i < n; i++)
            people.enqueue(i);
        Queue<Integer> order = new Queue<>();
        int counter = 1;
        while (!people.isEmpty()) {
            if (counter % m == 0)
                order.enqueue(people.dequeue());
            else
                people.enqueue(people.dequeue());
            counter++;
        }
        return order;
    }

    // Solution given by the authors in the book's site
    static Queue<Integer> josephusBook(int n, int m) {
        // initialize the queue
        Queue<Integer> queue = new Queue<Integer>();
        for (int i = 0; i < n; i++)
            queue.enqueue(i);
        Queue<Integer> order = new Queue<>();
        while (!queue.isEmpty()) {
            for (int i = 0; i < m-1; i++)
                queue.enqueue(queue.dequeue());
            order.enqueue(queue.dequeue());
        }
        return order;
    }

    public static void main(String[] args) {
        if (args.length != 2)
            throw new IllegalArgumentException("Pass N and M");
        int n = Integer.parseInt(args[0]);
        int m = Integer.parseInt(args[1]);
        if (n < 1 || m < 1)
            throw new IllegalArgumentException("N and M must be greater than 0");
        m = (m % n);  // Wraps M if M >= N
        System.out.print("V1: ");
        Queue<Integer> order = josephus(n, m);
        while (!order.isEmpty())
            System.out.print(order.dequeue() + " ");
        System.out.print("\nV2: ");
        order = josephusV2(n, m);
        while (!order.isEmpty())
            System.out.print(order.dequeue() + " ");
        System.out.print("\nV3: ");
        order = josephusBook(n, m);
        while (!order.isEmpty())
            System.out.print(order.dequeue() + " ");
        System.out.println();
    }
}
