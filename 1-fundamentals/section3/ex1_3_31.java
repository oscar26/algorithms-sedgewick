import java.util.Iterator;
import java.util.NoSuchElementException;

class DoubleLinkedList<Item> implements Iterable<Item> {

    private DoubleNode first;
    private int n;

    public class DoubleNode {
        Item item;
        DoubleNode next;
        DoubleNode previous;

        public DoubleNode(Item item) {
            this.item = item;
        }
    }

    public boolean isEmpty() {
        return first == null;  // or n == 0;
    }

    public int size() {
        return n;
    }

    public DoubleNode insertAtBeginning(Item item) {
        DoubleNode newNode = new DoubleNode(item);
        if (isEmpty()) {
            first = newNode;
        } else {
            newNode.next = first;
            first.previous = newNode;
            first = newNode;
        }
        n++;
        return newNode;
    }

    public DoubleNode insertAtEnd(Item item) {
        DoubleNode newNode = new DoubleNode(item);
        if (isEmpty()) {
            first = newNode;
        } else {
            DoubleNode currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
            newNode.previous = currentNode;
        }
        n++;
        return newNode;
    }

    public Item removeFromBeginning() {
        if (isEmpty())
            throw new NoSuchElementException("list is empty");
        Item item = first.item;
        first = first.next;
        if (!isEmpty())
            first.previous = null;
        n--;
        return item;
    }

    public Item removeFromEnd() {
        if (isEmpty())
            throw new NoSuchElementException("list is empty");
        DoubleNode currentNode = first;
        while (currentNode.next != null)
            currentNode = currentNode.next;
        Item item = currentNode.item;
        if (currentNode == first)
            first = null;
        else
            currentNode.previous.next = null;
        n--;
        return item;
    }

    public DoubleNode createNode(Item item) {
        // Public method to facilitate tests as the following three methods
        // expect a DoubleNode
        return new DoubleNode(item);
    }

    public void insertBefore(DoubleNode node, Item item) {
        isInList(node);
        DoubleNode newNode = new DoubleNode(item);
        DoubleNode previous = node.previous;
        if (previous != null)
            previous.next = newNode;
        node.previous = newNode;
        newNode.next = node;
        if (node == first)
            first = newNode;
        n++;
    }

    public void insertAfter(DoubleNode node, Item item) {
        isInList(node);
        DoubleNode newNode = new DoubleNode(item);
        newNode.next = node.next;
        newNode.previous = node;
        node.next = newNode;
        n++;
    }

    public void remove(DoubleNode node) {
        isInList(node);
        DoubleNode previous = node.previous;
        DoubleNode next = node.next;
        if (previous != null)
            previous.next = next;
        if (next != null)
            next.previous = previous;
        if (node == first)
            first = node.next;
        n--;
    }

    private void isInList(DoubleNode node) {
        // If node is not found, an exception is raised
        DoubleNode currentNode = first;
        while (currentNode != node && currentNode != null)
            currentNode = currentNode.next;
        if (currentNode == null)
            throw new NoSuchElementException("node not found");
    }

    public Iterator<Item> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<Item> {

        private DoubleNode current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}
    }
}


class Main {
    static void testInsertAtBeginning() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;
        list.insertAtBeginning(4);
        for (int item : list)
            assert item == 4;

        for (int i = 3; i >= 0; i--)
            list.insertAtBeginning(i);
        assert list.size() == 5;
        assert !list.isEmpty();
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
    }

    static void testInsertAtEnd() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;
        list.insertAtEnd(0);
        for (int item : list)
            assert item == 0;

        for (int i = 1; i < 5; i++)
            list.insertAtEnd(i);
        assert list.size() == 5;
        assert !list.isEmpty();
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
    }

    static void testRemoveFromBeginning() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;
        boolean exceptionThrown = false;
        try {
            list.removeFromBeginning();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            list.insertAtBeginning(i);
        assert list.size() == 5;
        assert !list.isEmpty();
        for (int i = 0; i < 5; i++) {
            int item = list.removeFromBeginning();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert list.size() == 0;
        assert list.isEmpty();

        list.insertAtEnd(1);
        assert list.removeFromBeginning() == 1;
        assert list.size() == 0;
        assert list.isEmpty();
    }

    static void testRemoveFromEnd() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;
        boolean exceptionThrown = false;
        try {
            list.removeFromEnd();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            list.insertAtBeginning(i);
        assert list.size() == 5;
        assert !list.isEmpty();
        for (int i = 4; i >= 0; i--) {
            int item = list.removeFromEnd();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert list.size() == 0;
        assert list.isEmpty();

        list.insertAtBeginning(1);
        assert list.removeFromEnd() == 1;
        assert list.size() == 0;
        assert list.isEmpty();
    }

    static void testInsertBefore() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;

        boolean exceptionThrown = false;
        try {
            list.insertBefore(null, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode node = list.createNode(0);
        exceptionThrown = false;
        try {
            list.insertBefore(node, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode last = list.insertAtBeginning(1);
        exceptionThrown = false;
        try {
            list.insertBefore(node, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        list.insertBefore(last, 0);
        assert list.size() == 2;
        assert !list.isEmpty();
        assert 1 == list.removeFromEnd();
        assert 0 == list.removeFromEnd();
        assert list.size() == 0;
        assert list.isEmpty();

        last = list.insertAtBeginning(4);
        for (int i = 0; i < 4; i++)
            list.insertBefore(last, i);
        assert list.size() == 5;
        assert !list.isEmpty();
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert counter == 5;

        list.insertAtEnd(10);
        assert 10 == list.removeFromEnd();
        assert 4 == list.removeFromEnd();
    }

    static void testInsertAfter() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;

        boolean exceptionThrown = false;
        try {
            list.insertAfter(null, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode node = list.createNode(0);
        exceptionThrown = false;
        try {
            list.insertAfter(node, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode first = list.insertAtBeginning(0);
        exceptionThrown = false;
        try {
            list.insertAfter(node, 0);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        list.insertAfter(first, 1);
        assert list.size() == 2;
        assert !list.isEmpty();
        assert 1 == list.removeFromEnd();
        assert 0 == list.removeFromEnd();
        assert list.size() == 0;
        assert list.isEmpty();

        first = list.insertAtBeginning(0);
        for (int i = 4; i >= 1; i--)
            list.insertAfter(first, i);
        assert list.size() == 5;
        int counter = 0;
        for (int item : list) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert counter == 5;
        assert !list.isEmpty();

        list.insertAtEnd(10);
        assert 10 == list.removeFromEnd();
        assert 4 == list.removeFromEnd();
    }

    static void testRemove() {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        assert list.size() == 0;

        boolean exceptionThrown = false;
        try {
            list.remove(null);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode node = list.createNode(0);
        exceptionThrown = false;
        try {
            list.remove(node);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        DoubleLinkedList.DoubleNode first = list.insertAtBeginning(0);
        exceptionThrown = false;
        try {
            list.remove(node);
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        assert list.size() == 1;
        assert !list.isEmpty();

        list.remove(first);
        assert list.size() == 0;
        assert list.isEmpty();

        first = list.insertAtBeginning(0);
        for (int i = 1; i < 4; i++)
            list.insertAtEnd(i);
        DoubleLinkedList.DoubleNode last = list.insertAtEnd(4);
        assert list.size() == 5;
        assert !list.isEmpty();
        list.remove(first);
        assert list.size() == 4;
        assert !list.isEmpty();
        list.remove(last);
        assert list.size() == 3;
        assert !list.isEmpty();

        assert list.removeFromBeginning() == 1;
        assert list.size() == 2;
        assert !list.isEmpty();
        assert list.removeFromEnd() == 3;
        assert list.size() == 1;
        assert !list.isEmpty();

        list.removeFromBeginning();
        list.insertAtEnd(0);
        list.insertAtEnd(1);
        DoubleLinkedList.DoubleNode middle = list.insertAtEnd(2);
        list.insertAtEnd(3);
        list.insertAtEnd(4);
        assert list.size() == 5;
        assert !list.isEmpty();

        list.remove(middle);
        assert list.size() == 4;
        assert !list.isEmpty();

        int counter = 0;
        for (int item : list) {
            if (counter == 2) counter++;
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
    }

    public static void main(String[] args) {
        testInsertAtBeginning();
        testInsertAtEnd();
        testRemoveFromBeginning();
        testRemoveFromEnd();
        testInsertBefore();
        testInsertAfter();
        testRemove();
    }
}
