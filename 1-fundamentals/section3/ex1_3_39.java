import java.util.NoSuchElementException;

class RingBuffer<Item> {

    private Item[] items;
    private int n;
    private int first;

    public RingBuffer(int cap) {
        items = (Item[]) new Object[cap];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public int capacity() {
        return items.length;
    }

    public void enqueue(Item item) {
        if (n == items.length)
            throw new RuntimeException("buffer is full");
        n++;
        int last = (first + n - 1) % items.length;
        items[last] = item;
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException();
        Item item = items[first];
        first = (first + 1) % items.length;
        n--;
        return item;
    }

}

class Main {
    static void testEmptyException(RingBuffer<Integer> buffer) {
        assert buffer.isEmpty();
        boolean exceptionThrown = false;
        try {
            buffer.dequeue();
        } catch(NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;
    }

    static void testFullException(RingBuffer<Integer> buffer) {
        assert buffer.isEmpty();
        for (int i = 0; i < buffer.capacity(); i++)
            buffer.enqueue(i);
        assert buffer.size() == buffer.capacity();
        boolean exceptionThrown = false;
        try {
            buffer.enqueue(10);
        } catch(RuntimeException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;
    }

    static void testInsertionsDeletions(RingBuffer<Integer> buffer) {
        assert buffer.isEmpty();
        for (int i = 0; i < buffer.capacity(); i++)
            buffer.enqueue(i);
        assert buffer.size() == buffer.capacity();
        for (int i = 0; i < buffer.capacity(); i++)
            assert buffer.dequeue() == i;
        assert buffer.isEmpty();
        for (int i = 0; i < buffer.capacity(); i++) {
            buffer.enqueue(i);
            assert buffer.dequeue() == i;
        }
        assert buffer.isEmpty();
    }

    public static void main(String[] args) {
        int capacity = 10;
        testEmptyException(new RingBuffer<Integer>(capacity));
        testFullException(new RingBuffer<Integer>(capacity));
        testInsertionsDeletions(new RingBuffer<Integer>(capacity));
    }
}
