import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdRandom;

class RandomBag<Item> implements Iterable<Item> {

    private Item[] items;
    private int n;

    public RandomBag() {
        this.items = (Item[]) new Object[1];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    private void resize(int cap) {
        Item[] newArray = (Item[]) new Object[cap];
        for (int i = 0; i < size(); i++)
            newArray[i] = items[i];
        items = newArray;
    }

    public void add(Item item) {
        items[n++] = item;
        if (size() == items.length)
            resize(2*items.length);
    }

    public Iterator<Item> iterator() {
        return new RandomBagIterator();
    }

    private class RandomBagIterator implements Iterator<Item> {

        private int count;
        private int[] randomIndexes;

        // Recalling exercise 1.1.36
        private int[] indexesPermutation() {
            // Initialize indexes array
            int n = size();
            int[] indexes = new int[n];
            for (int i = 0; i < n; i++)
                indexes[i] = i;
            // Shuffle indexes
            for (int i = 0; i < n; i++) {
                int r = i + StdRandom.uniform(n-i);
                int temp = indexes[i];
                indexes[i] = indexes[r];
                indexes[r] = temp;
            }
            return indexes;
        }

        public RandomBagIterator() {
            randomIndexes = indexesPermutation();
        }

        public boolean hasNext() {
            return count < size();
        }

        public Item next() {
            return items[randomIndexes[count++]];
        }

        public void remove() {}

    }

}

class Main {
    public static void main(String[] args) {
        int trials = 10000;
        int n = 10;
        RandomBag<Integer> randomBag = new RandomBag<>();
        for (int i = 0; i < n; i++)
            randomBag.add(i);
        int[] frequencies = new int[n];
        for (int i = 0; i < trials; i++) {
            int counter = 0;
            for (int item : randomBag) {
                if (item == counter)
                    frequencies[counter]++;
                counter++;
            }

        }
        double expectedProbability = 1.0/n;
        for (int i = 0; i < n; i++) {
            double empiricalProbabilty = 1.0*frequencies[i]/trials;
            System.out.printf("Number %d found in position %d: %.2f%%\n", i, i, empiricalProbabilty*100);
            assert Math.abs(expectedProbability - empiricalProbabilty) < 1e-2;
        }

    }
}
