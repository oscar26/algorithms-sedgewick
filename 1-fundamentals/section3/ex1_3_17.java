import edu.princeton.cs.algs4.Transaction;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;

class Main {
    static Transaction[] readDates(String filename) {
        In in = new In(filename);
        Queue<Transaction> queue = new Queue<>();
        while (!in.isEmpty())
            queue.enqueue(new Transaction(in.readLine()));
        Transaction[] transactions = new Transaction[queue.size()];
        int i = 0;
        for (Transaction transaction : queue)
            transactions[i++] = transaction;
        return transactions;
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the file name");
        Transaction[] transactions = readDates(args[0]);
        for (Transaction transaction : transactions)
            System.out.println(transaction);
    }
}
