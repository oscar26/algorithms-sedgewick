class Node {
    String item;
    Node next;

    public Node(String item) {
        this.item = item;
    }
}

class LinkedList {

    private int n;
    public Node first;

    public int size() {
        return n;
    }

    public void append(String item) {
        this.append(new Node(item));
    }

    public void append(Node newNode) {
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
        n++;
    }

    public void remove(String key) {
        Node currentNode = first;
        while (currentNode != null && currentNode.next != null) {
            if (currentNode.next.item.equals(key)) {
                currentNode.next = currentNode.next.next;
                n--;
            } else {
                currentNode = currentNode.next;
            }
        }
        // First node is handled in last to avoid losing the reference to
        // the rest of the list
        if (first != null && first.item.equals(key)) {
            first = first.next;
            n--;
        }
    }

}

class Main {
    static void test1() {
        LinkedList list = new LinkedList();
        list.remove("0");
        assert list.size() == 0;
    }

    static void test2() {
        LinkedList list = new LinkedList();
        list.append("1");
        list.append("2");
        list.append("3");
        list.remove("0");
        assert list.size() == 3;
    }

    static void test3() {
        LinkedList list = new LinkedList();
        assert list.size() == 0;
        list.append("1");
        assert list.size() == 1;
        list.remove("1");
        assert list.size() == 0;
    }

    static void test4() {
        LinkedList list = new LinkedList();
        assert list.size() == 0;
        list.append("1");
        list.append("1");
        assert list.size() == 2;
        assert list.first.item.equals("1");
        list.remove("1");
        assert list.size() == 0;
        assert list.first == null;
    }

    static void test5() {
        LinkedList list = new LinkedList();
        assert list.size() == 0;
        list.append("1");
        list.append("1");
        list.append("2");
        assert list.size() == 3;
        assert list.first.item.equals("1");
        assert list.first.next != null;
        assert list.first.next.item.equals("1");
        list.remove("1");
        assert list.size() == 1;
        assert list.first.item.equals("2");
        assert list.first.next == null;
    }

    static void test6() {
        LinkedList list = new LinkedList();
        assert list.size() == 0;
        list.append("2");
        list.append("1");
        list.append("1");
        assert list.size() == 3;
        assert list.first.item.equals("2");
        assert list.first.next != null;
        assert list.first.next.item.equals("1");
        list.remove("1");
        assert list.size() == 1;
        assert list.first.item.equals("2");
        assert list.first.next == null;
    }

    static void test7() {
        LinkedList list = new LinkedList();
        assert list.size() == 0;
        list.append("1");
        list.append("2");
        list.append("1");
        assert list.size() == 3;
        assert list.first.item.equals("1");
        assert list.first.next != null;
        assert list.first.next.item.equals("2");
        list.remove("1");
        assert list.size() == 1;
        assert list.first.item.equals("2");
        assert list.first.next == null;
    }

    static void test8() {
        LinkedList list = new LinkedList();
        Node node1, node2, node3;
        node1 = new Node("1");
        node2 = new Node("2");
        node3 = new Node("3");
        assert list.size() == 0;
        list.append(node1);
        list.append(node2);
        list.append(node3);
        assert list.first == node1;
        assert node1.next == node2;
        assert node2.next == node3;
        assert list.size() == 3;
        list.remove("2");
        assert list.size() == 2;
        assert node1.next == node3;
    }

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
    }
}
