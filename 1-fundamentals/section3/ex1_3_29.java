import java.util.Iterator;
import java.util.NoSuchElementException;

class CircularQueue<Item> implements Iterable<Item> {

    private int n;
    private Node last;

    private class Node {
        Item item;
        Node next;
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return last == null;  // or n == 0;
    }

    public void enqueue(Item item) {
        Node node = new Node();
        node.item = item;
        if (isEmpty()) {
            last = node;
            node.next = node;
        } else {
            node.next = last.next;
            last.next = node;
            last = node;
        }
        n++;
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("Queue is empty");
        Item item = last.next.item;
        if (last == last.next)  // or n == 1;
            last = null;
        else
            last.next = last.next.next;
        n--;
        return item;
    }

    private Node getFirst() {
        if (isEmpty())
            return null;
        return last.next;
    }

    public Iterator<Item> iterator() {
        return new QueueIterator();
    }

    private class QueueIterator implements Iterator<Item> {

        // Using the private method getFirst() to avoid repeatedly evaluating
        // the conditional inside next()
        private Node current = getFirst();
        private int i = 0;

        public boolean hasNext() {
            return !isEmpty() && i < size();
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            i++;
            return item;
        }

        public void remove() {}
    }
}


class Main {
    static void testEnqueue() {
        CircularQueue<Integer> queue = new CircularQueue<>();
        assert queue.size() == 0 && queue.isEmpty();
        queue.enqueue(0);
        assert queue.size() == 1;
        for (int item : queue)
            assert 0 == item;
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        assert queue.size() == 8;
        int counter = 0;
        for (int item : queue) {
            assert counter == item
                : String.format("Item #%d doesn't match. Expected: %d, got: %d", counter, counter, item);
            counter++;
        }
    }

    static void testDequeue() {
        CircularQueue<Integer> queue = new CircularQueue<>();
        boolean exceptionThrown = false;
        try {
            queue.dequeue();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        queue.enqueue(0);
        assert queue.size() == 1;
        assert queue.dequeue() == 0;
        assert queue.size() == 0 && queue.isEmpty();

        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        assert queue.size() == 8;
        for (int i = 0; i < 8; i++)
            assert queue.dequeue() == i;
        assert queue.size() == 0 && queue.isEmpty();
    }

    public static void main(String[] args) {
        testEnqueue();
        testDequeue();
    }
}
