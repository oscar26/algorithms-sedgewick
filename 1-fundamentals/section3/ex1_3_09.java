/*
 * The solution assumes that tokens are separated by spaces. Otherwise, a
 * tokenizer must be implemented.
 * */

import edu.princeton.cs.algs4.Stack;

class Main {
    static String fillParentheses(String expression) {
        // Parentheses are inverted as the stack reverses the order of
        // processed tokens. The whole expression is reversed at the end to
        // correct this.
        //
        // The character "@" is used as placeholder to eliminate nesting levels
        // and replace later with balanced subexpressions.
        Stack<String> subExpressions = new Stack<>();
        Stack<String> unprocessed = new Stack<>();
        for (String token : expression.split("\\s")) {
            if (token.equals(")")) {
                if (unprocessed.size() == 1) {
                    // Handle redundant parentheses
                    unprocessed.pop();
                    subExpressions.push(") @ (");
                } else {
                    String[] symbols = {")", unprocessed.pop(), unprocessed.pop(), unprocessed.pop(), "("};
                    subExpressions.push(String.join(" ", symbols));
                }
                unprocessed.push("@");
            } else {
                unprocessed.push(token);
            }
        }
        String balancedExp = "";
        while (!unprocessed.isEmpty())
            balancedExp += unprocessed.pop() + " ";
        balancedExp = balancedExp.trim();
        while (!subExpressions.isEmpty())
            balancedExp = balancedExp.replaceFirst("@", subExpressions.pop());
        return new StringBuilder(balancedExp).reverse().toString();
    }

    public static void main(String[] args) {
        String[][] testCases = {
            {"1 + 2 ) * 3 - 4 ) * 5 - 6 ) ) )", "( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )"},
            {"1 + 2 ) * 3 - 4 ) ) * 5 - 6 ) ) )", "( ( ( ( 1 + 2 ) * ( 3 - 4 ) ) * ( 5 - 6 ) ) )"},
            {"1 + 2 ) * 3 - 4 ) ) * 5 - 6 ) ) )", "( ( ( ( 1 + 2 ) * ( 3 - 4 ) ) * ( 5 - 6 ) ) )"},
            {"1 + 2 ) * 3 - 4 ) ) * 5", "( ( 1 + 2 ) * ( 3 - 4 ) ) * 5"},
            {"5 * 1 + 2 ) * 3 - 4 ) )", "5 * ( ( 1 + 2 ) * ( 3 - 4 ) )"},
            {"5 * 6", "5 * 6"},
            {"1 + 2 )", "( 1 + 2 )"},
            {"1 + 2 ) )", "( ( 1 + 2 ) )"},
            {"1 + 2 ) ) )", "( ( ( 1 + 2 ) ) )"}
        };
        for (int i = 0; i < testCases.length; i++) {
            assert fillParentheses(testCases[i][0]).equals(testCases[i][1])
                : "Failed test case " + i;
            System.out.println(testCases[i][0]);
            System.out.println("    " + fillParentheses(testCases[i][0]));
        }
    }
}
