import edu.princeton.cs.algs4.Stack;

class Main {
    public static void main(String[] args) {
        int N = 50;
        // Converts to binary
        Stack<Integer> stack = new Stack<Integer>();
        while (N > 0) {
            stack.push(N % 2);
            N /= 2;
        }
        for (int d : stack) System.out.print(d);
        System.out.println();
    }
}
