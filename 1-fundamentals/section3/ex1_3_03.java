/*
 * The sequences of pushes and pops are shown for each valid sequence of
 * numbers. For the invalid sequences, the explanation is laid out in the
 * comments.
 * */

class FixedCapacityStackOfIntegers {
    private int[] a;
    private int N;

    public FixedCapacityStackOfIntegers(int cap) {
        a = new int[cap];
    }

    public void push(int item) {
        a[N++] = item;
    }

    public int pop() {
        int item = a[--N];
        System.out.printf("Item: %d. Stack (bottom to top):", item);
        for (int i = 0; i < N; i++)
            System.out.printf(" %d ", a[i]);
        System.out.println();
        return item;
    }

}


class Main {

    static void a() {
        // Valid sequence
        System.out.println("\nSequence (a) (valid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }

    static void b() {
        // Invalid since after popping the 9 remains 0 and 1, where 1 is at the
        // top of the stack, then the last two pops will print "1 0", not "0 1".
        System.out.println("\nSequence (b) (invalid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.push(5);
        stack.push(6);
        stack.pop();
        stack.push(7);
        stack.push(8);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.push(9);
        stack.pop();
        stack.pop();
        stack.pop();
    }

    static void c() {
        // Valid
        System.out.println("\nSequence (c) (valid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.pop();
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.pop();
        stack.push(6);
        stack.pop();
        stack.push(7);
        stack.pop();
        stack.pop();
        stack.push(8);
        stack.pop();
        stack.push(9);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }

    static void d() {
        // Valid
        System.out.println("\nSequence (d) (valid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.push(5);
        stack.pop();
        stack.push(6);
        stack.pop();
        stack.push(7);
        stack.pop();
        stack.push(8);
        stack.pop();
        stack.push(9);
        stack.pop();
    }

    static void e() {
        // Valid
        System.out.println("\nSequence (e) (valid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.pop();
        stack.push(2);
        stack.pop();
        stack.push(3);
        stack.pop();
        stack.push(4);
        stack.pop();
        stack.push(5);
        stack.pop();
        stack.push(6);
        stack.pop();
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }

    static void f() {
        // Invalid. After the 8 is popped, the item at the top is 7 and 1 is at
        // the bottom, implying that 7 must be printed before the 1
        System.out.println("\nSequence (f) (invalid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.pop();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.push(5);
        stack.push(6);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.push(7);
        stack.push(8);
        stack.pop();
        stack.push(9);
    }

    static void g() {
        // Invalid. After the 3 is popped, 2 is the top item and 0 the bottom
        // one, meaning that in the next pop, 2 will be printed first, ending
        // the sequence in "2 0" and not "0 2"
        System.out.println("\nSequence (g) (invalid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.pop();
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.pop();
        stack.push(8);
        stack.push(9);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }

    static void h() {
        // Valid
        System.out.println("\nSequence (h) (valid)");
        FixedCapacityStackOfIntegers stack;
        stack = new FixedCapacityStackOfIntegers(10);
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.pop();
        stack.pop();
        stack.push(3);
        stack.push(4);
        stack.pop();
        stack.pop();
        stack.push(5);
        stack.push(6);
        stack.pop();
        stack.pop();
        stack.push(7);
        stack.push(8);
        stack.pop();
        stack.pop();
        stack.push(9);
        stack.pop();
        stack.pop();
    }

    public static void main(String[] args) {
        a();
        b();
        c();
        d();
        e();
        f();
        g();
        h();
    }
}
