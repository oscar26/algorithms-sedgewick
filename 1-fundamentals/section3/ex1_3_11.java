/*
 * The solution assumes that tokens are separated by spaces. Otherwise, a
 * tokenizer must be implemented.
 *
 * */

import edu.princeton.cs.algs4.Stack;

class Main {
    static double evaluatePostfix(String expression) {
        Stack<String> operands = new Stack<>();
        for (String token : expression.split("\\s")) {
            if (token.equals("+")) {
                double b = Double.parseDouble(operands.pop());
                double a = Double.parseDouble(operands.pop());
                operands.push(String.valueOf(a + b));
            } else if (token.equals("-")) {
                double b = Double.parseDouble(operands.pop());
                double a = Double.parseDouble(operands.pop());
                operands.push(String.valueOf(a - b));
            } else if (token.equals("*")) {
                double b = Double.parseDouble(operands.pop());
                double a = Double.parseDouble(operands.pop());
                operands.push(String.valueOf(a * b));
            } else if (token.equals("/")) {
                double b = Double.parseDouble(operands.pop());
                double a = Double.parseDouble(operands.pop());
                operands.push(String.valueOf(a / b));
            } else {
                operands.push(token);
            }
        }
        return Double.parseDouble(operands.pop());
    }

    public static void main(String[] args) {
        String[] testCases = {
            "1 2 +",
            "1 2 + 3 4 - 5 6 - * *",
            "1 2 + 3 4 - * 5 6 - *",
            "1 2 + 3 4 - * 5 *",
            "5 1 2 + 3 4 - * *",
            "5 6 *"
        };
        double[] expectedResults = {
            3.0,
            3.0,
            3.0,
            -15.0,
            -15.0,
            30
        };
        for (int i = 0; i < testCases.length; i++) {
            assert evaluatePostfix(testCases[i]) == expectedResults[i]
                : "Failed test case " + i;
            System.out.println(testCases[i]);
            System.out.println("    " + evaluatePostfix(testCases[i]));
        }
    }
}
