import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;

class Stack<Item> implements Iterable<Item> {

    private int n;
    private Node first;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public boolean isEmpty() {
        return first == null;  // or n == 0;
    }

    public int size() {
        return n;
    }

    public void push(Item item) {
        Node node = new Node(item);
        if (first == null) {
            first = node;
        } else {
            node.next = first;
            first = node;
        }
        n++;
    }

    public Item pop() {
        if (isEmpty())
            throw new NoSuchElementException("stack is empty");
        Item item = first.item;
        first = first.next;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new StackIterator();
    }

    private class StackIterator implements Iterator<Item> {

        private Node current = first;
        private int initialSize = n;

        public boolean hasNext() {
            if (size() != initialSize)
                throw new ConcurrentModificationException();
            return current != null;
        }

        public Item next() {
            if (size() != initialSize)
                throw new ConcurrentModificationException();
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}

    }

}

class Main {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        assert stack.isEmpty();
        assert stack.size() == 0;
        int n = 5;
        for (int i = 0; i < n; i++)
            stack.push(i);
        assert !stack.isEmpty();
        assert stack.size() == n;
        int index = n - 1;
        for (int item : stack)
            assert item == index--;
        assert index == -1;
        boolean exceptionThrown = false;
        int poppedItem = -1;
        try {
            int counter = 0;
            for (int item : stack) {
                if (counter++ == stack.size()/2)
                    poppedItem = stack.pop();
            }
        } catch (ConcurrentModificationException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;
        assert stack.size() == n - 1;
        assert poppedItem == n - 1;
        index = n - 2;
        for (int item : stack)
            assert item == index--;
        assert index == -1;
    }
}
