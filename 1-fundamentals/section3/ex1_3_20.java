import java.util.NoSuchElementException;

class LinkedList {

    private int n;
    private Node first;

    private class Node {
        String item;
        Node next;
    }

    public void append(String item) {
        Node newNode = new Node();
        newNode.item = item;
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
        n++;
    }

    public String delete(int k) {
        if (k < 0)
            throw new IllegalArgumentException("k cannot be negative");
        if (k >= n)
            throw new NoSuchElementException();
        // After the previous two checks, there's at least one element
        String item;
        if (k == 0) {
            item = first.item;
            first = first.next;
        } else {
            Node currentNode = first;
            for (int i = 0; i < k - 1; i++)
                currentNode = currentNode.next;
            item = currentNode.next.item;
            currentNode.next = currentNode.next.next;
        }
        n--;
        return item;
    }

}

class Main {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.append("1");
        list.append("2");
        list.append("3");
        System.out.println(list.delete(0));
        System.out.println(list.delete(0));
        System.out.println(list.delete(0));
        System.out.println();

        list.append("1");
        list.append("2");
        list.append("3");
        System.out.println(list.delete(2));
        System.out.println(list.delete(0));
        System.out.println(list.delete(0));
        System.out.println();

        list.append("1");
        list.append("2");
        list.append("3");
        System.out.println(list.delete(1));
        System.out.println(list.delete(0));
        System.out.println(list.delete(0));
    }
}
