import java.util.NoSuchElementException;

class StackPeek<Item> {

    private Node first;
    private int N;

    private class Node {
        Item item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;  // N == 0 for array-based implementation
    }

    public int size() {
        return N;
    }

    public Item peek() {
        if (isEmpty())
            throw new NoSuchElementException("Stack is empty");
        return first.item;  // a[N-1] for array-based implementation
    }

    public void push(Item item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        N++;
    }

    public Item pop() {
        if (isEmpty())
            throw new NoSuchElementException("Stack is empty");
        Item item = first.item;
        first = first.next;
        N--;
        return item;
    }

}

class Main {
    public static void main(String[] args) {
        StackPeek<Integer> stack = new StackPeek<Integer>();
        boolean error = false;
        try {
            stack.peek();
        } catch (NoSuchElementException e) {
            error = true;
        }
        assert error;

        stack.push(1);
        assert stack.peek() == 1;

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.pop();
        assert stack.peek() == 2;
    }
}
