import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

class Cache<Item> implements Iterable<Item> {

    private int n;
    private Node mostRecent;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public boolean isEmpty() {
        return mostRecent == null;
    }

    public int size() {
        return n;
    }

    public void store(Item item) {
        remove(item);  // remove duplicates if any
        add(item);
    }

    public void add(Item item) {
        if (item == null)
            throw new IllegalArgumentException("null is not allowed");
        Node newNode = new Node(item);
        if (!isEmpty())
            newNode.next = mostRecent;
        mostRecent = newNode;
        n++;
    }

    public Item remove(Item key) {
        if (isEmpty())
            return null;
        Node previous = null;
        Node current = mostRecent;
        while (!current.item.equals(key) && current.next != null) {
            previous = current;
            current = current.next;
        }
        Item item = current.item;
        if (item.equals(key)) {
            if (previous == null)
                mostRecent = mostRecent.next;
            else
                previous.next = current.next;
            n--;
            return item;
        }
        return null;
    }

    public Iterator<Item> iterator() {
        return new CacheIterator();
    }

    private class CacheIterator implements Iterator<Item> {

        private Node current = mostRecent;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}

    }

}

class Main {
    static void test() {
        int n = 10;
        Cache<Integer> cache = new Cache<>();
        assert cache.isEmpty();
        assert cache.size() == 0;
        for (int i = 0; i < n; i++)
            cache.store(i);
        assert !cache.isEmpty();
        assert cache.size() == n;
        int counter = n;
        for (int item : cache)
            assert item == --counter;
        assert counter == 0;
        // Test cache refresh
        for (int i = 0; i < n; i++)
            cache.store(i);
        assert !cache.isEmpty();
        assert cache.size() == n;
        counter = n;
        for (int item : cache)
            assert item == --counter;
        assert counter == 0;

        cache.store(0);
        for (int item : cache) {
            assert item == 0;
            break;
        }
        cache.remove(5);
        for (int item : cache)
            assert item != 5;
    }

    public static void main(String[] args) {
        test();
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(Pattern.compile("\\p{javaWhitespace}+"));
        Cache<Character> cache = new Cache<>();
        while(scanner.hasNext()) {
            cache.store(scanner.next().charAt(0));
            System.out.print("Cache:");
            for (char item : cache)
                System.out.print(" " + item);
            System.out.println();
        }
    }
}
