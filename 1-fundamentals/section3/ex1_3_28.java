class Node {
    int item;
    Node next;

    public Node(int item) {
        this.item = item;
    }
}

class LinkedList {

    private int n;
    public Node first;

    public void append(int item) {
        this.append(new Node(item));
    }

    public void append(Node newNode) {
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
    }

}

class Main {
    static int max(Node first) {
        return max(first, 0);
    }

    static int max(Node first, int currentMax) {
        if (first == null)
            return currentMax;
        if (currentMax > first.item)
            return max(first.next, currentMax);
        return max(first.next, first.item);
    }

    static void test1() {
        LinkedList list = new LinkedList();
        assert max(list.first) == 0;
    }

    static void test2() {
        LinkedList list = new LinkedList();
        list.append(1);
        assert max(list.first) == 1;
    }

    static void test3() {
        LinkedList list = new LinkedList();
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.append(5);
        assert max(list.first) == 5;
    }

    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }
}
