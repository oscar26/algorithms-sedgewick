import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdIn;

class Main {
    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the k-th position");
        int k = Integer.parseInt(args[0]);
        Queue<String> queue = new Queue<>();
        while (!StdIn.isEmpty()) {
            queue.enqueue(StdIn.readLine());
            if (queue.size() == k + 1)
                queue.dequeue();
        }
        if (queue.size() == k)
            System.out.println(queue.peek());
        else
            System.out.println("Queue not filled");
    }
}
