/*
 * Steque API:
 *
 * class Steque<Item> implements Iterable<Item>
 *
 *              Steque()            create an empty steque
 *     boolean  isEmpty()           whether the steque is empty
 *         int  size()              number of items in the steque
 *        void  push(Item item)     add an item at the beginning
 *        void  enqueue(Item item)  add an item at the end
 *        Item  pop()               remove an item from the beginning
 * */

import java.util.Iterator;
import java.util.NoSuchElementException;

class Steque<Item> implements Iterable<Item> {

    private int n;
    private Node first;
    private Node last;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return first == null;  // or n == 0;
    }

    public void push(Item item) {
        Node newNode = new Node(item);
        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            newNode.next = first;
            first = newNode;
        }
        n++;
    }

    public void enqueue(Item item) {
        Node newNode = new Node(item);
        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        n++;
    }

    public Item pop() {
        if (isEmpty())
            throw new NoSuchElementException("steque is empty");
        Item item = first.item;
        first = first.next;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new StequeIterator();
    }

    private class StequeIterator implements Iterator<Item> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}

    }

}

class Main {
    static void testEnqueue() {
        Steque<Integer> steque = new Steque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.enqueue(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();

        for (int i = 1; i < 5; i++)
            steque.enqueue(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();

        int counter = 0;
        for (int item : steque) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert counter == 5;
    }

    static void testPush() {
        Steque<Integer> steque = new Steque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.push(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();

        for (int i = 1; i < 5; i++)
            steque.push(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();

        int counter = 4;
        for (int item : steque) {
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter--;
        }
        assert counter == -1;
    }

    static void testPop() {
        Steque<Integer> steque = new Steque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.push(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();
        assert steque.pop() == 0;
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.enqueue(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();
        assert steque.pop() == 0;
        assert steque.size() == 0;
        assert steque.isEmpty();

        for (int i = 0; i < 5; i++)
            steque.push(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
        int counter = 4;
        while (!steque.isEmpty()) {
            int item = steque.pop();
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter--;
        }
        assert steque.size() == 0;
        assert steque.isEmpty();
        assert counter == -1;

        for (int i = 0; i < 5; i++)
            steque.enqueue(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
        counter = 0;
        while (!steque.isEmpty()) {
            int item = steque.pop();
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert steque.size() == 0;
        assert steque.isEmpty();
        assert counter == 5;
    }

    public static void main(String[] args) {
        testEnqueue();
        testPush();
        testPop();
    }
}
