import edu.princeton.cs.algs4.Stack;

class Main {
    static boolean isBalanced(String parentheses) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < parentheses.length(); i++) {
            char ch = parentheses.charAt(i);
            // Push the closing pair to easily compare with future closing
            // tokens
            if (ch == '{') { stack.push('}'); }
            else if (ch == '[') { stack.push(']'); }
            else if (ch == '(') { stack.push(')'); }
            else if (stack.size() != 0) {
                // If a closing token is found, it must match the inner most
                // parenthesis token
                char innerMost = stack.pop();
                if (innerMost != ch)
                    return false;
            }
        }
        return stack.size() == 0;
    }

    public static void main(String[] args) {
        String[] testCases = {
            "[()]{}{[()()]()}",
            "[(])",
            "[()]{}{[()(])()}",
            ")(",
            "{[()]}",
            ""
        };
        boolean[] expectedValid = {
            true,
            false,
            false,
            false,
            true,
            true
        };
        for (int i = 0; i < testCases.length; i++)
            assert expectedValid[i] == isBalanced(testCases[i]) : "Failed case " + i;
    }
}
