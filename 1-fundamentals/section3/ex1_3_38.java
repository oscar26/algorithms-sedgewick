import java.util.NoSuchElementException;

interface GeneralizedQueue<Item> {
    boolean isEmpty();
    int size();
    void insert(Item item);
    Item delete(int k);
}

class GeneralizedQueueLinkedList<Item> implements GeneralizedQueue<Item> {

    private int n;
    private Node first;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return first == null;  // or n == 0;
    }

    public void insert(Item item) {
        Node newNode = new Node(item);
        if (first == null) {
            first = newNode;
        } else {
            Node currentNode = first;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = newNode;
        }
        n++;
    }

    public Item delete(int k) {
        if (k >= n)
            throw new NoSuchElementException();
        Item item;
        if (k == 0) {
            item = first.item;
            first = first.next;
        } else {
            Node currentNode = first;
            for (int i = 1; i < k; i++)
                currentNode = currentNode.next;
            item = currentNode.next.item;
            currentNode.next = currentNode.next.next;
        }
        n--;
        return item;
    }

}

class GeneralizedQueueArray<Item> implements GeneralizedQueue<Item> {

    private Item[] items;
    private int n;

    public GeneralizedQueueArray() {
        items = (Item[]) new Object[1];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    private void resize(int cap) {
        Item[] newArray = (Item[]) new Object[cap];
        for (int i = 0; i < n; i++)
            newArray[i] = items[i];
        items = newArray;
    }

    public void insert(Item item) {
        items[n++] = item;
        if (n == items.length)
            resize(2*items.length);
    }

    public Item delete(int k) {
        if (k >= n)
            throw new NoSuchElementException();
        Item item = items[k];
        for (int i = k+1; i < n; i++)
            items[i-1] = items[i];
        items[--n] = null;
        if (n > 0 && n == items.length/4)
            resize(items.length/2);
        return item;
    }

}

class Main {
    static void testStackBehaviour(GeneralizedQueue<Integer> queue) {
        assert queue.isEmpty();
        int n = 10;
        for (int i = 0; i < n; i++)
            queue.insert(i);
        assert !queue.isEmpty();
        assert queue.size() == n;
        for (int i = 0; i < n; i++)
            assert queue.delete(queue.size()-1) == (n-i-1);
        assert queue.isEmpty();
        assert queue.size() == 0;
    }

    static void testQueueBehaviour(GeneralizedQueue<Integer> queue) {
        assert queue.isEmpty();
        int n = 10;
        for (int i = 0; i < n; i++)
            queue.insert(i);
        assert !queue.isEmpty();
        assert queue.size() == n;
        for (int i = 0; i < n; i++)
            assert queue.delete(0) == i;
        assert queue.isEmpty();
        assert queue.size() == 0;
    }

    static void testMiddleDeletes(GeneralizedQueue<Integer> queue) {
        assert queue.isEmpty();
        int n = 10;
        for (int i = 0; i < n; i++)
            queue.insert(i);
        assert !queue.isEmpty();
        assert queue.size() == n;
        for (int i = 0; i < n; i++) {
            int item = queue.delete(queue.size()/2);
            int expectedItem = (int) (n + i*Math.pow(-1, i))/2;
            assert item == expectedItem;
        }
        assert queue.isEmpty();
        assert queue.size() == 0;
    }

    public static void main(String[] args) {
        testStackBehaviour(new GeneralizedQueueLinkedList<Integer>());
        testQueueBehaviour(new GeneralizedQueueLinkedList<Integer>());
        testMiddleDeletes(new GeneralizedQueueLinkedList<Integer>());

        testStackBehaviour(new GeneralizedQueueArray<Integer>());
        testQueueBehaviour(new GeneralizedQueueArray<Integer>());
        testMiddleDeletes(new GeneralizedQueueArray<Integer>());
    }
}
