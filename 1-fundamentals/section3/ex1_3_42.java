import java.util.Iterator;
import java.util.NoSuchElementException;

class Stack<Item> implements Iterable<Item> {

    private int n;
    private Node first;

    private class Node {
        Item item;
        Node next;

        public Node(Item item) {
            this.item = item;
        }
    }

    public Stack() {}

    public Stack(Stack<Item> original) {
        // A queue with an equal implementation of Node can also be used to
        // avoid creating a temporary stack to reverse the order
        if (!original.isEmpty()) {
            first = new Node(original.first.item);
            n = 1;
            Node previousCopy = first;
            Node currentOriginal = original.first.next;
            while (currentOriginal != null) {
                Node currentCopy = new Node(currentOriginal.item);
                previousCopy.next = currentCopy;
                previousCopy = currentCopy;
                currentOriginal = currentOriginal.next;
                n++;
            }
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return n;
    }

    public void push(Item item) {
        Node newNode = new Node(item);
        if (!isEmpty())
            newNode.next = first;
        first = newNode;
        n++;
    }

    public Item pop() {
        if (isEmpty())
            throw new NoSuchElementException("stack is empty");
        Item item = first.item;
        first = first.next;
        n--;
        return item;
    }

    public Iterator<Item> iterator() {
        return new QueueIterator();
    }

    private class QueueIterator implements Iterator<Item> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {}

    }
}

class Main {

    public static void main(String[] args) {
        int n = 10;
        Stack<String> original = new Stack<>();
        assert original.isEmpty();
        for (int i = 0; i < n; i++)
            original.push(String.valueOf(i));
        assert !original.isEmpty();
        assert original.size() == n;
        Stack<String> copy = new Stack<>(original);
        assert !copy.isEmpty();
        assert copy.size() == n;
        assert original != copy;
        for (int i = n-1; i >= 0; --i) {
            String originalStr = original.pop();
            assert original.size() == copy.size() - 1;
            String copyStr = copy.pop();
            assert original.size() == copy.size();
            assert originalStr.equals(copyStr);
            assert originalStr.equals(String.valueOf(i));
        }
        assert original.isEmpty();
        assert copy.isEmpty();
    }
}
