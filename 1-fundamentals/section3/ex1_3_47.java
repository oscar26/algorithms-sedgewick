import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;
import stdlib.Steque;

class Main {
    static <T> Queue<T> concatQueues(Queue<T> queue1, Queue<T> queue2) {
        Queue<T> result = new Queue<>();
        while (!queue1.isEmpty())
            result.enqueue(queue1.dequeue());
        while (!queue2.isEmpty())
            result.enqueue(queue2.dequeue());
        return result;
    }

    static <T> Stack<T> concatStacks(Stack<T> stack1, Stack<T> stack2) {
        Stack<T> reversedResult = new Stack<>();
        while (!stack1.isEmpty())
            reversedResult.push(stack1.pop());
        while (!stack2.isEmpty())
            reversedResult.push(stack2.pop());
        Stack<T> result = new Stack<>();
        while(!reversedResult.isEmpty())
            result.push(reversedResult.pop());
        return result;
    }

    static <T> Steque<T> concatSteques(Steque<T> steque1, Steque<T> steque2) {
        Steque<T> result = new Steque<>();
        while (!steque1.isEmpty())
            result.enqueue(steque1.pop());
        while (!steque2.isEmpty())
            result.enqueue(steque2.pop());
        for (T item : result)
            System.out.println(item);
        return result;
    }

    static void testConcatQueues() {
        Queue<Integer> queue1 = new Queue<>();
        Queue<Integer> queue2 = new Queue<>();
        int n1 = 5;
        int n2 = 5;
        for (int i = 0; i < n1; i++)
            queue1.enqueue(i);
        for (int i = n2; i < n1 + n2; i++)
            queue2.enqueue(i);
        Queue<Integer> queue3 = concatQueues(queue1, queue2);
        for (int i = 0; i < n1 + n2; i++)
            assert queue3.dequeue() == i;
    }

    static void testConcatStack() {
        Stack<Integer> stack1 = new Stack<>();
        Stack<Integer> stack2 = new Stack<>();
        int n1 = 5;
        int n2 = 5;
        for (int i = n2; i < n1 + n2; i++)
            stack1.push(i);
        for (int i = 0; i < n1; i++)
            stack2.push(i);
        Stack<Integer> stack3 = concatStacks(stack1, stack2);
        for (int i = n1+n2-1; i >= 0; i--)
            assert stack3.pop() == i;
    }

    static void testConcatSteques() {
        Steque<Integer> steque1 = new Steque<>();
        Steque<Integer> steque2 = new Steque<>();
        int n1 = 5;
        int n2 = 5;
        for (int i = 0; i < n1; i++)
            steque1.enqueue(i);
        for (int i = n2; i < n1 + n2; i++)
            steque2.enqueue(i);
        Steque<Integer> steque3 = concatSteques(steque1, steque2);
        for (int i = 0; i < n1+n2; i++)
            assert steque3.pop() == i;
    }

    public static void main(String[] args) {
        testConcatQueues();
        testConcatStack();
        testConcatSteques();
    }
}
