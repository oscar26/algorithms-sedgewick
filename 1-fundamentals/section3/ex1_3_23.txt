x.next = t;
t.next = x.next;


    Before

    ...  ->  x  ->  x+1  ->  ...



    x.next = t

             t
             ^
             |
    ...  ->  x      x+1  ->  ...



    t.next = x.next

             t --|
             ^   |
             |   |
    ...  ->  x <--    x+1  ->  ...


The nodes after x are lost as the original reference to x.next was not stored
before changing.
