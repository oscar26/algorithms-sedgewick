import edu.princeton.cs.algs4.StdOut;

/* Prints the fibonacci sequence:
 *
 *   i  f  g
 *   -------
 *   0  0  1
 *   1  1  0
 *   2  1  1
 *   3  2  1
 *   4  3  2
 *   5  5  3
 *   6  8  5
 *   .  .  .
 *   .  .  .
 *   .  .  .
 *
 *   */

class Main {
    public static void main(String[] args) {
        int f = 0;
        int g = 1;
        for (int i = 0; i <= 15; i++) {
            StdOut.println(f);
            f = f + g;
            g = f - g;
        }
    }
}
