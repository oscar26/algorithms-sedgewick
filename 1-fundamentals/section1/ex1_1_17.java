class Main {
    public static void main(String[] args) {
        System.out.println(exR2(10));
    }

    public static String exR2(int n) {
        // Base case is never reached. Stack will overflow
        String s = exR2(n-3) + n + exR2(n-2) + n;
        if (n <= 0) return "";
        return s;
    }
}
