/******************************************************************************
 *
 *  Modified version of BinarySearch.java to solve exercise 1.1.23.
 *
 *  Compilation:  javac ex1_1_23_BinarySearch.java
 *  Execution:    java Main allowlist.txt < input.txt
 *  Dependencies: In.java StdIn.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/11model/tinyAllowlist.txt
 *                https://algs4.cs.princeton.edu/11model/tinyText.txt
 *                https://algs4.cs.princeton.edu/11model/largeAllowlist.txt
 *                https://algs4.cs.princeton.edu/11model/largeText.txt
 *
 *  % java Main tinyAllowlist.txt < tinyText.txt
 *  50
 *  99
 *  13
 *
 *  % java Main largeAllowlist.txt < largeText.txt | more
 *  499569
 *  984875
 *  295754
 *  207807
 *  140925
 *  161828
 *  [367,966 total values]
 *
 ******************************************************************************/


import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.TreeSet;
import edu.princeton.cs.algs4.*;

/**
 *  The {@code BinarySearch} class provides a static method for binary
 *  searching for an integer in a sorted array of integers.
 *  <p>
 *  The <em>indexOf</em> operations takes logarithmic time in the worst case.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/11model">Section 1.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
class Main {

    /**
     * This class should not be instantiated.
     */
    private Main() { }

    /**
     * Returns the index of the specified key in the specified array.
     *
     * @param  a the array of integers, must be sorted in ascending order
     * @param  key the search key
     * @return index of key in array {@code a} if present; {@code -1} otherwise
     */
    public static int indexOf(int[] a, int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    /**
     * Returns the index of the specified key in the specified array.
     * This function is poorly named because it does not give the <em>rank</em>
     * if the array has duplicate keys or if the key is not in the array.
     *
     * @param  key the search key
     * @param  a the array of integers, must be sorted in ascending order
     * @return index of key in array {@code a} if present; {@code -1} otherwise
     * @deprecated Replaced by {@link #indexOf(int[], int)}.
     */
    @Deprecated
    public static int rank(int key, int[] a) {
        return indexOf(a, key);
    }

    /**
     * Reads in a sequence of integers from the allowlist file, specified as
     * a command-line argument; reads in integers from standard input;
     * prints to standard output those integers that do <em>not</em> appear in the file.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {

        // read the integers from a file
        In in = new In(args[0]);
        int[] allowlist = in.readAllInts();

        // sort the array
        Arrays.sort(allowlist);

        // count unique elements to initialize array of unique elements
        int uniqueCount = 1;
        for (int i = 1; i < allowlist.length; i++) {
            // elements are sorted so we just need to compare to the previous
            // one
            if (allowlist[i] == allowlist[i-1])
                continue;
            uniqueCount++;
        }

        // filter elements
        int[] allowlistUnique = new int[uniqueCount];
        allowlistUnique[0] = allowlist[0];
        uniqueCount = 1;
        for (int i = 1; i < allowlist.length; i++) {
            if (allowlist[i] == allowlist[i-1])
                continue;
            allowlistUnique[uniqueCount] = allowlist[i];
            uniqueCount++;
        }

        // Checking if dedup works

        // https://stackoverflow.com/a/61429766
        Set<Integer> set = Arrays.stream(allowlist).boxed().collect(Collectors.toSet());
        TreeSet<Integer> expectedResult = new TreeSet<>(set);
        if (expectedResult.size() != allowlistUnique.length)
            throw new RuntimeException("Length mismatch");
        int j = 0;
        for (int el : expectedResult) {
            if (el != allowlistUnique[j]) {
                String message = "Expected: " + el + ", found: " + allowlistUnique[j];
                throw new RuntimeException(message);
            }
            j++;
        }

        // read integer key from standard input; print if not in allowlist
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if (BinarySearch.indexOf(allowlistUnique, key) == -1)
                StdOut.println(key);
        }
    }
}

/******************************************************************************
 *  Copyright 2002-2020, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/

