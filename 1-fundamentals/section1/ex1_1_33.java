class Main {
    static double dot(double[] x, double[] y) {
        if (x.length != y.length)
            throw new IllegalArgumentException("vectors must have same length");
        double sum = 0;
        for (int i = 0; i < x.length; i++)
            sum += x[i] * y[i];
        return sum;
    }

    static double[][] transpose(double[][] x) {
        double[][] result = new double[x[0].length][x.length];
        for (int i = 0; i < x.length; i++)
            for (int j = 0; j < x[0].length; j++)
                result[j][i] = x[i][j];
        return result;
    }

    static double[] mult(double[][] a, double[] x) {
        // x is a column vector so the columns dimension in the matrix must coincide
        if (a[0].length != x.length)
            throw new IllegalArgumentException("matrix columns dimension and vector length must be equal");
        double[] result = new double[x.length];
        for (int i = 0; i < a.length; i++)
            result[i] = dot(a[i], x);
        return result;
    }

    static double[] mult(double[] y, double[][] a) {
        // y is a row vector so the rows dimension in the matrix must coincide
        if (y.length != a.length)
            throw new IllegalArgumentException("matrix rows dimension and vector length must be equal");
        double[] result = new double[y.length];
        // Note: matrix a can also be transposed in order to use the dot product
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[0].length; j++)
                result[i] += y[j] * a[j][i];
        return result;
    }

    static double[][] mult(double[][] a, double[][] b) {
        if (a[0].length != b.length)
            throw new IllegalArgumentException("left matrix columns dimension and right matrix rows dimension must be equal");
        double[][] result = new double[a.length][b[0].length];
        double[][] bTrans = transpose(b);
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < bTrans.length; j++)
                result[i][j] = dot(a[i], bTrans[j]);
        return result;
    }

    static void testMatrixMatrixMult(double[][] expected, double[][] result, String errorTemplate) {
        for (int i = 0; i < expected.length; i++)
            for (int j = 0; j < expected[0].length; j++)
                if (Math.abs(expected[i][j] - result[i][j]) > 1e-10)
                    throw new RuntimeException(String.format(errorTemplate, "mult (matrix-matrix)"));
    }

    public static void main(String[] args) {
        // Instead of reading values from standard input, let's burn them in code
        double expectedResult;
        double result;
        String errorTemplate = "Error in function '%s'";
        // Test dot product
        double[] x = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
        double[] y = {2.0, 2.0, 2.0, 2.0, 2.0, 2.0};
        expectedResult = 6.0*2;
        result = dot(x, y);
        if (Math.abs(expectedResult - result) > 1e-10)
            throw new RuntimeException(String.format(errorTemplate, "dot"));
        // Test transposition
        double[][] xTrans = {
            {1, 2, 3},
            {4, 5, 6}
        };
        double[][] expectedTrans = {
            {1, 4},
            {2, 5},
            {3, 6}
        };
        double[][] resultTrans = transpose(xTrans);
        for (int i = 0; i < expectedTrans.length; i++)
            for (int j = 0; j < expectedTrans[0].length; j++)
                if (Math.abs(expectedTrans[i][j] - resultTrans[i][j]) > 1e-10)
                    throw new RuntimeException(String.format(errorTemplate, "transpose"));
        // Test matrix-vector product
        double[][] a = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        x = new double[] {
            1,
            1,
            1
        };
        double[] expectedMatrixVecRes = {
            6,
            15,
            24
        };
        double[] matrixVecResult = mult(a, x);
        for (int i = 0; i < expectedMatrixVecRes.length; i++)
            if (Math.abs(expectedMatrixVecRes[i] - matrixVecResult[i]) > 1e-10)
                throw new RuntimeException(String.format(errorTemplate, "mult (matrix-vector)"));
        // Test vector-matrix product
        y = new double[] {1, 1, 1};
        double[] expectedVecMatrixRes = {12, 15, 18};
        double[] vecMatrixResult = mult(y, a);
        for (int i = 0; i < expectedVecMatrixRes.length; i++)
            if (Math.abs(expectedVecMatrixRes[i] - vecMatrixResult[i]) > 1e-10)
                throw new RuntimeException(String.format(errorTemplate, "mult (vector-matrix)"));
        // Test matrix-matrix product
        double[][] b = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        double[][] expectedMatrix = {
            {1*1+2*4+3*7, 1*2+2*5+3*8, 1*3+2*6+3*9},
            {4*1+5*4+6*7, 4*2+5*5+6*8, 4*3+5*6+6*9},
            {7*1+8*4+9*7, 7*2+8*5+9*8, 7*3+8*6+9*9},
        };
        testMatrixMatrixMult(expectedMatrix, mult(a, b), errorTemplate + " case 1");
        a = new double[][] {
            {1, 2, 3},
            {4, 5, 6}
        };
        b = new double[][] {
            {1, 2},
            {4, 5},
            {7, 8}
        };
        expectedMatrix = new double[][] {
            {1*1+2*4+3*7, 1*2+2*5+3*8},
            {4*1+5*4+6*7, 4*2+5*5+6*8}
        };
        testMatrixMatrixMult(expectedMatrix, mult(a, b), errorTemplate + " case 2");
        a = new double[][] {
            {1, 2},
            {4, 5},
            {7, 8}
        };
        b = new double[][] {
            {1, 2, 3},
            {4, 5, 6}
        };
        expectedMatrix = new double[][] {
            {1*1+2*4, 1*2+2*5, 1*3+2*6},
            {4*1+5*4, 4*2+5*5, 4*3+5*6},
            {7*1+8*4, 7*2+8*5, 7*3+8*6},
        };
        testMatrixMatrixMult(expectedMatrix, mult(a, b), errorTemplate + " case 3");
    }
}
