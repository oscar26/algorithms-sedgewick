class Main {
    public static void main(String[] args) {
        System.out.println((1+2.236)/2);  // double: 1.618
        System.out.println(1+2+3+4.0);  // double: 10.0
        System.out.println(4.1 >= 4);  // bool: true
        System.out.println(1+2+"3");  // String: "33"
    }
}

