import edu.princeton.cs.algs4.StdIn;

class Main {
    public static void main(String[] args) {
        int N = StdIn.readInt();
        System.out.println(lg(N));
    }

    static int lg(int N) {
        int exponent = 0;
        int power = 1;
        while (power < N) {
            power *= 2;
            exponent++;
        }
        // Equivalent to truncating the decimals from the real-valued logarithm
        if (N % power != 0) exponent--;
        // Return the largest integer not larger than the base-2 logarithm of N
        return exponent;
    }
}
