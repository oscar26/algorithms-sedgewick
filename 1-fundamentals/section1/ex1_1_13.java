import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int rows = ThreadLocalRandom.current().nextInt(1, 21);
        int cols = ThreadLocalRandom.current().nextInt(1, 21);
        boolean[][] a = new boolean[rows][cols];
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[0].length; j++)
                a[i][j] = random.nextBoolean();
        StdOut.println("Original matrix");
        printBooleanMatrix(a);
        StdOut.println("Transposed matrix");
        printBooleanMatrixTransposed(a);
    }

    static void printBooleanMatrix(boolean[][] a) {
        int padding = ((int) Math.log10(a[0].length)) + 2;
        StdOut.printf("%" + (padding+2) + "d", 0);
        for (int i = 1; i < a[0].length; i++)
            StdOut.printf("%" + padding + "d", i);
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (j == 0)
                    StdOut.printf("\n%2d", i);
                if (a[i][j])
                    StdOut.printf("%" + padding + "s", "*");
                else
                    StdOut.printf("%" + padding + "s", " ");
            }
        }
        StdOut.println("\n");
    }

    static void printBooleanMatrixTransposed(boolean[][] a) {
        int padding = ((int) Math.log10(a.length)) + 2;
        StdOut.printf("%" + (padding+2) + "d", 0);
        for (int j = 1; j < a.length; j++)
            StdOut.printf("%" + padding + "d", j);
        for (int j = 0; j < a[0].length; j++) {
            for (int i = 0; i < a.length; i++) {
                if (i == 0)
                    StdOut.printf("\n%2d", j);
                if (a[i][j])
                    StdOut.printf("%" + padding + "s", "*");
                else
                    StdOut.printf("%" + padding + "s", " ");
            }
        }
        StdOut.println("\n");
    }
}
