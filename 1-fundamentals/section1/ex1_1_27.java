class Main {

    static long count = 0;

    public static void main(String[] args) {
        // Unfortunately, the estimates were defined by trial and error
        // counting the calls to the function as the logic for why they work
        // was not clear to me as the recursive function is somewhat
        // complicated.
        //
        // When n < k or n == k, the estimations seem exact (no proof
        // provided). When n > k, the estimation I make is between 2^(k+2) and
        // 2^(n+2).
        //
        // For n=100 and k=50 the estimation would be between 2^52 and 2^102.

        int n = Integer.parseInt(args[0]);
        int k = Integer.parseInt(args[1]);
        count = 0;
        System.out.println(binomialFast(n, k, 0.5));
        count = 0;
        System.out.println(binomial(n, k, 0.5));
        System.out.printf("Count: %d, Estimate: %d\n", count, estimate(n, k));

        // count = 0;
        // System.out.println(binomialFast(100, 50, 0.5));
        // System.out.println(estimate(100, 50));
        // System.out.println(count);

        // System.out.println(binomialCount(100, 50));

        // Checking if estimates for n <= k < 21 hold
        for (int i = 0; i < 21; ++i) {
            for(int j = 0; j <= i; ++j) {
                long realCount = countBinomial(j, i);
                long est = estimate(j, i);
                if (realCount != est) {
                    System.out.printf("n=%d k=%d, count: %d  estimate: %d\n", j, i, realCount, est);
                }
            }
        }
    }

    static double binomial(int N, int k, double p) {
        count++;
        // Fixes from the errata:
        //     https://algs4.cs.princeton.edu/errata/errata-printing1.php
        if (N == 0 && k == 0) return 1.0;
        if (N < 0 || k < 0) return 0.0;
        return (1.0 - p)*binomial(N-1, k, p) + p*binomial(N-1, k-1, p);
    }

    static double binomialFast(int N, int k, double p) {
        double[][] memory = new double[N+1][k+1];
        for(int j = 0; j < k+1; ++j)
            for (int i = 0; i < N+1; ++i)
                memory[i][j] = -1.0;
        return binomialMemory(N, k, p, memory);
    }

    static double binomialMemory(int N, int k, double p, double[][] memory) {
        count++;
        if (N == 0 && k == 0) return 1.0;
        if (N < 0 || k < 0) return 0.0;
        if (memory[N][k] > -1.0)
            return memory[N][k];
        memory[N][k] = (1.0 - p)*binomialMemory(N-1, k, p, memory) + p*binomialMemory(N-1, k-1, p, memory);
        return memory[N][k];
    }

    static long countBinomial(int n, int k) {
        count = 1;
        binomialCount(n, k);
        return count;
    }

    static double binomialCount(int N, int k) {
        count++;
        if (N == 0 && k == 0) return 0.0;
        if (N < 0 || k < 0) return 0.0;
        return binomialCount(N-1, k) + binomialCount(N-1, k-1);
    }

    static long estimate(int n, int k) {
        if (n > k)
            return (long) Math.pow(2, n+2);
        else if (n < k)
            return (long) Math.pow(2, n+2);
        else
            return (long) Math.pow(2, n+2) - 2;
    }
}
