import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

class Main {

    static int SIDES = 6;

    static double[] sum2DiceProbDist() {
        double[] dist = new double[2*SIDES+1];
        for (int i = 1; i <= SIDES; i++)
            for (int j = 1; j <= SIDES; j++)
                dist[i+j] += 1.0;
        for (int k = 2; k <= 2*SIDES; k++)
            dist[k] /= 36.0;
        return dist;
    }

    static double[] simulateSum2Dice(int n) {
        double[] simulatedDist = new double[2*SIDES+1];
        for (int k = 0; k < n; k++) {
            int i = StdRandom.uniform(1, 7);
            int j = StdRandom.uniform(1, 7);
            simulatedDist[i+j] += 1.0;
        }
        for (int k = 1; k < simulatedDist.length; k++)
            simulatedDist[k] /= n*1.0;
        return simulatedDist;
    }

    static boolean closeDistributions(double[] dist1, double[] dist2, double tolerance) {
        for (int i = 1; i < dist1.length; i++)
            if (Math.abs(dist1[i] - dist2[i]) >= tolerance)
                return false;
        return true;
    }

    public static void main(String[] args) {
        System.out.println("This takes a long time...\n");
        int nSims = 100;
        int[] firstNDistMatched = new int[nSims];
        double[] theoreticalDist = sum2DiceProbDist();
        for (int i = 0; i < nSims; i++) {
            if ((i+1) % 10 == 0)
                System.out.println("Experiment #" + (i+1));
            // 1e7 is a hard limit on how many simulations to run
            for (int j = 1; j < 1e5; j++) {
                double[] empiricalDist = simulateSum2Dice(j);
                if (closeDistributions(theoreticalDist, empiricalDist, 1e-3)) {
                    firstNDistMatched[i] = j;
                    break;
                }
            }
        }
        double max = StdStats.max(firstNDistMatched);
        double min = StdStats.min(firstNDistMatched);
        double mean = StdStats.mean(firstNDistMatched);
        double var = StdStats.var(firstNDistMatched);
        double stddev = StdStats.stddev(firstNDistMatched);
        System.out.println(
              "\nStatistics about N, where N is the number of simulated dice"
            + "\nthrows to match the theorectical probability distribution"
            + "\nup to 3 decimal places\n"
        );
        System.out.printf(
              "  Max:      %.2f\n"
            + "  Min:      %.2f\n"
            + "  Mean:     %.2f\n"
            + "  Variance: %.2f\n"
            + "  Std. Dev: %.2f\n\n",
            max, min, mean, var, stddev
        );
    }
}
