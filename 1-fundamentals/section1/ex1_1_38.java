/*
 * As binary search needs a sorted array, I include the time for doing so to be
 * more fair.
 *
 * Brute search's output is compared to the binary search's output to check
 * both coincide before measuring time.
 *
 * Times:
 *   Brute force 185.685682088 seconds
 *   Binary (including sort) 1.308257593 seconds
 *   Binary (not including sort) 0.285957666 seconds
 * */

import java.util.Arrays;
import edu.princeton.cs.algs4.BinarySearch;
import edu.princeton.cs.algs4.In;

class Main {
    static int linearSearch(int[] a, int key) {
        for (int i = 0; i < a.length; i++)
            if (a[i] == key) return i;
        return -1;
    }

    public static void main(String[] args) {
        // read the integers from two files
        In in = new In(args[0]);
        int[] listToSearch = in.readAllInts();
        in = new In(args[1]);
        int[] keysList = in.readAllInts();
        // Brute force search
        long startTime = System.nanoTime();
        for (int i = 0; i < keysList.length; i++)
            linearSearch(listToSearch, keysList[i]);
        long stopTime = System.nanoTime();
        System.out.println("Brute force " + ((stopTime - startTime)/1.0e9) + " seconds");
        // Binary search
        long startSortTime = System.nanoTime();
        Arrays.sort(listToSearch);
        startTime = System.nanoTime();
        for (int i = 0; i < keysList.length; i++)
            BinarySearch.indexOf(listToSearch, keysList[i]);
        stopTime = System.nanoTime();
        System.out.println("Binary (including sort) " + ((stopTime - startSortTime)/1.0e9) + " seconds");
        System.out.println("Binary (not including sort) " + ((stopTime - startTime)/1.0e9) + " seconds");
    }
}
