class Main {
    public static void main(String[] args) {
        String format = "Position of element %d: %d\n\n";
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = -1; i < 11; i++) {
            System.out.println("Depth: [low, high]");
            System.out.printf(format, i, rank(i, array));
        }
    }

    public static int rank(int key, int[] a) {
        return rank(key, a, 0, a.length - 1, 0);
    }

    public static int rank(int key, int[] a, int lo, int hi, int trace) {
        System.out.printf("%" + (trace+1) + "s: [%d, %d]\n", trace, lo, hi);
        if (lo > hi) return -1;
        int mid = lo + (hi - lo)/2;
        if (key < a[mid]) return rank(key, a, lo, mid - 1, trace + 1);
        else if (key > a[mid]) return rank(key, a, mid + 1, hi, trace + 1);
        else return mid;
    }
}
