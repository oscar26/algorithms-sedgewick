/* Example from the java/ folder
 *
 * % ./javarun.sh 1-fundamentals/section1/ex1_1_32.java 10 0 100 < ../algs4-data/largeText.txt
 *
 * Axes' text gets crammed if magnitudes are too high.
 *
 * */

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdIn;

class Main {
    public static void main(String[] args) {
        if (args.length != 3) {
            String message = "Pass the number of bins and the left and right boundaries";
            throw new IllegalArgumentException(message);
        }
        int n = Integer.parseInt(args[0]);
        double l = Double.parseDouble(args[1]);
        double r = Double.parseDouble(args[2]);
        if (n < 1)
            throw new IllegalArgumentException("Mininum number of bins is 1");
        if (l > r)
            throw new IllegalArgumentException("Left boundary must be less than or equal to the right boundary");
        double[] binTicks = new double[n+1];
        for (int i = 0; i < binTicks.length; i++)
            binTicks[i] = l + i*(r-l)/n;
        double[] numbers = StdIn.readAllDoubles();
        // double[] numbers = new double[]{ 0.1, 1.1, 1.2, 2.3, 4.5, 6.5 };
        // double[] numbers = new double[26];
        // for (int i = 0; i < numbers.length; i++)
        //     numbers[i] = 4.5;
        int[] counts = histogram(binTicks, numbers);
        double[] origin = new double[] { 0.1, 0.1 };
        double xAxisLength = 1.0 - origin[0]*2;
        double yAxisLength = 1.0 - origin[1]*2;
        drawAxes(origin, xAxisLength, yAxisLength);
        drawBins(origin, xAxisLength, yAxisLength, binTicks, counts);
    }

    static int[] histogram(double[] binTicks, double[] numbers) {
        int[] counts = new int[binTicks.length-1];
        for (int i = 0; i < numbers.length; i++) {
            double number = numbers[i];
            // Don't count numbers outside the user-specified limits
            if (number <= binTicks[0] || number > binTicks[binTicks.length-1])
                continue;
            int low = 0;
            int high = binTicks.length - 1;
            while (high - low > 1) {
                int mid = low + (high - low)/2;
                // Inclusive on the right side
                if (number <= binTicks[mid]) high = mid;
                else low = mid;
                // System.out.printf(
                //     "%.2f < %.2f <= %.2f, %d %d %d\n",
                //     binTicks[low], number, binTicks[high], low, mid, high
                // );
            }
            counts[low]++;
        }
        return counts;
    }

    static void drawAxes(double[] origin, double xAxisLength, double yAxisLength) {
        StdDraw.line(origin[0], origin[1], origin[0] + xAxisLength, origin[1]);
        StdDraw.line(origin[0], origin[1], origin[0], origin[1] + yAxisLength);
    }

    static void drawBins(double[] origin, double xAxisLength, double yAxisLength, double[] binTicks, int[] counts) {
        // TODO: resize windows and font according to the magnitude of the
        // ticks to avoid crammed text
        int maxCount = -1;
        for (int i = 0; i < counts.length; i++)
            if (counts[i] > maxCount) maxCount = counts[i];
        if (maxCount == 0)  // At least draw the ticks
            maxCount++;
        double tickLineLength = 0.02;
        // Draw y axis ticks
        double yRectScale = 0.9;
        int yTicks = Math.min(10, maxCount);
        int yDelta = (int) Math.round(1.0*maxCount/yTicks);
        double maxCountLength = yRectScale * yAxisLength;
        double yIncrement = yDelta*maxCountLength/maxCount;
        double yLabelLeftMargin = 0.05;
        for (int i = 0; i < yTicks; i++) {
            double yPos = origin[1] + (i+1)*yIncrement;
            StdDraw.line(origin[0], yPos, origin[0] - tickLineLength, yPos);
            StdDraw.text(origin[0] - yLabelLeftMargin, yPos, String.format("%d", (i+1)*yDelta));
            if (yPos > yAxisLength)  // Prevent drawing ticks outside the axis
                break;
        }
        // Draw bins and x ticks
        double xLabelTopMargin = 0.05;
        StdDraw.text(origin[0], origin[1] - xLabelTopMargin, String.format("%.2f", binTicks[0]));
        double xIncrement = xAxisLength/(binTicks.length-1);
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > 0) {
                double rectHeight = (maxCountLength * counts[i]/maxCount)/2;
                // Rectangles are drawn with an anchor point at the center
                double x = origin[0] + i*xIncrement + xIncrement/2;
                double y = origin[1] + rectHeight;
                double rw = xIncrement/2;
                double rh = rectHeight;
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.filledRectangle(x, y, rw, rh);
                StdDraw.setPenColor(StdDraw.BLACK);
                StdDraw.rectangle(x, y, rw, rh);
            }
            double xPos = origin[0] + (i+1)*xIncrement;
            // Draw x axis ticks
            StdDraw.line(xPos, origin[1], xPos, origin[1] - tickLineLength);
            StdDraw.text(xPos, origin[1] - xLabelTopMargin, String.format("%.1f", binTicks[i+1]));
        }
    }
}
