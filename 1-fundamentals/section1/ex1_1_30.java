class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Pass only the size of the matrix");
            System.exit(1);
        }
        int n = Integer.parseInt(args[0]);
        boolean[][] relatives = relativePrimes(n);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                System.out.printf("(%d, %d): %s\n", i+1, j+1, relatives[i][j]);
    }

    public static boolean[][] relativePrimes(int n) {
        // This can be optimized by noting that gcd(p, q) == gcd(q, p) for p
        // and q integers
        boolean[][] relatives = new boolean[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                relatives[i][j] = gcd(i+1, j+1) == 1;
        return relatives;
    }

    public static int gcd(int p, int q) {
        if (q == 0) return p;
        int r = p % q;
        return gcd(q, r);
    }
}
