import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        int N = StdIn.readInt();
        StdOut.println(mySolution(N));
        StdOut.println(bookSolution(N));
        // String s1, s2;
        // for (int i = 0; i <= 10000000; i++) {
        //     s1 = mySolution(i);
        //     s2 = bookSolution(i);
        //     if (!s1.equals(s2)) {
        //         StdOut.println(i);
        //         break;
        //     }
        // }
    }

    static String mySolution(int N) {
        String s = "";
        for (int i = (int)(Math.log(N)/Math.log(2)); i >= 0; i--)
            if (((1 << i) & N) != 0)
                s += "1";
            else
                s += "0";
        return s;
    }

    static String bookSolution(int N) {
        String s = "";
        for (int n = N; n > 0; n /= 2)
            s = (n % 2) + s;
        return s;
    }
}
