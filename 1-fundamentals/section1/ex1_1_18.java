class Main {
    public static void main(String[] args) {
        System.out.println(mystery_plus(2, 25));
        System.out.println(mystery_plus(3, 11));
        System.out.println(mystery_mult(2, 25));
        System.out.println(mystery_mult(3, 11));
    }

    public static int mystery_plus(int a, int b) {
        /*
         * | mystery_plus(2, 25) = mystery_plus(4, 12) + 2
         * | mystery_plus(4, 12) = mystery_plus(8, 6)
         * | mystery_plus(8, 6)  = mystery_plus(16, 3)
         * | mystery_plus(16, 3) = mystery_plus(32, 1) + 16
         * | mystery_plus(32, 1) = mystery_plus(64, 0) + 32
         * v mystery_plus(64, 0) = 0
         *
         * ^ mystery_plus(2, 25) = 48 +  2 = 50
         * | mystery_plus(4, 12) = 48
         * | mystery_plus(8, 6)  = 48
         * | mystery_plus(16, 3) = 32 + 16 = 48
         * | mystery_plus(32, 1) =  0 + 32 = 32
         * | mystery_plus(64, 0) =  0
         *
         * --------------------------
         *
         * | mystery_plus(3, 11) = mystery_plus(6, 5)  + 3
         * | mystery_plus(6, 5)  = mystery_plus(12, 2) + 6
         * | mystery_plus(12, 2) = mystery_plus(24, 1)
         * | mystery_plus(24, 1) = mystery_plus(48, 0) + 24
         * v mystery_plus(48, 0) = 0
         *
         * ^ mystery_plus(3, 11) = 30 +  3 = 33
         * | mystery_plus(6, 5)  = 24 +  6 = 30
         * | mystery_plus(12, 2) =           24
         * | mystery_plus(24, 1) =  0 + 24 = 24
         * | mystery_plus(48, 0) =  0
         *
         * */
        if (b == 0) return 0;
        if (b % 2 == 0) return mystery_plus(a+a, b/2);
        return mystery_plus(a+a, b/2) + a;
    }

    public static int mystery_mult(int a, int b) {
        /*
         * | mystery_plus(2, 25) = mystery_plus(4, 12) * 2
         * | mystery_plus(4, 12) = mystery_plus(8, 6)
         * | mystery_plus(8, 6)  = mystery_plus(16, 3)
         * | mystery_plus(16, 3) = mystery_plus(32, 1) * 16
         * | mystery_plus(32, 1) = mystery_plus(64, 0) * 32
         * v mystery_plus(64, 0) = 1
         *
         * ^ mystery_plus(2, 25) = 512 *  2 = 1024
         * | mystery_plus(4, 12) = 512
         * | mystery_plus(8, 6)  = 512
         * | mystery_plus(16, 3) = 32  * 16 = 512
         * | mystery_plus(32, 1) =  1  * 32 = 32
         * | mystery_plus(64, 0) =  1
         *
         * --------------------------
         *
         * | mystery_plus(3, 11) = mystery_plus(6, 5)  * 3
         * | mystery_plus(6, 5)  = mystery_plus(12, 2) * 6
         * | mystery_plus(12, 2) = mystery_plus(24, 1)
         * | mystery_plus(24, 1) = mystery_plus(48, 0) * 24
         * v mystery_plus(48, 0) = 0
         *
         * ^ mystery_plus(3, 11) = 144 *  3 = 432
         * | mystery_plus(6, 5)  = 24  *  6 = 144
         * | mystery_plus(12, 2) =            24
         * | mystery_plus(24, 1) =   1 * 24 = 24
         * | mystery_plus(48, 0) =   1
         *
         * */
        if (b == 0) return 1;
        if (b % 2 == 0) return mystery_mult(a+a, b/2);
        return mystery_mult(a+a, b/2) * a;
    }
}
