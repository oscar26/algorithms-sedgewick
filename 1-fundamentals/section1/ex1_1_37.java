/*
 * Let m be an integer denoting the size of the array. The naive algorithm has
 * m^m possible shuffles while the total number of permutations is m!. One
 * condition for proper unbiased shuffling is that the total permutations must
 * divide the number of possible shuffles/outcomes, which is violated and,
 * therefore, some outcomes will be favoured over others (biased). A rigorous
 * proof would need knowledge of combinatorics and number theory.
 *
 * For an example, let m = 3 (an array from 1 to 3). Then m^m = 3^3 = 27
 * possible shuffles/outcomes and m! = 3! = 6 total permutations. Note that 6
 * does not divide 27 and thus some shuffles will happen with more (or less)
 * frequency. For this particular example, after laying out the outcomes tree
 * and counting, the frequencies would be,
 *
 *   1-2-3 = 4 times
 *   1-3-2 = 5 times
 *   2-1-3 = 5 times
 *   2-3-1 = 5 times
 *   3-1-2 = 4 times
 *   3-2-1 = 4 times
 *   ---------------
 *           7 times
 *
 * See more,
 *   https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Na%C3%AFve_method
 *   https://blog.codinghorror.com/the-danger-of-naivete/
 *
 * */

import edu.princeton.cs.algs4.StdRandom;

class Main {
    static int[] initArray(int m) {
        int[] array = new int[m];
        for (int i = 0; i < m; i++)
            array[i] = i;
        return array;
    }

    static void shuffle(int[] a) {
        int N = a.length;
        for (int i = 0; i < N; i++) {
            // Exchange a[i] with random element in a[0..N-1]
            int r = StdRandom.uniform(N);
            int temp = a[i];
            a[i] = a[r];
            a[r] = temp;
        }
    }

    public static void main(String[] args) {
        int m = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        int[][] count = new int[m][m];
        int max = 0;
        for (int k = 0; k < n; k++) {
            int[] a = initArray(m);
            shuffle(a);
            for (int j = 0; j < m; j++) {
                count[a[j]][j]++;
                if (count[a[j]][j] > max)
                    max = count[a[j]][j];
            }
        }
        System.out.printf("N/M: %.3f", (1.0*n)/m);
        int padding = ((int) Math.log10(max)) + 2;
        for (int i = 0; i < m; i++) {
            System.out.println();
            for (int j = 0; j < m; j++)
                System.out.printf("%"+padding+"d", count[i][j]);
        }
        System.out.println();
    }
}

