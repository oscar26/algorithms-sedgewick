class Main {
    public static void main(String[] args) {
        System.out.println('b');               // a. Prints "b"
        System.out.println('b' + 'c');         // b. Prints 197, the sum of the ASCII values of both letters
        System.out.println((int) 'b');
        System.out.println((int) 'c');
        System.out.println((char) ('a' + 4));  // c. Prints "e", the sum of the ASCII value of 'a' plus 4 converted to char
    }
}
