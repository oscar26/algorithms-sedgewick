class Main {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        int c;
        // if (a > b) then c = 0;        // "then" is not a valid word in Java
        // if a > b { c = 0; }           // Condition is not enclosed in parenthesis
        // if (a > b) c = 0;             // It's fine
        // if (a > b) c = 0 else b = 0;  // ";" missing after "c = 0"
    }
}
