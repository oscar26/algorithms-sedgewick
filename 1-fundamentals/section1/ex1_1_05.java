import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        double x = StdIn.readDouble();
        double y = StdIn.readDouble();
        boolean xWithin = x > 0 && x < 1;
        boolean yWithin = y > 0 && y < 1;
        if (xWithin && yWithin)
            StdOut.println("true");
        else
            StdOut.println("false");
    }
}
