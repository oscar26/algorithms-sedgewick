import java.time.Instant;
import java.util.HashMap;
import edu.princeton.cs.algs4.StdOut;

class Main {

    static HashMap<Integer, Long> memory = new HashMap<>();

    public static void main(String[] args) {
        // Either N=58 or N=59 would execute under an hour
        // estimateFibTime();
        long startTime, endTime, result, executionTime;
        for (int N = 0; N < 100; N++) {
            startTime = Instant.now().toEpochMilli();
            // result = F(N);
            result = fastaFib(N);
            endTime = Instant.now().toEpochMilli();
            executionTime = endTime - startTime;
            StdOut.println(N + " " + result + "; Time: " + executionTime + " ms");
        }
    }

    public static long F(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        return F(N-1) + F(N-2);
    }

    public static long fastaFib(int N) {
        if (N == 0) memory.putIfAbsent(N, 0l);
        if (N == 1) memory.putIfAbsent(N, 1l);
        if (memory.containsKey(N)) return memory.get(N);
        long result = fastaFib(N-1) + fastaFib(N-2);
        memory.put(N, result);
        return result;
    }

    public static void estimateFibTime() {
        long time = 20000;
        for (int i = 49; i < 100; i++) {
            time *= 1.61;
            StdOut.println(i + " " + time + " ms");
            if (time > 3600000) break;
        }
    }
}
