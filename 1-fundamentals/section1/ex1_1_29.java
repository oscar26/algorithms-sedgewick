/******************************************************************************
 *
 *  Modified version of BinarySearch.java to solve exercise 1.1.23.
 *
 *  Compilation:  javac ex1_1_23_BinarySearch.java
 *  Execution:    java Main allowlist.txt < input.txt
 *  Dependencies: In.java StdIn.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/11model/tinyAllowlist.txt
 *                https://algs4.cs.princeton.edu/11model/tinyText.txt
 *                https://algs4.cs.princeton.edu/11model/largeAllowlist.txt
 *                https://algs4.cs.princeton.edu/11model/largeText.txt
 *
 *  % java Main tinyAllowlist.txt < tinyText.txt
 *  50
 *  99
 *  13
 *
 *  % java Main largeAllowlist.txt < largeText.txt | more
 *  499569
 *  984875
 *  295754
 *  207807
 *  140925
 *  161828
 *  [367,966 total values]
 *
 ******************************************************************************/


import java.util.Arrays;
import edu.princeton.cs.algs4.*;

/**
 *  The {@code BinarySearch} class provides a static method for binary
 *  searching for an integer in a sorted array of integers.
 *  <p>
 *  The <em>indexOf</em> operations takes logarithmic time in the worst case.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/11model">Section 1.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
class Main {

    /**
     * This class should not be instantiated.
     */
    private Main() { }

    /**
     * Returns the index of the specified key in the specified array.
     *
     * @param  a the array of integers, must be sorted in ascending order
     * @param  key the search key
     * @return index of key in array {@code a} if present; {@code -1} otherwise
     */
    public static int indexOf(int[] a, int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    public static int rank(int key, int[] a) {
        int index = indexOf(a, key);
        // if key is not found, either it's smaller than the smallest element
        // or larger than the largest element
        if (index == -1) {
            if (key < a[0])
                return 0;
            if (key > a[a.length-1])
                return a.length;
        }
        // look left until the start is reached or a different element is found
        while (index > 0 && a[index-1] == key)
            index--;
        return index;
    }

    public static int count(int key, int[] a) {
        int index = indexOf(a, key);
        if (index == -1)
            return 0;
        int lookLeftIndex = index;
        int lookRightIndex = index;
        int count = 1;
        while (lookLeftIndex > 0 && a[lookLeftIndex-1] == key) {
            lookLeftIndex--;
            count++;
        }
        while (lookRightIndex < a.length-1 && a[lookRightIndex+1] == key) {
            lookRightIndex++;
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        // Note: arrays must be sorted
        int[][] testArrays = new int[5][];
        testArrays[0] = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        testArrays[1] = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        testArrays[2] = new int[]{0, 1, 2, 3, 3, 3, 3, 7, 8, 9};
        testArrays[3] = new int[]{1, 1, 1, 1, 5, 6, 7, 8, 9, 10};
        testArrays[4] = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 10, 10};
        int[][] testCases = new int[14][4];
        //                       test array, key, expected rank, expected count
        testCases[0]  = new int[]{        0,   9,             9,              1 };
        testCases[1]  = new int[]{        0,   0,             0,              1 };
        testCases[2]  = new int[]{        0,   8,             8,              1 };
        testCases[3]  = new int[]{        0,  -1,             0,              0 };
        testCases[4]  = new int[]{        0,  10,            10,              0 };
        testCases[5]  = new int[]{        1,   1,             0,             10 };
        testCases[6]  = new int[]{        1,   2,            10,              0 };
        testCases[7]  = new int[]{        1,   0,             0,              0 };
        testCases[8]  = new int[]{        2,   7,             7,              1 };
        testCases[9]  = new int[]{        2,   2,             2,              1 };
        testCases[10] = new int[]{        2,   3,             3,              4 };
        testCases[11] = new int[]{        3,   1,             0,              4 };
        testCases[12] = new int[]{        3,   6,             5,              1 };
        testCases[13] = new int[]{        4,  10,             8,              2 };
        for (int i = 0; i < testCases.length; i++) {
            int[] testArray = testArrays[testCases[i][0]];
            int key = testCases[i][1];
            int expectedRank = testCases[i][2];
            int expectedCount = testCases[i][3];
            int rank = Main.rank(key, testArray);
            int count = Main.count(key, testArray);
            if (rank != expectedRank) {
                String message = String.format(
                    "Test case #%d. Test array: %d,  expected rank: %d, got: %d",
                    i, testCases[i][0], expectedRank, rank
                );
                throw new RuntimeException(message);
            }
            if (count != expectedCount) {
                String message = String.format(
                    "Test case #%d. Test array: %d,  expected count: %d, got: %d",
                    i, testCases[i][0], expectedCount, count
                );
                throw new RuntimeException(message);
            }
        }
    }
}

/******************************************************************************
 *  Copyright 2002-2020, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/

