import edu.princeton.cs.algs4.StdRandom;

class Main {
    static int[] initArray(int m) {
        int[] array = new int[m];
        for (int i = 0; i < m; i++)
            array[i] = i;
        return array;
    }

    static void shuffle(int[] a) {
        int N = a.length;
        for (int i = 0; i < N; i++) {
            // Exchange a[i] with random element in a[i..N-1]
            int r = i + StdRandom.uniform(N-i);
            int temp = a[i];
            a[i] = a[r];
            a[r] = temp;
        }
    }

    public static void main(String[] args) {
        int m = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        int[][] count = new int[m][m];
        int max = 0;
        for (int k = 0; k < n; k++) {
            int[] a = initArray(m);
            shuffle(a);
            for (int j = 0; j < m; j++) {
                count[a[j]][j]++;
                if (count[a[j]][j] > max)
                    max = count[a[j]][j];
            }
        }
        System.out.printf("N/M: %.3f", (1.0*n)/m);
        int padding = ((int) Math.log10(max)) + 2;
        for (int i = 0; i < m; i++) {
            System.out.println();
            for (int j = 0; j < m; j++)
                System.out.printf("%"+padding+"d", count[i][j]);
        }
        System.out.println();
    }
}
