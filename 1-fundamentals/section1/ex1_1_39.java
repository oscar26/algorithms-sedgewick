import java.util.Arrays;
import edu.princeton.cs.algs4.BinarySearch;
import edu.princeton.cs.algs4.StdRandom;

class Main {
    static int[] generateRandomList(int n, int low, int high) {
        int[] a = new int[n];
        for (int i = 0; i < n; i++)
            a[i] = StdRandom.uniform(low, high);
        return a;
    }

    static double countMatches(int n) {
        int[] a1 = generateRandomList(n, 1000000, 10000000);
        int[] a2 = generateRandomList(n, 1000000, 10000000);
        Arrays.sort(a1);
        int count = 0;
        for (int i = 0; i < n; i++)
            if (BinarySearch.indexOf(a1, a2[i]) != -1)
                count++;
        return count;
    }

    static double avgMatches(int n, int t) {
        long totalCount = 0;
        for (int i = 0; i < t; i++)
            totalCount += countMatches(n);
        return (1.0*totalCount)/t;
    }

    public static void main(String[] args) {
        int t = Integer.parseInt(args[0]);
        System.out.printf("Trials: %d\n\n", t);
        System.out.println("N: average matches");
        for (int n = 1000; n <= 1000000; n*=10)
            System.out.printf("%7d: %.3f\n", n, avgMatches(n, t));
    }
}
