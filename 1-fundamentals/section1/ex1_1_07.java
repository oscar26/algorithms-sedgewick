import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        a();
        b();
        c();
    }

    static void a() {
        // 9.0, 5.0, 3.4, ~3.02, ..., ~3.0
        // Square root
        double t = 9.0;
        while (Math.abs(t - 9.0/t) > .001)
            t = (9.0/t + t) / 2.0;
        StdOut.printf("%.5f\n", t);
    }

    static void b() {
        // 1 + 2 + 3 + ... + 999 = 499500
        // Arithmetic series
        int sum = 0;
        for (int i = 1; i < 1000; i++)
            for (int j = 0; j < i; j++)
                sum++;
        StdOut.println(sum);
    }

    static void c() {
        // 1, 2, 4, 8, ..., 512 -> 10 times
        // 1000 * 10 = 10000
        // Obfuscated multiplication
        int sum = 0;
        for (int i = 1; i < 1000; i *= 2)
            for (int j = 0; j < 1000; j++)  // Error in the book. It's 1000, not N
                sum++;
        StdOut.println(sum);
    }
}
