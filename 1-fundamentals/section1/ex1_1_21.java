import java.util.ArrayList;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;

class Main {
    public static void main(String[] args) {
        String[] rawLines = StdIn.readAllLines();
        ArrayList<String> lines = new ArrayList<>();
        for (String line : rawLines)
            if (line.length() > 0) lines.add(line);
        int rows = lines.size();
        String[] names = new String[rows];
        int[] dividends = new int[rows];
        int[] divisors = new int[rows];
        double[] divisions = new double[rows];
        String[] content;
        int maxNameLength = 0;
        int maxDividendLength = 0;
        int maxDivisorLength = 0;
        int maxDivisionLength = 0;
        for (int i = 0; i < rows; i++) {
            content = lines.get(i).split(" ");
            names[i] = content[0];
            dividends[i] = Integer.parseInt(content[1]);
            divisors[i] = Integer.parseInt(content[2]);
            divisions[i] = ((double) dividends[i])/((double) divisors[i]);
            maxNameLength = Math.max(names[i].length(), maxNameLength);
            maxDividendLength = (int) Math.max(Math.ceil(Math.log10(dividends[i]))+1, maxDividendLength);
            maxDivisorLength = (int) Math.max(Math.ceil(Math.log10(divisors[i]))+1, maxDivisorLength);
            maxDivisionLength = (int) Math.max(Math.ceil(Math.log10(divisions[i]))+1, maxDivisionLength);
        }
        String headerFormat = "";
        headerFormat += "%-" + maxNameLength + "s";
        headerFormat += " | %" + maxDividendLength + "s";
        headerFormat += " | %" + maxDivisorLength + "s";
        headerFormat += " | %" + (maxDivisionLength+4) + "s\n";
        String dataFormat = "";
        dataFormat += "%-" + maxNameLength + "s";
        dataFormat += " | %" + maxDividendLength + "d";
        dataFormat += " | %" + maxDivisorLength + "d";
        dataFormat += " | %" + maxDivisionLength + ".3f\n";
        StdOut.printf(headerFormat, "Name", "a", "b", "a/b");
        for (int i = 0; i < rows; i++)
            StdOut.printf(dataFormat, names[i], dividends[i], divisors[i], divisions[i]);
    }
}
