import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdRandom;

class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Pass the number of points and a probability between 0 and 1");
            System.exit(1);
        }
        int n = Integer.parseInt(args[0]);
        double p = Double.parseDouble(args[1]);
        if (n < 2)
            throw new IllegalArgumentException("Minumum number of points is 2");
        if (p < 0 || p > 1)
            throw new IllegalArgumentException("Probability p must be within [0, 1] inclusive");
        double xCenter = 0.5;
        double yCenter = 0.5;
        double radius = 0.4;
        double angle = 2*Math.PI/n;
        double[] x = new double[n];
        double[] y = new double[n];
        for (int i = 0; i < n; i++) {
            // Drawing origin is located at the lower left corner
            x[i] = radius*Math.cos(i*angle) + xCenter;
            y[i] = radius*Math.sin(i*angle) + yCenter;
        }
        StdDraw.setPenRadius(0.015);
        StdDraw.setPenColor(StdDraw.GRAY);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (i != j && StdRandom.random() < p)
                    StdDraw.line(x[i], y[i], x[j], y[j]);
        // Drawing circle and points last so they are showed on top of the
        // lines
        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.circle(xCenter, yCenter, radius);
        StdDraw.setPenRadius(0.02);
        StdDraw.setPenColor(StdDraw.RED);
        for (int i = 0; i < n; i++)
            StdDraw.point(x[i], y[i]);
    }
}
