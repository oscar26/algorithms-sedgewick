import java.util.concurrent.ThreadLocalRandom;
import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        int M = 10;
        int[] array = new int[M];
        StdOut.print("Array:     ");
        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(0, M);
            StdOut.printf("%2d", array[i]);
        }
        int[] hist = histogram(array, M);
        StdOut.print("\nHistogram: ");
        for (int i = 0; i < hist.length; i++)
            StdOut.printf("%2d", hist[i]);
        StdOut.println();
        int sum = 0;
        for (int i = 0; i < hist.length; i++)
            sum += hist[i];
        StdOut.println("Sum: " + sum);
    }

    static int[] histogram(int[] array, int M) {
        int[] hist = new int[M];
        for (int i = 0; i < array.length; i++)
            if (array[i] >= 0 && array[i] < M)
                hist[array[i]]++;
        return hist;
    }
}
