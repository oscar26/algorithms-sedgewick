class Main {
    public static void main(String[] args) {
        System.out.println(logFactorial(10));
    }

    static double logFactorial(int n) {
        return Math.log(factorial(n));
    }

    static long factorial(int n) {
        if (n == 0) return 1;
        return n * factorial(n-1);
    }
}
