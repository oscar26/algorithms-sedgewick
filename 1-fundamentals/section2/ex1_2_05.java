/* Prints:
 *     Hello World
 *
 * As strings are immutable, the methods don't modify the instance but rather
 * return a new string.
 */
import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        String s = "Hello World";
        s.toUpperCase();
        s.substring(6, 11);
        StdOut.println(s);
    }
}
