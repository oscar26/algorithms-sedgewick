import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;

class Main {

    static class SmartDate {

        private final int value;

        public SmartDate(int y, int m, int d) {
            validateDate(y, m, d);
            value = y*512 + m*32 + d;
        }

        public int year() {
            return value/512;
        }

        public int month() {
            return (value/32) % 16;
        }

        public int day() {
            return value % 32;
        }

        public String toString() {
            return String.format("%04d-%02d-%02d", year(), month(), day());
        }

        public int weekday() {
            int y = year();
            int m = month();
            int d = day();
            if (y < 2000)
                throw new UnsupportedOperationException("Method supported only for dates after 2000-01-01");
            // Epoch date: 2000-01-01 (Saturday)
            int wholeYears = y - 2000;
            int leapYears = (wholeYears+1) / 4;
            // Since 2000 was a leap year, we need to include its extra day if
            // the date is equal or comes after 2000-02-29. The extra day is
            // added if the date is after february 28th
            if (m > 2)
                leapYears++;
            // Four months have 30 days, 7 months have 31 days and february
            // always has 28 days. February extra day is adjusted by the number
            // of leap years between the defined epoch and the date.
            int wholeYearDays = (30*4 + 31*7 + 28)*wholeYears + leapYears;
            int remainingDays;
            switch (m) {
                case 1:
                    remainingDays = d;
                    break;
                case 2:
                    remainingDays = 31 + d;
                    break;
                case 3:
                    remainingDays = 31 + 28 + d;
                    break;
                case 4:
                    remainingDays = 31*2 + 28 + d;
                    break;
                case 5:
                    remainingDays = 31*2 + 30 + 28 + d;
                    break;
                case 6:
                    remainingDays = 31*3 + 30 + 28 + d;
                    break;
                case 7:
                    remainingDays = 31*3 + 30*2 + 28 + d;
                    break;
                case 8:
                    remainingDays = 31*4 + 30*2 + 28 + d;
                    break;
                case 9:
                    remainingDays = 31*5 + 30*2 + 28 + d;
                    break;
                case 10:
                    remainingDays = 31*5 + 30*3 + 28 + d;
                    break;
                case 11:
                    remainingDays = 31*6 + 30*3 + 28 + d;
                    break;
                default:
                    remainingDays = 31*6 + 30*4 + 28 + d;
                    break;
            }
            remainingDays--;
            // Java days of the week index:
            //     1: Sunday
            //     2: Monday
            //     3: Tuesday
            //     4: Wednesday
            //     5: Thursday
            //     6: Friday
            //     7: Saturday
            //
            // 6 is added since the epoch date was a saturday and then 1 is
            // added to make it 1-based
            return ((6 + wholeYearDays + remainingDays) % 7) + 1;
        }

        public String dayOfTheWeek() {
            switch (weekday()) {
                case 1:
                    return "Sunday";
                case 2:
                    return "Monday";
                case 3:
                    return "Tuesday";
                case 4:
                    return "Wednesday";
                case 5:
                    return "Thursday";
                case 6:
                    return "Friday";
                case 7:
                    return "Saturday";
            }
            throw new RuntimeException("Unexpected error");
        }

        private void validateDate(int y, int m, int d) {
            if (y < 0)
                throw new IllegalArgumentException("year cannot be negative");
            if (m < 1 || m > 12)
                throw new IllegalArgumentException("month must be between 1 and 12");
            if (d < 1 || d > 31)
                throw new IllegalArgumentException("day must be between 1 and 31");
            switch (m) {
                // Handle february
                case 2:
                    // Leap years
                    if (isLeapYear(y)) {
                        if (d > 29)
                            throw new IllegalArgumentException(
                                "Leap year. Month 2 does not have " + d + " days"
                            );
                    } else {
                        if (d > 28)
                            throw new IllegalArgumentException(
                                "Not leap year. Month 2 does not have " + d + " days"
                            );
                    }
                    break;
                // Handle the rest of the months that cannot have 31 days
                case 4: case 6: case 9: case 11:
                    if (d == 31)
                        throw new IllegalArgumentException("month " + m + " does not have 31 days");
                    break;
            }
        }

        public static boolean isLeapYear(int y) {
            if (y % 4 != 0) return false;
            else if (y % 100 != 0) return true;
            else if (y % 400 != 0) return false;
            return true;
        }

    }

    public static void main(String[] args) {
        int[][] testCases = new int[][] {
            {2000, 1, 1},
            {2000, 1, 2},
            {2000, 2, 28},
            {2000, 2, 29},
            {2012, 1, 2},
            {2015, 2, 28},
            {2020, 2, 29},
            {2021, 3, 15},
            {2021, 7, 3}
        };
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < testCases.length; i++) {
            SmartDate date = new SmartDate(
                testCases[i][0],
                testCases[i][1],
                testCases[i][2]
            );
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(dateFormat.parse(date.toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int expectedWeekday = calendar.get(Calendar.DAY_OF_WEEK);
            assert expectedWeekday == date.weekday()
                : String.format(
                    "Date: %s. Expected: %d, got: %d",
                    date.toString(), expectedWeekday, date.weekday()
                );
            System.out.println(date.dayOfTheWeek());
        }
    }
}

