class Main {
    static class Date {

        private final int value;

        public Date(int y, int m, int d) {
            value = y*512 + m*32 + d;
        }

        public int year() {
            return value/512;
        }

        public int month() {
            return (value/32) % 16;
        }

        public int day() {
            return value % 32;
        }

        public String toString() {
            return String.format("%04d-%02d-%02d", year(), month(), day());
        }
    }

    static class Transaction {

        private final String who;
        private final Date when;
        private final double amount;

        public Transaction(String who, Date when, double amount) {
            this.who = who;
            this.when = when;
            this.amount = amount;
        }

        public String who() {
            return who;
        }

        public Date when() {
            return when;
        }

        public double amount() {
            return amount;
        }

        public String toString() {
            return String.format(
                "Transaction(who=%s, when=%s, amount=%.5f)",
                who, when.toString(), amount
            );
        }

    }

    public static void main(String[] args) {
        Date when = new Date(2020, 1, 1);
        Transaction transaction = new Transaction("Someone", when, 11.0);
        System.out.println(transaction);
    }
}
