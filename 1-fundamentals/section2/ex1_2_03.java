import edu.princeton.cs.algs4.Interval1D;
import edu.princeton.cs.algs4.Interval2D;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.StdRandom;

class Main {
    static Interval1D random1DInterval(double min, double max) {
        double length = StdRandom.uniform(min, max);
        double start = StdRandom.uniform(0.0, 1.0 - length);
        return new Interval1D(start, start + length);
    }

    // If interval A contains an interval B, then all the corners of interval B
    // must lie within interval A
    static boolean containsOther(Interval2D intervalA, Point2D[] cornersB) {
        for (int i = 0; i < cornersB.length; i++)
            if (!intervalA.contains(cornersB[i]))
                return false;
        return true;
    }

    public static void main(String[] args) {
        if (args.length != 3)
            throw new RuntimeException("Pass the number of intervals (N), min and max");
        int n = Integer.parseInt(args[0]);
        double min = Double.parseDouble(args[1]);
        double max = Double.parseDouble(args[2]);
        if (n < 2)
            throw new RuntimeException("N must be at least 2");
        if (min < 0 || min > 1)
            throw new RuntimeException("min must be between 0 and 1, inclusive");
        if (max < 0 || max > 1)
            throw new RuntimeException("min must be between 0 and 1, inclusive");
        if (min >= max)
            throw new RuntimeException("min must be less than max");
        // TODO: draw unit square
        Interval2D[] intervals = new Interval2D[n];
        Point2D[][] corners = new Point2D[n][];
        for (int i = 0; i < n; i++) {
            Interval1D x = random1DInterval(min, max);
            Interval1D y = random1DInterval(min, max);
            intervals[i] = new Interval2D(x, y);
            corners[i] = new Point2D[] {
                new Point2D(x.min(), y.min()),
                new Point2D(x.min(), y.max()),
                new Point2D(x.max(), y.min()),
                new Point2D(x.max(), y.max()),
            };
            intervals[i].draw();
        }
        int intersections = 0;
        int containments = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (intervals[i].intersects(intervals[j]))
                    intersections++;
                if (containsOther(intervals[i], corners[j]) || containsOther(intervals[j], corners[i]))
                    containments++;
            }
        }
        System.out.printf("Intersections: %d, Containments: %d\n", intersections, containments);
    }
}
