/*
 * My first solution was to convert the values to BigInteger and compare the
 * results using long to the expected result using BigInteger. This approach
 * has no sense since we just can use BigInteger and forgo the use of the long
 * type.
 *
 * After a bit of research (StackOverflow, very aptly named), I found some
 * methods in the class java.lang.Math which implement simple and elegan checks
 * to identify overflows (addExact, subtractExact, multiplyExact, etc). Link:
 *
 *     https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/lang/Math.java#L879
 *
 * To illustrate how those checks work, let's assume that our integer values
 * are represented by two's complement 4 bits, e.g.,
 *
 * Note: recall that two's complements is inverting the bits and adding 1.
 *
 *     1 = 0001, -1 = 1111
 *     2 = 0010, -2 = 1110
 *     3 = 0011, -3 = 1101
 *     4 = 0100, -4 = 1100
 *     5 = 0101, -5 = 1011
 *     6 = 0110, -6 = 1010
 *     7 = 0111, -7 = 1001
 *
 * If we add/subtract two numbers with the same sign, the sign of the result
 * must be the same as of the operands, except when the result overflows.
 *
 *       1      0001
 *     + 2    + 0010
 *     ---    ------
 *       3      0011
 *
 *              111
 *       5      0101
 *     + 3    + 0011
 *     ---    ------
 *       8      1000  <-- Overflow
 *
 *              1
 *     - 3      1101
 *     - 2    + 1110
 *     ---    ------
 *     - 5      1011
 *
 *     - 4      1100
 *     - 6    + 1001
 *     ---    ------
 *     -10      0101  <-- Overflow
 *
 * By using XOR we can check if the sign of each operand differs from the sign
 * of the result (we only need the leftmost bit) and use AND to check if both
 * both, in which case, the sign bit will be set and thus the numeric value of
 * the comparison will be negative,
 *
 *     ((x ^ result) & (y ^ result)) < 0
 *
 * If the two numbers are of different signs, there's no problem since the
 * magnitude of the addition/subtraction will be less than |Long.MAX_VALUE| or
 * |Long.MIN_VALUE| where |.| denotes the absolute value.
 *
 * For multiplication, we need to check if we can get back the operands by
 * reverting (dividing) the operation. Let x and y be the operands,
 *
 *     result = x * y
 *     x == result / y, given y != 0 or,
 *     y == result / x, given x != 0
 *
 * java.lang.Math.multiplyExact(long, long) does an additional optimization
 * which is performing the division only if any of the leftmost 32 bits are set
 * (perhaps because division is an expensive operation).
 *
 * A single division is sufficient as the only edge case left is when
 * x == Long.MIN_VALUE and y == -1. Let's consider this case,
 *
 * Long.MIN_VALUE      = 1000000000000000000000000000000000000000000000000000000000000000
 *
 * Long.MIN_VALUE * -1 = invert bits and add 1
 *
 *                       111111111111111111111111111111111111111111111111111111111111111
 *                       0111111111111111111111111111111111111111111111111111111111111111
 *                     + 0000000000000000000000000000000000000000000000000000000000000001
 *                     ------------------------------------------------------------------
 *                       1000000000000000000000000000000000000000000000000000000000000000
 *
 * As we can see, the sum overflows to the sign bit as we need an extra bit to
 * represent the positive value, resulting in the same number.
 *
 * So, it's a matter to add this check to the conditional.
 *
 *
 * The robust operations are implemented in carefulAdd and carefulMult, and
 * they are more or less a copy of the ones found in the Math class. BUT, the
 * value is in the research to solve this problem.
 *
 * */

import java.math.BigInteger;

class Main {

    static class Rational {

        private final long num;
        private final long den;
        private final static String OVERFLOW_MSG = "Operation overflow";

        public Rational(long numerator, long denominator) {
            if (denominator == 0)
                throw new ArithmeticException("Division by zero");
            long commonFactor = gcd(numerator, denominator);
            if (commonFactor < 0)
                commonFactor = -commonFactor;
            // If the number is negative, put the sign in the numerator
            boolean numNegative = numerator < 0;
            boolean denNegative = denominator < 0;
            if ((numNegative && denNegative) || (!numNegative && denNegative)) {
                this.num = -numerator/commonFactor;
                this.den = -denominator/commonFactor;
            } else {
                this.num = numerator/commonFactor;
                this.den = denominator/commonFactor;
            }
        }

        public long numerator() {
            return num;
        }

        public long denominator() {
            return den;
        }

        public Rational plus(Rational b) {
            long newDen = lcm(den, b.denominator());
            long aFactor = newDen/den;
            long bFactor = newDen / b.denominator();
            long left = carefulMult(num, aFactor);
            long right = carefulMult(b.numerator(), bFactor);
            long newNum = carefulAdd(left, right);
            return new Rational(newNum, newDen);
        }

        public Rational minus(Rational b) {
            long numerator = carefulMult(-1, b.numerator());
            return this.plus(new Rational(numerator, b.denominator()));
        }

        public Rational times(Rational b) {
            long numAdenBcommonFactor = gcd(num, b.denominator());
            long denAnumBcommonFactor = gcd(den, b.numerator());
            // Reducing to least common terms to lower the risk of overflow
            long numA = num / numAdenBcommonFactor;
            long denA = den / denAnumBcommonFactor;
            long numB = b.numerator() / denAnumBcommonFactor;
            long denB = b.denominator() / numAdenBcommonFactor;
            long numerator = carefulMult(numA, numB);
            long denominator = carefulMult(denA, denB);
            return new Rational(numerator, denominator);
        }

        public Rational divides(Rational b) {
            // Division by zero is handled by the constructor
            return this.times(new Rational(b.denominator(), b.numerator()));
        }

        public boolean equals(Object x) {
            if (this == x) return true;
            if (x == null) return false;
            if (this.getClass() != x.getClass()) return false;
            Rational that = (Rational) x;
            if (num != that.numerator()) return false;
            if (den != that.denominator()) return false;
            return true;

        }

        public String toString() {
            return String.format("%d/%d", num, den);
        }

        public static long lcm(long a, long b) {
            if (a == 0 && b == 0)
                throw new ArithmeticException("Division by zero");
            if (a < 0) a = -a;
            if (b < 0) b = -b;
            long commonDivisor = gcd(a, b);
            long reducedA = a / commonDivisor;
            return carefulMult(reducedA, b);
        }

        public static long gcd(long p, long q) {
            if (q == 0) return p;
            long r = p % q;
            return gcd(q, r);
        }

        public static long carefulAdd(long x, long y) {
            long r = x + y;
            assert ((x ^ r) & (y ^ r)) >= 0 : OVERFLOW_MSG;
            return r;
        }

        public static long carefulMult(long x, long y) {
            long r = x * y;
            boolean overflow = ((y != 0) && (r / y != x)) ||
                (x == Long.MIN_VALUE && y == -1);
            assert !overflow : OVERFLOW_MSG;
            return r;
        }
    }

    // Some utility functions to test out ideas

    static void printBinary(long n) {
        String str = String.format("%64s", Long.toBinaryString(n)).replace(" ", "0");
        System.out.println(str);
    }

    static void scratchpad() {
        long x = 1;
        // long x = Long.MAX_VALUE;
        long y = 2;
        long r = x + y;
        if (((x ^ r) & (y ^ r)) < 0)
            System.out.println("overflow");
        printBinary(x);
        printBinary(y);
        printBinary(r);
        System.out.println();
        printBinary(x ^ r);
        printBinary(y ^ r);
        printBinary((x ^ r) & (y ^ r));
        System.out.println();
        printBinary(Long.MIN_VALUE);
        printBinary(-1);
        printBinary(Long.MIN_VALUE * -1);
        System.out.println();
        r = -1l * Long.MIN_VALUE;
        System.out.println(r);
        System.out.println(r / Long.MIN_VALUE);
        // System.out.println(Math.multiplyExact(Long.MIN_VALUE, -1L));
        // System.out.println(Math.multiplyExact(-1l, Long.MIN_VALUE));
    }

    public static void main(String args[]) {
        // scratchpad();

        // Test zero denominator check
        Rational number;
        boolean zeroException = false;
        try {
            number = new Rational(1, 0);
        } catch (ArithmeticException e) {
            zeroException = true;
        }
        assert zeroException;

        // Test construction
        number = new Rational(0, 1);
        assert number.numerator() == 0;
        assert number.denominator() == 1;
        number = new Rational(1, 1);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(3, 2);
        assert number.numerator() == 3;
        assert number.denominator() == 2;
        number = new Rational(2, 3);
        assert number.numerator() == 2;
        assert number.denominator() == 3;

        // Test sign normalization. Sign must end up in the numerator. If
        // numerator and denominator and both negative, the new rational number
        // must be positive
        number = new Rational(-1, 1);
        assert number.numerator() == -1;
        assert number.denominator() == 1;
        number = new Rational(1, -1);
        assert number.numerator() == -1;
        assert number.denominator() == 1;
        number = new Rational(-1, -1);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(-127, 3);
        assert number.numerator() == -127;
        assert number.denominator() == 3;
        number = new Rational(127, -3);
        assert number.numerator() == -127;
        assert number.denominator() == 3;
        number = new Rational(-127, -3);
        assert number.numerator() == 127;
        assert number.denominator() == 3;

        // Test common factor elimination during construction
        number = new Rational(2, 2);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(12, 9);
        assert number.numerator() == 4;
        assert number.denominator() == 3;
        number = new Rational(0, 4);
        assert number.numerator() == 0;
        assert number.denominator() == 1;
        number = new Rational(0, 7);
        assert number.numerator() == 0;
        assert number.denominator() == 1;

        // Test toString()
        number = new Rational(1, 1);
        assert number.toString().equals("1/1");
        number = new Rational(0, 1);
        assert number.toString().equals("0/1");
        number = new Rational(2, 1);
        assert number.toString().equals("2/1");
        number = new Rational(1, 2);
        assert number.toString().equals("1/2");
        number = new Rational(-1, 1);
        assert number.toString().equals("-1/1");
        number = new Rational(-127, 3);
        assert number.toString().equals("-127/3");

        // Test equals
        Rational numberA;
        Rational numberB;
        assert number.equals(number);
        assert !number.equals(null);
        assert !number.equals("string class");
        numberA = new Rational(2, 1);
        numberB = new Rational(1, 1);
        assert !numberA.equals(numberB);
        numberA = new Rational(1, 2);
        assert !numberA.equals(numberB);
        numberA = new Rational(1, 1);
        assert numberA.equals(numberB);

        Rational result;
        // Test addition
        result = (new Rational(1, 1)).plus(new Rational(1, 1));
        assert result.numerator() == 2;
        assert result.denominator() == 1;
        result = (new Rational(1, 1)).plus(new Rational(-1, 1));
        assert result.numerator() == 0;
        assert result.denominator() == 1;
        result = (new Rational(-1, 1)).plus(new Rational(1, 1));
        assert result.numerator() == 0;
        assert result.denominator() == 1;
        result = (new Rational(-1, 1)).plus(new Rational(-1, 1));
        assert result.numerator() == -2;
        assert result.denominator() == 1;
        result = (new Rational(-10, 5)).plus(new Rational(3, 2));
        assert result.numerator() == -1;
        assert result.denominator() == 2;
        result = (new Rational(3, 2)).plus(new Rational(-10, 5));
        assert result.numerator() == -1;
        assert result.denominator() == 2;
        result = (new Rational(12, 9)).plus(new Rational(8, 6));
        assert result.equals(new Rational(8, 3));

        // Test subtraction
        result = (new Rational(12, 9)).minus(new Rational(8, 6));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(0, 1)).minus(new Rational(0, 1));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(1, 1)).minus(new Rational(0, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(2, 1)).minus(new Rational(1, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(3, 2)).minus(new Rational(1, 2));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(-10, 9)).minus(new Rational(-9, 5));
        assert result.equals(new Rational(31, 45));

        // Test multiplication
        result = (new Rational(1, 1)).times(new Rational(0, 1));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(1, 9)).times(new Rational(9, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(1, 1)).times(new Rational(-1, 1));
        assert result.equals(new Rational(-1, 1));
        result = (new Rational(100, 99)).times(new Rational(1, 2));
        assert result.equals(new Rational(50, 99));
        result = (new Rational(100, 99)).times(new Rational(9, 4));
        assert result.equals(new Rational(25, 11));
        result = (new Rational(23, 17)).times(new Rational(2, 3));
        assert result.equals(new Rational(46, 51));

        // Test division by zero handling
        zeroException = false;
        try {
            result = (new Rational(1, 1)).divides(new Rational(0, 1));
        } catch (ArithmeticException e) {
            zeroException = true;
        }
        assert zeroException;

        // Test division
        result = (new Rational(27, 13)).divides(new Rational(1, 1));
        assert result.equals(new Rational(27, 13));
        result = (new Rational(1, 9)).divides(new Rational(9, 1));
        assert result.equals(new Rational(1, 81));
        result = (new Rational(1, 1)).divides(new Rational(-1, 1));
        assert result.equals(new Rational(-1, 1));
        result = (new Rational(23, 17)).divides(new Rational(2, 3));
        assert result.equals(new Rational(69, 34));

        // Tests from: https://algs4.cs.princeton.edu/12oop/Rational.java.html

        Rational x, y, z;

        // 1/2 + 1/3 = 5/6
        x = new Rational(1, 2);
        y = new Rational(1, 3);
        z = x.plus(y);
        System.out.println(z);

        // 8/9 + 1/9 = 1
        x = new Rational(8, 9);
        y = new Rational(1, 9);
        z = x.plus(y);
        System.out.println(z);

        // 1/200000000 + 1/300000000 = 1/120000000
        x = new Rational(1, 200000000);
        y = new Rational(1, 300000000);
        z = x.plus(y);
        System.out.println(z);

        // 1073741789/20 + 1073741789/30 = 1073741789/12
        x = new Rational(1073741789, 20);
        y = new Rational(1073741789, 30);
        z = x.plus(y);
        System.out.println(z);

        //  4/17 * 17/4 = 1
        x = new Rational(4, 17);
        y = new Rational(17, 4);
        z = x.times(y);
        System.out.println(z);

        // 3037141/3247033 * 3037547/3246599 = 841/961
        x = new Rational(3037141, 3247033);
        y = new Rational(3037547, 3246599);
        z = x.times(y);
        System.out.println(z);

        // 1/6 - -4/-8 = -1/3
        x = new Rational(1, 6);
        y = new Rational(-4, -8);
        z = x.minus(y);
        System.out.println(z);

        // Test lcm overflow
        boolean overflowException = false;
        try {
            Rational.lcm(Long.MAX_VALUE, Long.MAX_VALUE-1);
        } catch (AssertionError e) {
            overflowException = true;
        }
        assert overflowException;

        // No overflow
        assert Long.MAX_VALUE == Rational.lcm(Long.MAX_VALUE, Long.MAX_VALUE);

        // Test addition and subtraction overflow
        overflowException = false;
        try {
            x = new Rational(Long.MAX_VALUE, 1);
            y = new Rational(1, 1);
            z = x.plus(y);
        } catch (AssertionError e) {
            overflowException = true;
        }
        assert overflowException;

        overflowException = false;
        try {
            x = new Rational(Long.MIN_VALUE, 1);
            y = new Rational(1, 1);
            z = x.minus(y);
        } catch (AssertionError e) {
            overflowException = true;
        }
        assert overflowException;

        // No overflow
        x = new Rational(Long.MAX_VALUE, 1);
        y = new Rational(-1, 1);
        assert x.plus(y).numerator() == Long.MAX_VALUE - 1;

        // Test multiplication and division overflow
        overflowException = false;
        try {
            x = new Rational(Long.MIN_VALUE, 1);
            y = new Rational(2, 1);
            z = x.times(y);
        } catch (AssertionError e) {
            overflowException = true;
        }
        assert overflowException;

        overflowException = false;
        try {
            x = new Rational(Long.MAX_VALUE, 1);
            y = new Rational(1, 3);
            z = x.divides(y);
        } catch (AssertionError e) {
            overflowException = true;
        }
        assert overflowException;
    }
}
