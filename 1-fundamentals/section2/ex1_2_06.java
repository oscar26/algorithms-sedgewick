class Main {
    static String rotate(String string, int pos) {
        assert pos >= 0 : "rotate string to the right not supported yet";
        if (string.length() < 2)
            return string;
        int wrappedPos = pos % string.length();
        return string.substring(wrappedPos, string.length()) + string.substring(0, wrappedPos);
    }

    // Is "t" a rotation of "s".
    // Returns the number of left rotations to make "s" equal to "t". If "t" is
    // not a circular rotation of "s", then -1 is returned.
    //
    // Attempt before reading the hint.
    static int isRotation(String s, String t) {
        if (s.length() != t.length())
            return -1;
        if (s.length() == 0)
            return 0;
        for (int i = 0; i < s.length(); i++)
            if (t.equals(rotate(s, i)))
                return i;
        return -1;
    }

    // Attempt after reading the hint
    static int bookHint(String s, String t) {
        return (s+s).indexOf(t);
    }

    public static void main(String[] args) {
        Object[][] testCases = new Object[][] {
            {"", "", 0},
            {"aa", "aa", 0},
            {"ab", "ba", 1},
            {"abc", "abc", 0},
            {"abc", "bca", 1},
            {"abc", "cab", 2},
            {"abc", "acb", -1},
            {"TGACGAC", "ACTGACG", 5},
            {"TGACGA", "ACTGACG", -1}
        };
        for (int i = 0; i < testCases.length; i++) {
            String s = (String) testCases[i][0];
            String t = (String) testCases[i][1];
            int r = (int) testCases[i][2];
            assert isRotation(s, t) == r : String.format("'%s' '%s', rotations: %d", s, t, r);
            assert isRotation(s, t) == bookHint(s, t);
        }
    }
}
