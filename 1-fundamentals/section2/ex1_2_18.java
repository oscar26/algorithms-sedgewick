/*
 * Corrected code:
 *     https://algs4.cs.princeton.edu/12oop/Accumulator.java.html
 *
 * */
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

class Main {
    static class Accumulator {
        private int n = 0;          // number of data values
        private double sum = 0.0;   // sample variance * (n-1)
        private double mu = 0.0;    // sample mean

        /**
         * Adds the specified data value to the accumulator.
         * @param  x the data value
         */
        public void addDataValue(double x) {
            n++;
            double delta = x - mu;
            mu  += delta / n;
            sum += (double) (n - 1) / n * delta * delta;
        }

        /**
         * Returns the mean of the data values.
         * @return the mean of the data values
         */
        public double mean() {
            return mu;
        }

        /**
         * Returns the sample variance of the data values.
         * @return the sample variance of the data values
         */
        public double var() {
            if (n <= 1) return Double.NaN;
            return sum / (n - 1);
        }

        /**
         * Returns the sample standard deviation of the data values.
         * @return the sample standard deviation of the data values
         */
        public double stddev() {
            return Math.sqrt(this.var());
        }
    }

    static double[] randomArray(int n) {
        double[] array = new double[n];
        for (int i = 0; i < n; i++)
            array[i] = StdRandom.uniform();
        return array;
    }

    static double[] stats(double[] values) {
        return new double[] {StdStats.mean(values), StdStats.var(values), StdStats.stddev(values)};
    }

    public static void main(String[] args) {
        double[][] testCases = {
            {1.0, 2.0, 3.0, 4.0, 5.0},
            randomArray(100),
            randomArray(100),
            randomArray(100)
        };
        double[][] expectedStats = {
            {3.0, 5.0/2.0, Math.sqrt(5.0/2.0)},
            stats(testCases[1]),
            stats(testCases[2]),
            stats(testCases[3])
        };
        for (int i = 0; i < testCases.length; i++) {
            Accumulator acc = new Accumulator();
            for (int j = 0; j < testCases[i].length; j++)
                acc.addDataValue(testCases[i][j]);
            // NOTE: I'm using the word error as if the implementations in
            // StdStats are numerically stable, which may not be the case. The
            // exercise asks to validate the provided code is correct (which it
            // is after fixing the error. See Welford's algorithm). Thus, we
            // use StdStats just as a reference as Java does not provide a
            // built-in basic stats package
            double errorMean = Math.abs(acc.mean() - expectedStats[i][0]);
            double errorVar = Math.abs(acc.var() - expectedStats[i][1]);
            double errorStddev = Math.abs(acc.stddev() - expectedStats[i][2]);
            System.out.println("\nRun #" + (i+1));
            System.out.println("Error mean: " + errorMean);
            System.out.println("Error var: " + errorVar);
            System.out.println("Error stddev: " + errorStddev);
            assert errorMean < 1e-8;
            assert errorVar < 1e-8;
            assert errorStddev < 1e-8;
        }
    }
}
