import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import edu.princeton.cs.algs4.In;

class Main {
    static int[] readInts(Scanner scanner) {
        String wholeInput = scanner.useDelimiter("\\A").next();
        String[] lines = wholeInput.split("\\s+");
        int[] values = new int[lines.length];
        for (int i = 0; i < lines.length; i++)
            values[i] = Integer.parseInt(lines[i]);
        scanner.reset();
        return values;
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass a file containing a list of integer numbers");
        try {
            Scanner scanner = new Scanner(new File(args[0]));
            int[] list = readInts(scanner);
            In in = new In(args[0]);
            int[] expectedList = in.readAllInts();
            assert list.length == expectedList.length : "Lists are of different lengths";
            for (int i = 0; i < expectedList.length; i++)
                assert list[i] == expectedList[i] :
                    String.format("Expected: %d, got: %d, position: %d", expectedList[i], list[i], i);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
