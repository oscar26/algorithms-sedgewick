import edu.princeton.cs.algs4.StdDraw;

class Main {

    static class VisualCounter {
        private int total;
        private int n;

        public VisualCounter(int N, int max) {
            StdDraw.setXscale(0, N);
            StdDraw.setYscale(-max, max);
            StdDraw.setPenRadius(.01);
        }

        public void increment() {
            total++;
            n++;
            addPoint();
        }

        public void decrement() {
            total--;
            n++;
            addPoint();
        }

        public int tally() {
            return total;
        }

        public String toString() {
            return "Counter(" + total + ")";
        }

        private void addPoint() {
            StdDraw.setPenColor(StdDraw.DARK_GRAY);
            StdDraw.point(n, total);
        }
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new RuntimeException("Pass the number of points to generate (N)");
        int n = Integer.parseInt(args[0]);
        VisualCounter counter = new VisualCounter(n, n);
        for (int i = 0; i < n; i++) {
            if (Math.random() > 0.5)
                counter.increment();
            else
                counter.decrement();
        }
    }
}
