class Main {

    static class SmartDate {

        private final int value;

        public SmartDate(int y, int m, int d) {
            validateDate(y, m, d);
            value = y*512 + m*32 + d;
        }

        public int year() {
            return value/512;
        }

        public int month() {
            return (value/32) % 16;
        }

        public int day() {
            return value % 32;
        }

        public String toString() {
            return String.format("%04d-%02d-%02d", year(), month(), day());
        }

        private void validateDate(int y, int m, int d) {
            if (y < 0)
                throw new IllegalArgumentException("year cannot be negative");
            if (m < 1 || m > 12)
                throw new IllegalArgumentException("month must be between 1 and 12");
            if (d < 1 || d > 31)
                throw new IllegalArgumentException("day must be between 1 and 31");
            switch (m) {
                // Handle february
                case 2:
                    // Leap years
                    if (isLeapYear(y)) {
                        if (d > 29)
                            throw new IllegalArgumentException(
                                "Leap year. Month 2 does not have " + d + " days"
                            );
                    } else {
                        if (d > 28)
                            throw new IllegalArgumentException(
                                "Not leap year. Month 2 does not have " + d + " days"
                            );
                    }
                    break;
                // Handle the rest of the months that cannot have 31 days
                case 4: case 6: case 9: case 11:
                    if (d == 31)
                        throw new IllegalArgumentException("month " + m + " does not have 31 days");
                    break;
            }
        }

        public static boolean isLeapYear(int y) {
            if (y % 4 != 0) return false;
            else if (y % 100 != 0) return true;
            else if (y % 400 != 0) return false;
            return true;
        }

    }

    public static void main(String[] args) {
        int[][] testCases = new int[][] {
            {-1, 1, 2},
            {2013, 0, 2},
            {2013, 13, 2},
            {2014, 1, 0},
            {2014, 1, 32},
            {2015, 4, 31},
            {2015, 6, 31},
            {2015, 9, 31},
            {2015, 11, 31},
            {2015, 2, 29},
            {2020, 2, 30},
            {2015, 2, 28},
            {2020, 2, 29},
            {2012, 1, 2}
        };
        String[] expectedErrorMessages = new String[] {
            "year cannot be negative",
            "month must be between 1 and 12",
            "month must be between 1 and 12",
            "day must be between 1 and 31",
            "day must be between 1 and 31",
            "month 4 does not have 31 days",
            "month 6 does not have 31 days",
            "month 9 does not have 31 days",
            "month 11 does not have 31 days",
            "Not leap year. Month 2 does not have 29 days",
            "Leap year. Month 2 does not have 30 days",
            "",
            "",
            ""
        };
        for (int i = 0; i < testCases.length; i++) {
            String errorMsg = "";
            String expectedMsg = expectedErrorMessages[i];
            try {
                SmartDate date = new SmartDate(
                    testCases[i][0],
                    testCases[i][1],
                    testCases[i][2]
                );
            } catch (IllegalArgumentException e) {
                errorMsg = e.getMessage();
            }
            assert errorMsg.equals(expectedMsg)
                : String.format("Expected: %s, got: %s", expectedMsg, errorMsg);
        }
    }
}
