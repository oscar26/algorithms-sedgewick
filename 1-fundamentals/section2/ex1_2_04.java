/* Prints:
 *     world
 *     hello
 *
 * string2 references the original value of string1 ("hello"), then string1
 * changes reference to a new string "world".
 * */

import edu.princeton.cs.algs4.StdOut;

class Main {
    public static void main(String[] args) {
        String string1 = "hello";
        String string2 = string1;
        string1 = "world";
        StdOut.println(string1);
        StdOut.println(string2);
    }
}
