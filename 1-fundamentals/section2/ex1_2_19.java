class Main {
    static class Date {

        private final int value;

        public Date(int y, int m, int d) {
            validateDate(y, m, d);
            value = y*512 + m*32 + d;
        }

        public int year() {
            return value/512;
        }

        public int month() {
            return (value/32) % 16;
        }

        public int day() {
            return value % 32;
        }

        public String toString() {
            return String.format("%04d-%02d-%02d", year(), month(), day());
        }

        public int weekday() {
            int y = year();
            int m = month();
            int d = day();
            if (y < 2000)
                throw new UnsupportedOperationException("Method supported only for dates after 2000-01-01");
            // Epoch date: 2000-01-01 (Saturday)
            int wholeYears = y - 2000;
            int leapYears = (wholeYears+1) / 4;
            // Since 2000 was a leap year, we need to include its extra day if
            // the date is equal or comes after 2000-02-29. The extra day is
            // added if the date is after february 28th
            if (m > 2)
                leapYears++;
            // Four months have 30 days, 7 months have 31 days and february
            // always has 28 days. February extra day is adjusted by the number
            // of leap years between the defined epoch and the date.
            int wholeYearDays = (30*4 + 31*7 + 28)*wholeYears + leapYears;
            int remainingDays;
            switch (m) {
                case 1:
                    remainingDays = d;
                    break;
                case 2:
                    remainingDays = 31 + d;
                    break;
                case 3:
                    remainingDays = 31 + 28 + d;
                    break;
                case 4:
                    remainingDays = 31*2 + 28 + d;
                    break;
                case 5:
                    remainingDays = 31*2 + 30 + 28 + d;
                    break;
                case 6:
                    remainingDays = 31*3 + 30 + 28 + d;
                    break;
                case 7:
                    remainingDays = 31*3 + 30*2 + 28 + d;
                    break;
                case 8:
                    remainingDays = 31*4 + 30*2 + 28 + d;
                    break;
                case 9:
                    remainingDays = 31*5 + 30*2 + 28 + d;
                    break;
                case 10:
                    remainingDays = 31*5 + 30*3 + 28 + d;
                    break;
                case 11:
                    remainingDays = 31*6 + 30*3 + 28 + d;
                    break;
                default:
                    remainingDays = 31*6 + 30*4 + 28 + d;
                    break;
            }
            remainingDays--;
            // Java days of the week index:
            //     1: Sunday
            //     2: Monday
            //     3: Tuesday
            //     4: Wednesday
            //     5: Thursday
            //     6: Friday
            //     7: Saturday
            //
            // 6 is added since the epoch date was a saturday and then 1 is
            // added to make it 1-based
            return ((6 + wholeYearDays + remainingDays) % 7) + 1;
        }

        public String dayOfTheWeek() {
            switch (weekday()) {
                case 1:
                    return "Sunday";
                case 2:
                    return "Monday";
                case 3:
                    return "Tuesday";
                case 4:
                    return "Wednesday";
                case 5:
                    return "Thursday";
                case 6:
                    return "Friday";
                case 7:
                    return "Saturday";
            }
            throw new RuntimeException("Unexpected error");
        }

        private void validateDate(int y, int m, int d) {
            if (y < 0)
                throw new IllegalArgumentException("year cannot be negative");
            if (m < 1 || m > 12)
                throw new IllegalArgumentException("month must be between 1 and 12");
            if (d < 1 || d > 31)
                throw new IllegalArgumentException("day must be between 1 and 31");
            switch (m) {
                // Handle february
                case 2:
                    // Leap years
                    if (isLeapYear(y)) {
                        if (d > 29)
                            throw new IllegalArgumentException(
                                "Leap year. Month 2 does not have " + d + " days"
                            );
                    } else {
                        if (d > 28)
                            throw new IllegalArgumentException(
                                "Not leap year. Month 2 does not have " + d + " days"
                            );
                    }
                    break;
                // Handle the rest of the months that cannot have 31 days
                case 4: case 6: case 9: case 11:
                    if (d == 31)
                        throw new IllegalArgumentException("month " + m + " does not have 31 days");
                    break;
            }
        }

        public static boolean isLeapYear(int y) {
            if (y % 4 != 0) return false;
            else if (y % 100 != 0) return true;
            else if (y % 400 != 0) return false;
            return true;
        }

        public static Date parseDate(String dateStr) {
            String[] fields = dateStr.split("-");
            if (fields.length != 3)
                throw new IllegalArgumentException("Bad date format");
            int year = Integer.parseInt(fields[0]);
            int month = Integer.parseInt(fields[1]);
            int day = Integer.parseInt(fields[2]);
            return new Date(year, month, day);
        }

    }

    static class Transaction {

        private final String who;
        private final Date when;
        private final double amount;

        public Transaction(String who, Date when, double amount) {
            this.who = who;
            this.when = when;
            this.amount = amount;
        }

        public String who() {
            return who;
        }

        public Date when() {
            return when;
        }

        public double amount() {
            return amount;
        }

        public String toString() {
            return String.format("%s %s %.5f", who, when.toString(), amount);
        }

        public static Transaction parseTransaction(String transaction) {
            String[] fields = transaction.split(" ");
            if (fields.length != 3)
                throw new IllegalArgumentException("Bad transaction format");
            for (int i = 0; i < 3; i++)
                if (fields[i].isBlank())
                    throw new IllegalArgumentException("Fields cannot be empty");
            return new Transaction(
                fields[0], Date.parseDate(fields[1]), Double.parseDouble(fields[2])
            );
        }

    }

    public static void main(String[] args) {
        // Test date parsing
        Date date;
        date = Date.parseDate("2020-01-10");
        assert date.year() == 2020;
        assert date.month() == 1;
        assert date.day() == 10;

        boolean error;
        error = false;
        try {
            date = Date.parseDate("2020/01-10");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().equals("Bad date format");
        }
        assert error;

        error = false;
        try {
            date = Date.parseDate("--");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().equals("Bad date format");
        }
        assert error;

        error = false;
        try {
            date = Date.parseDate("asd-asd-sd");
        } catch (NumberFormatException e) {
            error = true;
        }
        assert error;

        error = false;
        try {
            date = Date.parseDate("2020-02-30");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().contains("Month 2 does not have");
        }
        assert error;

        // Test transaction parsing
        Transaction transaction;
        transaction = Transaction.parseTransaction("someone 2020-01-10 40.5");
        assert transaction.who().equals("someone");
        assert transaction.when().year() == 2020;
        assert transaction.when().month() == 1;
        assert transaction.when().day() == 10;
        assert transaction.amount() == 40.5;

        error = false;
        try {
            transaction = Transaction.parseTransaction("  ");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().equals("Bad transaction format");
        }
        assert error;

        error = false;
        try {
            transaction = Transaction.parseTransaction("2020-01-10 40.5");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().equals("Bad transaction format");
        }
        assert error;

        error = false;
        try {
            transaction = Transaction.parseTransaction(" 2020-01-10 40.5");
        } catch (IllegalArgumentException e) {
            error = true;
            assert e.getMessage().equals("Fields cannot be empty");
        }
        assert error;

        error = false;
        try {
            transaction = Transaction.parseTransaction("someone 2020-01-10 asd");
        } catch (NumberFormatException e) {
            error = true;
        }
        assert error;
    }
}
