import edu.princeton.cs.algs4.Point2D;

class Main {
    static Point2D[] findClosestPointsPair(Point2D[] points) {
        Point2D[] pair = new Point2D[2];
        double shortestDistance = Double.POSITIVE_INFINITY;
        for (int i = 0; i < points.length; i++) {
            for (int j = i+1; j < points.length; j++) {
                double dist = points[i].distanceTo(points[j]);
                if (dist < shortestDistance) {
                    pair[0] = points[i];
                    pair[1] = points[j];
                    shortestDistance = dist;
                }
            }
        }
        return pair;
    }

    public static void main(String[] args) {
        if (args.length != 1)
            throw new RuntimeException("Pass the number of points (N)");
        int n = Integer.parseInt(args[0]);
        if (n < 2)
            throw new RuntimeException("N must be at least 2");
        Point2D[] points = new Point2D[n];
        for (int i = 0; i < n; i++)
            points[i] = new Point2D(Math.random(), Math.random());
        Point2D[] closestPair = findClosestPointsPair(points);
        double dist = closestPair[0].distanceTo(closestPair[1]);
        System.out.printf(
            "Closest distance: %.5f. Points: %s %s\n",
            dist, closestPair[0], closestPair[1]
        );
    }
}
