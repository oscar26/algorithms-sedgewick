class Main {
    static class Date {

        private final int value;

        public Date(int y, int m, int d) {
            value = y*512 + m*32 + d;
        }

        public int year() {
            return value/512;
        }

        public int month() {
            return (value/32) % 16;
        }

        public int day() {
            return value % 32;
        }

        public String toString() {
            return String.format("%04d-%02d-%02d", year(), month(), day());
        }

        public boolean equals(Object x) {
            if (this == x) return true;
            if (x == null) return false;
            if (this.getClass() != x.getClass()) return false;
            Date that = (Date) x;
            if (this.value != that.value) return false;
            return true;
        }
    }

    static class Transaction {

        private final String who;
        private final Date when;
        private final double amount;

        public Transaction(String who, Date when, double amount) {
            this.who = who;
            this.when = when;
            this.amount = amount;
        }

        public String who() {
            return who;
        }

        public Date when() {
            return when;
        }

        public double amount() {
            return amount;
        }

        public String toString() {
            return String.format(
                "Transaction(who=%s, when=%s, amount=%.5f)",
                who, when.toString(), amount
            );
        }

        public boolean equals(Object x) {
            if (this == x) return true;
            if (x == null) return false;
            if (this.getClass() != x.getClass()) return false;
            Transaction that = (Transaction) x;
            if (!this.who.equals(that.who)) return false;
            if (!this.when.equals(that.when)) return false;
            if (this.amount != that.amount) return false;
            return true;
        }

    }

    public static void main(String[] args) {
        Date when = new Date(2020, 1, 1);
        Transaction t1 = new Transaction("Someone", when, 11.0);
        Transaction t2 = t1;
        assert t1.equals(t2);
        assert !t1.equals(null);
        assert !t1.equals("string class");
        Transaction t3 = new Transaction("Other someone", when, 11.0);
        Date otherWhen = new Date(2020, 1, 2);
        Transaction t4 = new Transaction("Someone", otherWhen, 11.0);
        Transaction t5 = new Transaction("Someone", when, 2.0);
        Transaction t6 = new Transaction("Someone", when, 11.0);
        assert !t1.equals(t3);
        assert !t1.equals(t4);
        assert !t1.equals(t5);
        assert t1.equals(t6);
    }
}
