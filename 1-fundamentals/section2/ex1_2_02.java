import edu.princeton.cs.algs4.Interval1D;
import edu.princeton.cs.algs4.StdIn;

class Main {
    public static void main(String[] args) {
        if (args.length != 1)
            throw new RuntimeException("Pass the number of intervals (N)");
        int n = Integer.parseInt(args[0]);
        Interval1D[] intervals = new Interval1D[n];
        for (int i = 0; i < n; i++) {
            String[] numberStrs = StdIn.readLine().split("\\s+");
            double boundary1 = Double.parseDouble(numberStrs[0]);
            double boundary2 = Double.parseDouble(numberStrs[1]);
            if (boundary1 <= boundary2)
                intervals[i] = new Interval1D(boundary1, boundary2);
            else
                intervals[i] = new Interval1D(boundary2, boundary1);
        }
        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                if (intervals[i].intersects(intervals[j]))
                    System.out.println("Intersection: " + intervals[i] + " " + intervals[j]);
    }
}
