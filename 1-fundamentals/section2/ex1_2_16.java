class Main {

    static class Rational {

        private final long num;
        private final long den;

        public Rational(long numerator, long denominator) {
            if (denominator == 0)
                throw new ArithmeticException("Division by zero");
            long commonFactor = gcd(numerator, denominator);
            if (commonFactor < 0)
                commonFactor = -commonFactor;
            // If the number is negative, put the sign in the numerator
            boolean numNegative = numerator < 0;
            boolean denNegative = denominator < 0;
            if ((numNegative && denNegative) || (!numNegative && denNegative)) {
                this.num = -numerator/commonFactor;
                this.den = -denominator/commonFactor;
            } else {
                this.num = numerator/commonFactor;
                this.den = denominator/commonFactor;
            }
        }

        public long numerator() {
            return num;
        }

        public long denominator() {
            return den;
        }

        public Rational plus(Rational b) {
            long newDen = lcm(den, b.denominator());
            long newNum = num*(newDen / den) + b.numerator()*(newDen / b.denominator());
            return new Rational(newNum, newDen);
        }

        public Rational minus(Rational b) {
            return this.plus(new Rational(-1*b.numerator(), b.denominator()));
        }

        public Rational times(Rational b) {
            long numAdenBcommonFactor = gcd(num, b.denominator());
            long denAnumBcommonFactor = gcd(den, b.numerator());
            // Reducing to least common terms to lower the risk of overflow
            long numA = num / numAdenBcommonFactor;
            long denA = den / denAnumBcommonFactor;
            long numB = b.numerator() / denAnumBcommonFactor;
            long denB = b.denominator() / numAdenBcommonFactor;
            return new Rational(numA*numB, denA*denB);
        }

        public Rational divides(Rational b) {
            // Division by zero is handled by the constructor
            return this.times(new Rational(b.denominator(), b.numerator()));
        }

        public boolean equals(Object x) {
            if (this == x) return true;
            if (x == null) return false;
            if (this.getClass() != x.getClass()) return false;
            Rational that = (Rational) x;
            if (num != that.numerator()) return false;
            if (den != that.denominator()) return false;
            return true;

        }

        public String toString() {
            return String.format("%d/%d", num, den);
        }

        // With help from wikipedia
        public static long lcm(long a, long b) {
            if (a == 0 && b == 0)
                throw new ArithmeticException("Division by zero");
            if (a < 0) a = -a;
            if (b < 0) b = -b;
            return (a / gcd(a, b)) * b;
        }

        public static long gcd(long p, long q) {
            if (q == 0) return p;
            long r = p % q;
            return gcd(q, r);
        }
    }

    public static void main(String[] args) {
        // Test zero denominator check
        Rational number;
        boolean zeroException = false;
        try {
            number = new Rational(1, 0);
        } catch (ArithmeticException e) {
            zeroException = true;
        }
        assert zeroException;

        // Test construction
        number = new Rational(0, 1);
        assert number.numerator() == 0;
        assert number.denominator() == 1;
        number = new Rational(1, 1);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(3, 2);
        assert number.numerator() == 3;
        assert number.denominator() == 2;
        number = new Rational(2, 3);
        assert number.numerator() == 2;
        assert number.denominator() == 3;

        // Test sign normalization. Sign must end up in the numerator. If
        // numerator and denominator and both negative, the new rational number
        // must be positive
        number = new Rational(-1, 1);
        assert number.numerator() == -1;
        assert number.denominator() == 1;
        number = new Rational(1, -1);
        assert number.numerator() == -1;
        assert number.denominator() == 1;
        number = new Rational(-1, -1);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(-127, 3);
        assert number.numerator() == -127;
        assert number.denominator() == 3;
        number = new Rational(127, -3);
        assert number.numerator() == -127;
        assert number.denominator() == 3;
        number = new Rational(-127, -3);
        assert number.numerator() == 127;
        assert number.denominator() == 3;

        // Test common factor elimination during construction
        number = new Rational(2, 2);
        assert number.numerator() == 1;
        assert number.denominator() == 1;
        number = new Rational(12, 9);
        assert number.numerator() == 4;
        assert number.denominator() == 3;
        number = new Rational(0, 4);
        assert number.numerator() == 0;
        assert number.denominator() == 1;
        number = new Rational(0, 7);
        assert number.numerator() == 0;
        assert number.denominator() == 1;

        // Test toString()
        number = new Rational(1, 1);
        assert number.toString().equals("1/1");
        number = new Rational(0, 1);
        assert number.toString().equals("0/1");
        number = new Rational(2, 1);
        assert number.toString().equals("2/1");
        number = new Rational(1, 2);
        assert number.toString().equals("1/2");
        number = new Rational(-1, 1);
        assert number.toString().equals("-1/1");
        number = new Rational(-127, 3);
        assert number.toString().equals("-127/3");

        // Test equals
        Rational numberA;
        Rational numberB;
        assert number.equals(number);
        assert !number.equals(null);
        assert !number.equals("string class");
        numberA = new Rational(2, 1);
        numberB = new Rational(1, 1);
        assert !numberA.equals(numberB);
        numberA = new Rational(1, 2);
        assert !numberA.equals(numberB);
        numberA = new Rational(1, 1);
        assert numberA.equals(numberB);

        Rational result;
        // Test addition
        result = (new Rational(1, 1)).plus(new Rational(1, 1));
        assert result.numerator() == 2;
        assert result.denominator() == 1;
        result = (new Rational(1, 1)).plus(new Rational(-1, 1));
        assert result.numerator() == 0;
        assert result.denominator() == 1;
        result = (new Rational(-1, 1)).plus(new Rational(1, 1));
        assert result.numerator() == 0;
        assert result.denominator() == 1;
        result = (new Rational(-1, 1)).plus(new Rational(-1, 1));
        assert result.numerator() == -2;
        assert result.denominator() == 1;
        result = (new Rational(-10, 5)).plus(new Rational(3, 2));
        assert result.numerator() == -1;
        assert result.denominator() == 2;
        result = (new Rational(3, 2)).plus(new Rational(-10, 5));
        assert result.numerator() == -1;
        assert result.denominator() == 2;
        result = (new Rational(12, 9)).plus(new Rational(8, 6));
        assert result.equals(new Rational(8, 3));

        // Test subtraction
        result = (new Rational(12, 9)).minus(new Rational(8, 6));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(0, 1)).minus(new Rational(0, 1));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(1, 1)).minus(new Rational(0, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(2, 1)).minus(new Rational(1, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(3, 2)).minus(new Rational(1, 2));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(-10, 9)).minus(new Rational(-9, 5));
        assert result.equals(new Rational(31, 45));

        // Test multiplication
        result = (new Rational(1, 1)).times(new Rational(0, 1));
        assert result.equals(new Rational(0, 1));
        result = (new Rational(1, 9)).times(new Rational(9, 1));
        assert result.equals(new Rational(1, 1));
        result = (new Rational(1, 1)).times(new Rational(-1, 1));
        assert result.equals(new Rational(-1, 1));
        result = (new Rational(100, 99)).times(new Rational(1, 2));
        assert result.equals(new Rational(50, 99));
        result = (new Rational(100, 99)).times(new Rational(9, 4));
        assert result.equals(new Rational(25, 11));
        result = (new Rational(23, 17)).times(new Rational(2, 3));
        assert result.equals(new Rational(46, 51));

        // Test division by zero handling
        zeroException = false;
        try {
            result = (new Rational(1, 1)).divides(new Rational(0, 1));
        } catch (ArithmeticException e) {
            zeroException = true;
        }
        assert zeroException;

        // Test division
        result = (new Rational(27, 13)).divides(new Rational(1, 1));
        assert result.equals(new Rational(27, 13));
        result = (new Rational(1, 9)).divides(new Rational(9, 1));
        assert result.equals(new Rational(1, 81));
        result = (new Rational(1, 1)).divides(new Rational(-1, 1));
        assert result.equals(new Rational(-1, 1));
        result = (new Rational(23, 17)).divides(new Rational(2, 3));
        assert result.equals(new Rational(69, 34));

        // Tests from: https://algs4.cs.princeton.edu/12oop/Rational.java.html

        Rational x, y, z;

        // 1/2 + 1/3 = 5/6
        x = new Rational(1, 2);
        y = new Rational(1, 3);
        z = x.plus(y);
        System.out.println(z);

        // 8/9 + 1/9 = 1
        x = new Rational(8, 9);
        y = new Rational(1, 9);
        z = x.plus(y);
        System.out.println(z);

        // 1/200000000 + 1/300000000 = 1/120000000
        x = new Rational(1, 200000000);
        y = new Rational(1, 300000000);
        z = x.plus(y);
        System.out.println(z);

        // 1073741789/20 + 1073741789/30 = 1073741789/12
        x = new Rational(1073741789, 20);
        y = new Rational(1073741789, 30);
        z = x.plus(y);
        System.out.println(z);

        //  4/17 * 17/4 = 1
        x = new Rational(4, 17);
        y = new Rational(17, 4);
        z = x.times(y);
        System.out.println(z);

        // 3037141/3247033 * 3037547/3246599 = 841/961
        x = new Rational(3037141, 3247033);
        y = new Rational(3037547, 3246599);
        z = x.times(y);
        System.out.println(z);

        // 1/6 - -4/-8 = -1/3
        x = new Rational(1, 6);
        y = new Rational(-4, -8);
        z = x.minus(y);
        System.out.println(z);
    }
}
