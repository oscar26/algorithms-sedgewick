/*
 * Using the two-point form of the linear equation [1] we can avoid dealing
 * with the special case when the line is parallel to an axis. If that equation
 * holds, then the three points (x,y), (x1, y1) and (x2,y2) are collinear.
 *
 * For the second part of the exercise, let y1 = 0, y2 = -1 and y = 1, then the equation becomes:
 *
 *   -2*x1 + x2 + x = 0
 *
 * And lastly, we multiply x1 by -0.5 before applying the equation to cancel
 * out the -2 coefficient. Here, we represent a three sum triplet with the
 * points (x,1), (x1,0) and (x2,-1) to convert the problem into a collinearity
 * check. And, if the equation holds, the three elements sum to 0.
 *
 * [1] https://en.wikipedia.org/wiki/Linear_equation#Two-point_form
 *
 * */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.ThreeSumFast;

class ThreeCollinearity {

    private static final double TOLERANCE = 1e-9;

    static int count(Point2D[] points) {
        int n = points.length;
        int result = 0;
        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                for (int k = j+1; k < n; k++)
                    if (areCollinear(points[i], points[j], points[k]))
                        result++;
        return result;
    }

    static int threeSumCount(int[] values) {
        int n = values.length;
        int result = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                for (int k = j+1; k < n; k++) {
                    Point2D point1 = new Point2D(-0.5*values[i], 0);
                    Point2D point2 = new Point2D(values[j], -1);
                    Point2D point3 = new Point2D(values[k], 1);
                    if (areCollinear(point1, point2, point3))
                        result++;
                }
            }
        }
        return result;
    }

    static boolean areCollinear(Point2D point1, Point2D point2, Point2D point3) {
        // Two-point form of the linear equation:
        //   https://en.wikipedia.org/wiki/Linear_equation#Two-point_form
        //
        // This form has the advantage of avoiding problems with
        // lines parallel to any axis
        double term1 = (point2.x()-point1.x()) * (point3.y()-point1.y());
        double term2 = (point2.y()-point1.y()) * (point3.x()-point1.x());
        return Math.abs(term1-term2) < TOLERANCE;
    }
}

class Main {
    static void testCollinearity() {
        int[][][] testCases = {
            {},
            {{0,0}, {0,1}},
            {{1,1}, {2,2}, {3,4}},
            {{0,0}, {1,1}, {2,2}},
            {{0,0}, {0,1}, {0,2}},
            {{1,1}, {2,1}, {3,1}},
            {{0,0}, {1,1}, {2,2}, {3,3}},
            {{0,0}, {1,1}, {2,2}, {3,1}}
        };
        int[] expectedResult = {
            0,
            0,
            0,
            1,
            1,
            1,
            4,
            1
        };
        for (int i = 0; i < testCases.length; i++) {
            int n = testCases[i].length;
            Point2D[] points = new Point2D[n];
            for (int j = 0; j < n; j++)
                points[j] = new Point2D(testCases[i][j][0], testCases[i][j][1]);
            int result = ThreeCollinearity.count(points);
            int expected = expectedResult[i];
            assert result == expected : String.format("Case %d. Expected: %d, got %d", i, expected, result);
        }
    }

    static void testThreeSum() {
        int[][] testCases = {
            {},
            {0, 1},
            {0, 1, 2},
            {1, 2, -3},
            {-3, -4, 2, 4, 1},
            {0, 1, 2, 3, 4},
            {-4, -3, -2, -1, 0},
            {-3, -2, -1, 0, 1, 2},
            {-2, -1, 0, 2, 3},
            {-3, -2, -1, 0, 1, 2, 3}
        };
        for (int i = 0; i < testCases.length; i++) {
            int result = ThreeCollinearity.threeSumCount(testCases[i]);
            int expected = ThreeSumFast.count(testCases[i]);
            assert result == expected : String.format("Case %d. Expected: %d, got %d", i, expected, result);
        }
    }

    public static void main(String[] args) {
        testCollinearity();
        testThreeSum();
    }
}
