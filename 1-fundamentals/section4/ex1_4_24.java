/*
 * The problem statement is clearer in the book web site:
 *
 *   Throwing eggs from a building. Suppose that you have an N-story building
 *   and plenty of eggs. Suppose also that an egg is broken if it is thrown
 *   off floor F or higher, and unbroken otherwise. First, devise a strategy
 *   to determine the value of F such that the number of broken eggs is ~ lg
 *   N when using ~ lg N throws, then find a way to reduce the cost to ~ 2 lg
 *   F when N is much larger than F.
 *
 * An array of booleans is used to represent the floors of a building and
 * whether an egg breaks when throwing it from a given floor. It is not space
 * efficient but it is cool.
 *
 * The lg(N) solution is just a binary search. The 2lg(F) solution consists in
 * starting from the first floor and advancing them in exponential steps until
 * we find a floor where eggs break and narrow the search between that floor
 * and the previous step. The key insight is to start from the first floor as 0
 * <= F <= N, where 0 denotes the first floor. The exponential step in this
 * solution is a simple doubling, while other solutions found elsewhere use
 * Fibonacci.
 *
 * An extra optimization is to throw an egg from the last floor. If it doesn't
 * break, then there are no floors from which an egg could be broken.
 *
 *
 * As an example, consider a building with a 100 floors and any of its first 10
 * floors (~log(100)) is the floor at which the eggs start breaking. The count
 * of throws a broken eggs would be like this,
 *
 * Unmodified binary search
 * ------------------------
 *
 * Break floor | Thrown eggs | Broken eggs | log(N) |   2log(F)
 *       0            6             7         6.64   -Infinity
 *       1            6             6         6.64        0.00
 *       2            6             5         6.64        2.00
 *       3            6             6         6.64        3.17
 *       4            6             5         6.64        4.00
 *       5            6             4         6.64        4.64
 *       6            6             6         6.64        5.17
 *       7            6             5         6.64        5.61
 *       8            6             4         6.64        6.00
 *       9            6             5         6.64        6.34
 *      10            6             4         6.64        6.64
 *
 *
 * Less Waste
 * ----------
 *
 * Break floor | Thrown eggs | Broken eggs | log(N) |   2log(F)
 *       0            1             1         6.64   -Infinity
 *       1            3             1         6.64        0.00
 *       2            5             2         6.64        2.00
 *       3            5             1         6.64        3.17
 *       4            7             3         6.64        4.00
 *       5            7             2         6.64        4.64
 *       6            7             2         6.64        5.17
 *       7            7             1         6.64        5.61
 *       8            9             4         6.64        6.00
 *       9            9             3         6.64        6.34
 *      10            9             3         6.64        6.64
 *
 * Note that in the less-waste version, the number of broken eggs is lower in
 * comparison to the unmodified binary search solution and well within the
 * expected 2log(F) complexity.
 *
 * */

import java.util.Arrays;
import edu.princeton.cs.algs4.Counter;
import edu.princeton.cs.algs4.Queue;

class EggThrower {
    static int findFloor(boolean[] building, Counter thrownEggs, Counter brokenEggs) {
        if (building.length == 0)
            return -1;
        int low = 0;
        int high = building.length;
        int leftMost = -1;  // variable used to prevent an extra array access (less thrown eggs)
        while (low < high) {
            int mid = low + (high-low)/2;
            thrownEggs.increment();
            if (building[mid]) {
                // Keep searching for the lowest floor where the eggs start
                // breaking
                leftMost = mid;
                high = mid - 1;
                brokenEggs.increment();
            } else {
                low = mid + 1;
            }
        }
        if (low < building.length && building[low]) {
            brokenEggs.increment();
            return low;
        }
        return leftMost;
    }

    static int findFloorLessWaste(boolean[] building, Counter thrownEggs, Counter brokenEggs) {
        if (building.length == 0)
            return -1;
        int floor = 0;
        int power = 1;
        int leftMost = -1;
        boolean shrink = false;
        boolean shrinkLeft = false;
        while (power > 0) {
            thrownEggs.increment();
            if (building[floor]) {
                brokenEggs.increment();
                leftMost = floor;
                shrink = true;
                shrinkLeft = true;
            } else {
                if (!shrink && floor + power >= building.length)
                    shrink = true;
                shrinkLeft = false;
            }
            if (shrink) {
                do {
                    power >>= 1;
                } while (!shrinkLeft && floor + power >= building.length);
                floor += shrinkLeft ? -power : power;
            } else {
                floor += shrinkLeft ? -power : power;
                power <<= 1;
            }
        }
        return leftMost;
    }
}

class Main {
    static void testSpecialCases() {
        boolean[][] buildings = {
            {},
            {false},
            {true},
            {false, true},
            {true, true},
            {false, false, false, false, false},
            {true, true, true, true, true},
            {false, false, false, false, true, true},
            {false, false, false, false, false, true},
        };
        int[] expectedFloor = {
            -1,
            -1,
            0,
            1,
            0,
            -1,
            0,
            4,
            5
        };
        Counter thrownEggs = new Counter("ThrownEggs");
        Counter brokenEggs = new Counter("BrokenEggs");
        for (int i = 0; i < buildings.length; i++) {
            int floor = expectedFloor[i];
            int floorLogN = EggThrower.findFloor(buildings[i], thrownEggs, brokenEggs);
            int floorLogF = EggThrower.findFloorLessWaste(buildings[i], thrownEggs, brokenEggs);
            assert floorLogN == floor : String.format("(logN) Case %d. Expected: %d, got: %d", i, floor, floorLogN);
            assert floorLogF == floor : String.format("(logF) Case %d. Expected: %d, got: %d", i, floor, floorLogF);
        }
    }

    static void testIncreasingFloors() {
        // Test exhaustive combinations of buildings and breaking floors
        int maxTotalFloors = 100;
        int step = 1;
        testVariousInputSizes(false, maxTotalFloors, step, step);
        testVariousInputSizes(true, maxTotalFloors, step, step);
        // Count throws and broken eggs
        printResults(testVariousInputSizes(false, 100, 100, 1), "Normal");
        printResults(testVariousInputSizes(true, 100, 100, 1), "Less Waste");
    }

    static Queue<int[]> testVariousInputSizes(boolean optimized, int maxTotalFloors, int totalFloorSteps, int breakFloorSteps) {
        Queue<int[]> results = new Queue<>();
        for (int totalFloors = 0; totalFloors <= maxTotalFloors; totalFloors += totalFloorSteps) {
            for (int breakFloor = 0; breakFloor <= totalFloors; breakFloor += breakFloorSteps) {
                boolean[] building = new boolean[totalFloors];
                if (totalFloors > 0) {
                    Arrays.fill(building, 0, breakFloor, false);
                    if (breakFloor < totalFloors)
                        Arrays.fill(building, breakFloor, totalFloors, true);
                }
                Counter thrownEggs = new Counter("ThrownEggs");
                Counter brokenEggs = new Counter("BrokenEggs");
                int foundFloor;
                String errorMsg = "";
                if (optimized) {
                    foundFloor = EggThrower.findFloorLessWaste(building, thrownEggs, brokenEggs);
                    errorMsg = String.format(
                        "Less Waste. Floors: %d, break floor: %d, got: %d",
                        totalFloors, breakFloor, foundFloor
                    );
                } else {
                    foundFloor = EggThrower.findFloor(building, thrownEggs, brokenEggs);
                    errorMsg = String.format(
                        "Normal. Floors: %d, break floor: %d, got: %d",
                        totalFloors, breakFloor, foundFloor
                    );
                }
                if (totalFloors == 0 || breakFloor >= totalFloors)
                    assert foundFloor == -1 : errorMsg;
                else
                    assert foundFloor == breakFloor : errorMsg;
                results.enqueue(new int[] {totalFloors, breakFloor, thrownEggs.tally(), brokenEggs.tally()});
            }
        }
        return results;
    }

    static void printResults(Queue<int[]> results, String title) {
        System.out.printf("\n\n%s\n", title);
        System.out.println("Total floors | Break floor | Thrown eggs | Broken eggs | log(N) | 2log(F)");
        for (int[] testResult : results) {
            int totalFloors = testResult[0];
            int breakFloor = testResult[1];
            int thrownEggs = testResult[2];
            int brokenEggs = testResult[3];
            double complexityTotalFloors = log2(totalFloors);
            double complexityBreakFloor = 2*log2(breakFloor);
            System.out.printf(
                "%6d  %6d  %6d  %6d  %10.2f  %10.2f\n",
                totalFloors, breakFloor, thrownEggs, brokenEggs, complexityTotalFloors, complexityBreakFloor
            );
        }
    }

    static double log2(double value) {
        return Math.log(value)/Math.log(2);
    }

    public static void main(String[] args) {
        testSpecialCases();
        testIncreasingFloors();
    }
}
