import java.util.Arrays;
import edu.princeton.cs.algs4.StdRandom;

class BitomicSearch {
    static int find(int[] array, int key) {
        if (array.length == 0)
            return -1;
        if (array.length == 1) {
            if (array[0] == key)
                return 0;
            return -1;
        }
        int high = array.length - 1;
        int low = 0;
        int maxPos = 0;
        while (low < high) {
            maxPos = low + (high-low)/2;
            if (array[maxPos+1] > array[maxPos])
                low = maxPos + 1;
            else
                high = maxPos;
        }
        if (array[maxPos+1] > array[maxPos])
            maxPos++;
        // Search elements to the left of the maximum (increasing portion)
        high = maxPos;
        low = 0;
        while (low <= high) {
            int mid = low + (high-low)/2;
            if (key > array[mid])
                low = mid + 1;
            else if (key < array[mid])
                high = mid - 1;
            else
                return mid;
        }
        // Search elements to the right of the maximum (decreasing portion)
        high = array.length - 1;
        low = maxPos;
        while (low <= high) {
            int mid = low + (high-low)/2;
            if (key > array[mid])
                high = mid - 1;
            else if (key < array[mid])
                low = mid + 1;
            else
                return mid;
        }
        return -1;
    }
}

class Main {
    static void testSpecialCases() {
        int[][] arrays = {
            {1, 2, 3, 4, 5, 6, 7, 8},
            {-1, -2, -3, -4, -5, -6, -7, -8},
            {1, 2, 3, -4, -5, -6, -7, -8},
        };
        int[][] cases = {
            {0,  1,  0},
            {0,  8,  7},
            {0,  0, -1},
            {0,  9, -1},
            {1, -1,  0},
            {1, -8,  7},
            {1,  0, -1},
            {1, -9, -1},
            {2,  3,  2},
            {2, -4,  3},
            {2,  1,  0},
            {2, -8,  7},
            {2,  0, -1}
        };
        for (int i = 0; i < cases.length; i++) {
            int[] array = arrays[cases[i][0]];
            int key = cases[i][1];
            int expectedKey = cases[i][2];
            int result = BitomicSearch.find(array, key);
            assert result == expectedKey : String.format("Case %d. Expected: %d, got: %d", i, expectedKey, result);
        }
    }

    static void testRandom() {
        int trials = 100000;
        int n = 100;
        for (int i = 0; i < trials; i++) {
            int[] array = createBitomicArray(n);
            int keyPos = StdRandom.uniform(n);
            int result = BitomicSearch.find(array, array[keyPos]);
            assert result == keyPos;
        }
    }

    static int[] createBitomicArray(int n) {
        int maxPos = StdRandom.uniform(n);
        int[] increasing = StdRandom.permutation(n, maxPos);
        int[] decreasing = StdRandom.permutation(n, n-maxPos);
        Arrays.sort(increasing);
        Arrays.sort(decreasing);
        int[] array = new int[n];
        int j = 0;
        for (int i = 0; i < increasing.length; i++)
            array[j++] = increasing[i];
        for (int i = 0; i < decreasing.length; i++)
            array[j++] = -1 - decreasing[i];  // Avoid extra 0
        return array;
    }

    public static void main(String[] args) {
        testSpecialCases();
        testRandom();
    }
}
