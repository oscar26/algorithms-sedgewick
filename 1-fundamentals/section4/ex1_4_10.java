class Main {
    static int binarySearch(int[] a, int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else {
                if (mid == 0)
                    return mid;
                // If the previous element is equal to the key,
                // keep searching to the left
                if (a[mid-1] == key)
                    hi = mid - 1;
                else
                    return mid;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[][] testCases = {
            {},
            {0, 1, 2, 3, 4},
            {0, 1, 1, 3, 4},
            {1, 1, 1, 3, 4},
            {0, 0, 1, 1, 2, 2},
            {1, 1, 1, 1, 1, 1, 1, 1}
        };
        int[] keys = {
            1,
            0,
            1,
            1,
            2,
            1
        };
        int[] expectedResult = {
            -1,
            0,
            1,
            0,
            4,
            0
        };
        for (int i = 0; i < testCases.length; i++)
            assert expectedResult[i] == binarySearch(testCases[i], keys[i])
                : String.format("case %d failed", i);
    }
}
