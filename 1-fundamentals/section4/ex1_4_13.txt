Note: classes were stipped out of code not relevant to the memory calculation.

a. Accumulator

class Accumulator {
    int n;
    double sum;
    double mu;
}

Object overhead: 16 bytes
              n:  4 bytes
            sum:  8 bytes
             mu:  8 bytes
        padding:  4 bytes
-------------------------
          total: 40 bytes


b. Transaction

class Transaction {
    String who;
    Date when;
    double amount;
}

Object overhead:           16 bytes
      who (ref):            8 bytes
     when (ref):            8 bytes
         amount:            8 bytes
-----------------------------------
       subtotal:           40 bytes
      who (obj): 40 + 24 + 2N bytes
     when (obj):           32 bytes
-----------------------------------
          total:     136 + 2N bytes

where N is the number of characters in array within the string object and the
total is rounded up to a multiple of 8.


c. FixedCapacityStackOfStrings with capacity C and N entries

class FixedCapacityStackOfStrings {
    String[] a;
    int N;
}

Object overhead:                      16 bytes
        a (ref):                       8 bytes
              N:                       4 bytes
        padding:                       4 bytes
--------------------------------------------
       subtotal:                      32 bytes
      a (array):                 24 + 8C bytes
--------------------------------------------
       subtotal:                 56 + 8C bytes
     a (values):      N*(40 + 24 + 2m_i) bytes
--------------------------------------------
          total: 56 + 8C + N*(64 + 2m_i) bytes

where C is length or capacity of the array a, N is actual number of strings in
the array a, m_i represents the length of the character array of the string at
a[i] for i natural and the total is rounded up to the next multiple of 8.


d. Point2D

class Point2D {
    double x;
    double y;
}

Object overhead: 16 bytes
              x:  8 bytes
              y:  8 bytes
-------------------------
          total: 32 bytes


e. Interval1D

class Interval1D {
    double min;
    double max;
}

Object overhead: 16 bytes
            min:  8 bytes
            max:  8 bytes
-------------------------
          total: 32 bytes


f. Interval2D

class Interval2D {
    Interval1D x;
    Interval2D y;
}

Object overhead: 16 bytes
        x (ref):  8 bytes
        x (obj): 32 bytes
        y (ref):  8 bytes
        y (obj): 32 bytes
-------------------------
          total: 96 bytes


g. Double

Object overhead: 16 bytes
          value:  8 bytes
-------------------------
          total: 24 bytes
