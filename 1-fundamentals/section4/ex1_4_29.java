import edu.princeton.cs.algs4.Stack;

class TwoStackSteque<T> {

    private Stack<T> stack1;
    private Stack<T> stack2;

    public TwoStackSteque() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    public boolean isEmpty() {
        return stack1.isEmpty() && stack2.isEmpty();
    }

    public int size() {
        return stack1.size() + stack2.size();
    }

    public void push(T item) {
        if (!stack2.isEmpty())
            while (!stack2.isEmpty())
                stack1.push(stack2.pop());
        stack1.push(item);
    }

    public void enqueue(T item) {
        if (!stack1.isEmpty())
            while (!stack1.isEmpty())
                stack2.push(stack1.pop());
        stack2.push(item);
    }

    public T pop() {
        if (isEmpty())
            throw new RuntimeException("TwoStackSteque is empty");
        if (!stack2.isEmpty())
            while (!stack2.isEmpty())
                stack1.push(stack2.pop());
        return stack1.pop();
    }

}

class Main {
    static void testEnqueue() {
        TwoStackSteque<Integer> steque = new TwoStackSteque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.enqueue(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();

        for (int i = 1; i < 5; i++)
            steque.enqueue(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
    }

    static void testPush() {
        TwoStackSteque<Integer> steque = new TwoStackSteque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.push(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();

        for (int i = 1; i < 5; i++)
            steque.push(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
    }

    static void testPop() {
        TwoStackSteque<Integer> steque = new TwoStackSteque<>();
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.push(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();
        assert steque.pop() == 0;
        assert steque.size() == 0;
        assert steque.isEmpty();

        steque.enqueue(0);
        assert steque.size() == 1;
        assert !steque.isEmpty();
        assert steque.pop() == 0;
        assert steque.size() == 0;
        assert steque.isEmpty();

        for (int i = 0; i < 5; i++)
            steque.push(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
        int counter = 4;
        while (!steque.isEmpty()) {
            int item = steque.pop();
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter--;
        }
        assert steque.size() == 0;
        assert steque.isEmpty();
        assert counter == -1;

        for (int i = 0; i < 5; i++)
            steque.enqueue(i);
        assert steque.size() == 5;
        assert !steque.isEmpty();
        counter = 0;
        while (!steque.isEmpty()) {
            int item = steque.pop();
            assert item == counter : String.format("Expected: %d, got: %d", counter, item);
            counter++;
        }
        assert steque.size() == 0;
        assert steque.isEmpty();
        assert counter == 5;
    }

    public static void main(String[] args) {
        testEnqueue();
        testPush();
        testPop();
    }
}
