/*
 * The less waste version uses the arithmetic progression to achieve a
 * ~c*sqrt(F) complexity where F is the breaking floor.
 * */

import java.util.Arrays;
import edu.princeton.cs.algs4.Counter;
import edu.princeton.cs.algs4.Queue;

class TwoEggsThrower {
    static int findFloor(boolean[] building, Counter thrownEggs, Counter brokenEggs) {
        if (building.length == 0)
            return -1;
        int n = building.length;
        int sqrtN = (int) Math.sqrt(n);
        int i = 0;
        while (i < n) {
            thrownEggs.increment();
            if (building[i]) {
                brokenEggs.increment();
                break;
            }
            i += sqrtN;
        }
        if (i > 0)
            i -= sqrtN;
        for (int j = 0; j <= sqrtN && i < n; j++) {
            thrownEggs.increment();
            if (building[i]) {
                brokenEggs.increment();
                return i;
            }
            i++;
        }
        return -1;
    }

    static int findFloorLessWaste(boolean[] building, Counter thrownEggs, Counter brokenEggs) {
        if (building.length == 0)
            return -1;
        int n = building.length;
        int i = 0;
        int nextIncrement = 1;
        while (i < n) {
            thrownEggs.increment();
            if (building[i]) {
                brokenEggs.increment();
                break;
            }
            i += nextIncrement++;
        }
        if (i > 0)
            i -= --nextIncrement - 1;
        for (int j = 0; j <= nextIncrement && i < n; j++) {
            thrownEggs.increment();
            if (building[i]) {
                brokenEggs.increment();
                return i;
            }
            i++;
        }
        return -1;
    }
}

class Main {
    static void testSpecialCases() {
        boolean[][] buildings = {
            {},
            {false},
            {true},
            {false, true},
            {true, true},
            {false, false, false, false, false},
            {true, true, true, true, true},
            {false, false, false, false, true, true},
            {false, false, false, false, false, true},
        };
        int[] expectedFloor = {
            -1,
            -1,
            0,
            1,
            0,
            -1,
            0,
            4,
            5
        };
        Counter thrownEggs = new Counter("ThrownEggs");
        Counter brokenEggs = new Counter("BrokenEggs");
        for (int i = 0; i < buildings.length; i++) {
            int floor = expectedFloor[i];
            int floorSqrtN = TwoEggsThrower.findFloor(buildings[i], thrownEggs, brokenEggs);
            int floorSqrtF = TwoEggsThrower.findFloorLessWaste(buildings[i], thrownEggs, brokenEggs);
            assert floorSqrtN == floor : String.format("(sqrtN) Case %d. Expected: %d, got: %d", i, floor, floorSqrtN);
            assert floorSqrtF == floor : String.format("(sqrtF) Case %d. Expected: %d, got: %d", i, floor, floorSqrtF);
        }
    }

    static void testIncreasingFloors() {
        // Test exhaustive combinations of buildings and breaking floors
        int maxTotalFloors = 100;
        int step = 1;
        testVariousInputSizes(false, maxTotalFloors, step, step);
        testVariousInputSizes(true, maxTotalFloors, step, step);
        // Count throws and broken eggs
        printResults(testVariousInputSizes(false, 100, 100, 1), "Normal");
        printResults(testVariousInputSizes(true, 100, 100, 1), "Less Waste");
    }

    static Queue<int[]> testVariousInputSizes(boolean optimized, int maxTotalFloors, int totalFloorSteps, int breakFloorSteps) {
        Queue<int[]> results = new Queue<>();
        for (int totalFloors = 0; totalFloors <= maxTotalFloors; totalFloors += totalFloorSteps) {
            for (int breakFloor = 0; breakFloor <= totalFloors; breakFloor += breakFloorSteps) {
                boolean[] building = new boolean[totalFloors];
                if (totalFloors > 0) {
                    Arrays.fill(building, 0, breakFloor, false);
                    if (breakFloor < totalFloors)
                        Arrays.fill(building, breakFloor, totalFloors, true);
                }
                Counter thrownEggs = new Counter("ThrownEggs");
                Counter brokenEggs = new Counter("BrokenEggs");
                int foundFloor;
                String errorMsg = "";
                if (optimized) {
                    foundFloor = TwoEggsThrower.findFloorLessWaste(building, thrownEggs, brokenEggs);
                    errorMsg = String.format(
                        "Less Waste. Floors: %d, break floor: %d, got: %d",
                        totalFloors, breakFloor, foundFloor
                    );
                } else {
                    foundFloor = TwoEggsThrower.findFloor(building, thrownEggs, brokenEggs);
                    errorMsg = String.format(
                        "Normal. Floors: %d, break floor: %d, got: %d",
                        totalFloors, breakFloor, foundFloor
                    );
                }
                if (totalFloors == 0 || breakFloor >= totalFloors)
                    assert foundFloor == -1 : errorMsg;
                else
                    assert foundFloor == breakFloor : errorMsg;
                results.enqueue(new int[] {totalFloors, breakFloor, thrownEggs.tally(), brokenEggs.tally()});
            }
        }
        return results;
    }

    static void printResults(Queue<int[]> results, String title) {
        System.out.printf("\n\n%s\n", title);
        System.out.println("Total floors | Break floor | Thrown eggs | Broken eggs | 2*sqrt(N) | c*sqrt(F)");
        for (int[] testResult : results) {
            int totalFloors = testResult[0];
            int breakFloor = testResult[1];
            int thrownEggs = testResult[2];
            int brokenEggs = testResult[3];
            double complexityTotalFloors = 2*Math.sqrt(totalFloors);
            double complexityBreakFloor = Math.sqrt(breakFloor);
            System.out.printf(
                "%6d  %6d  %6d  %6d  %10.2f  %10.2f\n",
                totalFloors, breakFloor, thrownEggs, brokenEggs, complexityTotalFloors, complexityBreakFloor
            );
        }
    }

    public static void main(String[] args) {
        testSpecialCases();
        testIncreasingFloors();
    }
}
