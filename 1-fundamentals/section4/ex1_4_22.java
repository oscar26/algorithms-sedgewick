import java.util.Arrays;

class BinarySearchAddSubOnly {
    static int find(int[] array, int key) {
        int high = array.length - 1;
        int low = 0;
        while (low <= high) {
            int previousHalf = 0;
            int half = 1;
            int size = high - low;
            while (half < size) {
                previousHalf = half;
                half += half;
            }
            if (half > size)
                half = previousHalf;
            int mid = low + half;
            if (key > array[mid])
                low = mid + 1;
            else if (key < array[mid])
                high = mid - 1;
            else
                return mid;
        }
        return -1;
    }
}

class Main {
    public static void main(String[] args) {
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, 0) == 0;
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, 1) == 1;
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, 2) == 2;
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, 3) == 3;
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, 4) == -1;
        assert BinarySearchAddSubOnly.find(new int[] {0, 1, 2, 3}, -1) == -1;
    }
}
