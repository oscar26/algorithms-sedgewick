import java.util.Arrays;

class Main {
    static double[] closestPair(double[] a) {
        if (a.length < 2)
            throw new IllegalArgumentException("at least two elements are required");
        double[] values = new double[a.length];
        for (int i = 0; i < a.length; i++)
            values[i] = a[i];
        Arrays.sort(values);
        double smallestDiff = Double.MAX_VALUE;
        double[] pair = {values[0], values[1]};
        for (int i = 1; i < values.length; i++) {
            double diff = Math.abs(values[i] - values[i-1]);
            if (diff < smallestDiff) {
                smallestDiff = diff;
                pair = new double[] {values[i-1], values[i]};
            }
        }
        return pair;
    }

    public static void main(String[] args) {
        double[][] testCases = {
            {1.5, 2.0},
            {-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0},
            {-3.0, -2.0, -1.0, -0.5, 1.0, 2.0, 3.0}
        };
        double[][] expectedPairs = {
            {1.5, 2.0},
            {-3.0, -2.0},
            {-1.0, -0.5}
        };
        for (int i = 0; i < testCases.length; i++) {
            double[] expected = expectedPairs[i];
            double[] result = closestPair(testCases[i]);
            assert expected[0] == result[0]
                : String.format("case %d failed. Expected %.5f, got %.5f", i, expected[0], result[0]);
            assert expected[1] == result[1]
                : String.format("case %d failed. Expected %.5f, got %.5f", i, expected[1], result[1]);
        }
    }
}
