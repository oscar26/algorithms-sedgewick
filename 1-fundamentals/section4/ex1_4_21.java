import java.util.Arrays;
import edu.princeton.cs.algs4.Queue;

class StaticSetOfInts {
    private int[] a;

    public StaticSetOfInts(int[] keys) {
        a = new int[keys.length];
        for (int i = 0; i < keys.length; i++)
            a[i] = keys[i];
        Arrays.sort(a);
        if (a.length > 0) {
            Queue<Integer> dedup = new Queue<>();
            dedup.enqueue(a[0]);
            for (int i = 1; i < a.length; i++)
                if (a[i] != a[i-1])
                    dedup.enqueue(a[i]);
            a = new int[dedup.size()];
            int i = 0;
            while (!dedup.isEmpty())
                a[i++] = dedup.dequeue();
        }
    }

    public boolean contains(int key) {
        return rank(key) != -1;
    }

    public int rank(int key) {
        int high = a.length - 1;
        int low = 0;
        while (low <= high) {
            int mid = low + (high-low)/2;
            if (key > a[mid])
                low = mid + 1;
            else if (key < a[mid])
                high = mid - 1;
            else
                return mid;
        }
        return -1;
    }
}

class Main {
    public static void main(String[] args) {
        StaticSetOfInts set;
        set = new StaticSetOfInts(new int[] {0, 0, 1, 1, 1, 2, 3});
        assert set.contains(0);
        assert set.contains(1);
        assert set.contains(2);
        assert set.contains(3);
        assert !set.contains(4);
        assert !set.contains(-1);
    }
}
