import edu.princeton.cs.algs4.Stack;

class TwoStackQueue<T> {

    private Stack<T> stack1;
    private Stack<T> stack2;

    public TwoStackQueue() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    public int size() {
        return stack1.size() + stack2.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void enqueue(T item) {
        stack1.push(item);
    }

    public T dequeue() {
        if (isEmpty())
            throw new RuntimeException("queue is empty");
        if (stack2.isEmpty())
            while (!stack1.isEmpty())
                stack2.push(stack1.pop());
        return stack2.pop();
    }

}

class Main {
    public static void main(String[] args) {
        TwoStackQueue<Integer> queue = new TwoStackQueue<>();
        assert queue.isEmpty();
        queue.enqueue(1);
        assert !queue.isEmpty();
        assert queue.size() == 1;
        assert queue.dequeue() == 1;
        assert queue.isEmpty();
        for (int i = 0; i < 10; i++)
            queue.enqueue(i);
        assert !queue.isEmpty();
        assert queue.size() == 10;
        for (int i = 0; i < 10; i++)
            assert queue.dequeue() == i;
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        assert queue.dequeue() == 0;
        assert queue.dequeue() == 1;
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        assert queue.dequeue() == 2;
        assert queue.dequeue() == 3;
        assert queue.dequeue() == 4;
        assert queue.dequeue() == 5;
        queue.enqueue(10);
        assert queue.dequeue() == 6;
        assert queue.dequeue() == 7;
        assert queue.dequeue() == 8;
        assert queue.dequeue() == 9;
        assert queue.dequeue() == 10;
        assert queue.isEmpty();
    }
}
