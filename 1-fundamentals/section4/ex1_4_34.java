class Problem {
    public long counter = 0;
    public long high;
    private long previous = -1;  // [1, N]
    private long number;

    public Problem(long number, long high) {
        this.number = number;
        this.high = high;
    }

    // 1: hotter, 0: number found, -1: colder
    public int isHotter(long current) {
        counter++;
        int result = Math.abs(current - number) < Math.abs(previous - number) ? 1 : -1;
        previous = current;
        if (current == number)
            return 0;
        return result;
    }
}

class Guesser {
    public static int fast(Problem problem, int low, int high) {
        if (low > high)
            throw new IllegalArgumentException("low cannot be greater than high");
        if (low == high)
            return low;
        // Special case: first element
        int guessResult = problem.isHotter(low);
        if (guessResult == 0)
            return low;
        int previous = low;
        int current = high;
        // Number is guaranteed to be in the interval [previous, current]
        while (guessResult != 0) {
            // Last guessResult is always the current number
            guessResult = problem.isHotter(current);
            int mid = previous + (current - previous)/2;
            if (guessResult == -1) {  // Closer to previous
                guessResult = problem.isHotter(previous);  // Flip guess' previous attempt
                current = mid;
            } else if (guessResult == 1) {  // Closer to current
                guessResult = problem.isHotter(mid);  // Let mid be the new previous attempt
                previous = mid;
            }
        }
        return current;
    }

    public static long faster(Problem problem, long low, long high) {
        if (low > high)
            throw new IllegalArgumentException("low cannot be greater than high");
        int guessResult = problem.isHotter(low);  // Setting the first guess
        if (guessResult == 0)
            return low;
        long previous = low;
        long guess = high;
        long left = low;
        long right = high;
        long mid = left + (right - left)/2;
        long newMid = -1;
        while (right - left > 1) {
            guessResult = problem.isHotter(guess);
            if (guessResult == 1) {
                if (guess > previous) {
                    left = mid;
                    newMid = left + (right - left)/2;
                    low = 2*newMid - high;
                    previous = guess;
                    guess = low;
                } else {
                    right = mid;
                    newMid = left + (right - left)/2;
                    high = 2*newMid - low;
                    previous = guess;
                    guess = high;
                }
            } else if (guessResult == -1) {
                if (guess > previous) {
                    right = mid;
                    newMid = left + (right - left)/2;
                    low = 2*newMid - high;
                    previous = guess;
                    guess = low;
                } else {
                    left = mid;
                    newMid = left + (right - left)/2;
                    high = 2*newMid - low;
                    previous = guess;
                    guess = high;
                }
            } else {
                return guess;
            }
            mid = newMid;
        }
        return problem.isHotter(left) == 0 ? left : right;
    }
}

class Main {
    public static void main(String[] args) {
        int[] uppers = new int[14];
        int i = 0;
        // Even and odd number of elements
        for (int high = 1; high <= 1e6; high *= 10) {
            uppers[i++] = high;
            uppers[i++] = high + 1;
        }
        for (i = 0; i < uppers.length; i++) {
            int high = uppers[i];
            for (int number = 1; number <= high; number++) {
                Problem problem = new Problem(number, high);
                long guess = Guesser.fast(problem, 1, high);
                assert guess == number
                    : String.format(
                        "(Fast) High: %d, Expected: %d, got: %d",
                        high, number, guess
                    );
                problem = new Problem(number, high);
                guess = Guesser.faster(problem, 1, high);
                assert guess == number
                    : String.format(
                        "(Faster) High: %d, Expected: %d, got: %d",
                        high, number, guess
                    );
            }
        }
    }
}
