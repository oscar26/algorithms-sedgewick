import java.util.Arrays;
import edu.princeton.cs.algs4.In;

class TwoSum {
    // It is assumed that a is sorted
    static int count(int[] a) {
        if (a.length == 0)
            return 0;
        int i = 0;
        int j = a.length - 1;
        // If all elements are of the same sign, there are no pairs that sum up
        // to 0
        if ((a[i] >= 0 && a[j] >= 0) || (a[i] <= 0 && a[j] <= 0))
            return 0;
        int total = 0;
        while (i < j) {
            if (-a[i] > a[j]) {
                i++;
            } else if (-a[i] < a[j]) {
                j--;
            } else {
                total++;
                i++;
                j--;
            }
        }
        return total;
    }

    static int countBruteForce(int[] a) {
        int total = 0;
        for (int i = 0; i < a.length; i++)
            for (int j = i+1; j < a.length; j++)
                if (a[i] + a[j] == 0)
                    total++;
        return total;
    }
}

class ThreeSum {
    // It is assumed that a is sorted
    static int count(int[] a) {
        if (a.length == 0)
            return 0;
        // If all elements are of the same sign, there are no 3-tuples that sum
        // up to 0
        if ((a[0] >= 0 && a[a.length-1] >= 0) || (a[0] <= 0 && a[a.length-1] <= 0))
            return 0;
        int total = 0;
        for (int i = 0; i < a.length; i++) {
            int j = i + 1;
            int k = a.length - 1;
            while (j < k) {
                int partialSum = -(a[i] + a[j]);
                if (partialSum > a[k]) {
                    j++;
                } else if (partialSum < a[k]) {
                    k--;
                } else {
                    total++;
                    j++;
                    k--;
                }
            }
        }
        return total;
    }

    static int countBruteForce(int[] a) {
        int total = 0;
        for (int i = 0; i < a.length; i++)
            for (int j = i+1; j < a.length; j++)
                for (int k = j+1; k < a.length; k++)
                    if (a[i] + a[j] + a[k] == 0)
                        total++;
        return total;
    }
}

class Main {
    static void testFasterTwoSum() {
        int[][] testCases = {
            {},
            {0, 1, 2, 3, 4},
            {-4, -3, -2, -1, 0},
            {-3, -2, -1, 0, 1},
            {-2, -1, 0, 2, 3},
            {-3, -2, -1, 0, 1, 2, 3}
        };
        for (int i = 0; i < testCases.length; i++) {
            Arrays.sort(testCases[i]);
            int expected = TwoSum.countBruteForce(testCases[i]);
            int result = TwoSum.count(testCases[i]);
            assert expected == result
                : String.format("case %d failed. Expected %d, got %d", i, expected, result);
        }
    }

    static void testFasterThreeSum() {
        int[][] testCases = {
            {},
            {0, 1, 2, 3, 4},
            {-4, -3, -2, -1, 0},
            {-3, -2, -1, 0, 1, 2},
            {-2, -1, 0, 2, 3},
            {-3, -2, -1, 0, 1, 2, 3}
        };
        for (int i = 0; i < testCases.length; i++) {
            Arrays.sort(testCases[i]);
            int expected = ThreeSum.countBruteForce(testCases[i]);
            int result = ThreeSum.count(testCases[i]);
            assert expected == result
                : String.format("case %d failed. Expected %d, got %d", i, expected, result);
        }
    }

    public static void main(String[] args) {
        testFasterTwoSum();
        testFasterThreeSum();
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the file containing the numbers");
        int[] a = In.readInts(args[0]);
        Arrays.sort(a);
        System.out.println("Two sum faster: " + TwoSum.count(a));
        System.out.println("Two sum brute force: " + TwoSum.countBruteForce(a));
        System.out.println("Three sum faster: " + ThreeSum.count(a));
        System.out.println("Three sum brute force: " + ThreeSum.countBruteForce(a));
    }
}
