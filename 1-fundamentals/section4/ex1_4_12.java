import java.util.Arrays;
import edu.princeton.cs.algs4.Queue;

class Main {
    static int[] intersection(int[] array1, int[] array2) {
        // array1 and array2 are already sorted
        int i = 0;
        int j = 0;
        int lastInserted = 0;  // init value can be anything
        Queue<Integer> common = new Queue<>();
        while (i < array1.length && j < array2.length) {
            if (array1[i] < array2[j])
                i++;
            else if (array1[i] > array2[j])
                j++;
            else {
                if (i+j == 0 || array1[i] != lastInserted) {
                    common.enqueue(array1[i]);
                    lastInserted = array1[i];
                }
                i++;
                j++;
            }
        }
        int[] result = new int[common.size()];
        int k = 0;
        while (!common.isEmpty())
            result[k++] = common.dequeue();
        return result;
    }

    public static void main(String[] args) {
        int[][][] testCases = {
            {{}, {}},
            {{0, 1, 2, 3}, {4, 5, 6, 7}},
            {{0, 1, 2, 3}, {2, 3, 4, 5}},
            {{-7, -6, -5, -4}, {-7, -6, -5, -4}},
            {{0, 0, 0, 0}, {0, 0, 0, 0}}
        };
        int[][] expectedResult = {
            {},
            {},
            {2, 3},
            {-7, -6, -5, -4},
            {0}
        };
        for (int i = 0; i < testCases.length; i++) {
            Arrays.sort(testCases[i][0]);
            Arrays.sort(testCases[i][1]);
            int[] expected = expectedResult[i];
            int[] result = intersection(testCases[i][0], testCases[i][1]);
            assert expected.length == result.length;
            for (int j = 0; j < expected.length; j++)
                assert expected[j] == result[j]
                    : String.format("Case %d failed. Expected: %d, got: %d", i, expected[j], result[j]);
        }
    }
}
