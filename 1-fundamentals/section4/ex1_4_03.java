import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.ThreeSum;

class DoublingTest {
    public static double timeTrial(int n) {
        int MAX = 1000000;
        int[] a = new int[n];
        for (int i = 0; i < n; i++)
            a[i] = StdRandom.uniform(-MAX, MAX);
        Stopwatch timer = new Stopwatch();
        int count = ThreeSum.count(a);
        return timer.elapsedTime();
    }

    public static double[][] runTrials(int min_n, int max_n) {
        // first row contains the values for the X axis and second row values
        // for the Y axis
        Queue<Double> xValues = new Queue<>();
        Queue<Double> yValues = new Queue<>();
        for (int n = 250; n <= max_n; n += n) {
            xValues.enqueue((double) n);
            yValues.enqueue(DoublingTest.timeTrial(n));
        }
        double[][] values = new double[2][xValues.size()];
        int i = 0;
        while (!xValues.isEmpty() && !yValues.isEmpty()) {
            values[0][i] = xValues.dequeue();
            values[1][i] = yValues.dequeue();
            i++;
        }
        return values;
    }

    public static void plotStandard(int min_n, int max_n) {
        double[][] values = runTrials(min_n, max_n);
        Plot.plotValues(
            "Standard plot",
            "problem size N",
            "time (seconds)",
            values
        );
    }

    public static void plotLogLog(int min_n, int max_n) {
        double[][] values = runTrials(min_n, max_n);
        double[][] logValues = new double[values.length][values[0].length];
        for (int i = 0; i < logValues[0].length; i++) {
            logValues[0][i] = Math.log(values[0][i]);
            logValues[1][i] = 3*logValues[0][i] + 0.0;
        }
        Plot.plotValues(
            "Log-log plot",
            "problem size N",
            "time",
            logValues
        );
    }
}

class Plot {

    private static int DEFAULT_SIZE = 512;
    private static int COL_WIDTH = 16;
    private static int ROW_HEIGHT = 16;
    private static int ROWS = DEFAULT_SIZE/ROW_HEIGHT;
    private static int COLS = DEFAULT_SIZE/COL_WIDTH;
    private static int TOP_MARGIN = 4;      // in units of ROWS
    private static int BOTTOM_MARGIN = 4;   // in units of ROWS
    private static int LEFT_MARGIN = 6;     // in units of COLS
    private static int RIGHT_MARGIN = 4;    // in units of COLS
    private static double TICKS_END_SLACK = 0.05;
    private static double TICK_LENGTH = 0.02;
    private static double TICK_VALUE_PADDING = 0.02;
    private static double Y_TICK_VALUE_CORRECTION = 0.0075;

    public static void plotValues(
        String title,
        String xLabel,
        String yLabel,
        double[][] values
    ) {
        if (values[0].length < 2)
            throw new IllegalArgumentException("at least two points are needed");
        // Draw title
        StdDraw.textLeft(0, scaledRowPos(ROWS-1), title);
        // Draw axes' names
        StdDraw.text(scaledColPos(1), scaledRowPos(ROWS/2), yLabel, 90.0);
        StdDraw.text(scaledColPos(COLS/2), scaledRowPos(1), xLabel);
        // Draw axes
        double xOrigin = scaledColPos(LEFT_MARGIN);
        double yOrigin = scaledRowPos(BOTTOM_MARGIN);
        StdDraw.line(xOrigin, yOrigin, scaledColPos(COLS-RIGHT_MARGIN), yOrigin);
        StdDraw.line(xOrigin, yOrigin, xOrigin, scaledRowPos(ROWS-TOP_MARGIN));
        // Draw ticks
        double xTickStart = yOrigin - TICK_LENGTH;
        double yTickStart = xOrigin - TICK_LENGTH;
        double xTicksTotalLength = scaledColPos(COLS-RIGHT_MARGIN) - xOrigin - TICKS_END_SLACK;
        double yTicksTotalLength = scaledRowPos(ROWS-TOP_MARGIN) - yOrigin - TICKS_END_SLACK;
        double xTicksSpacing = xTicksTotalLength/values[0].length;
        double yTicksSpacing = yTicksTotalLength/values[0].length;
        // Compute scaling parameters
        double xMax = values[0][0];
        double yMax = values[1][0];
        for (int i = 1; i < values[0].length; i++) {
            if (values[0][i] > xMax) xMax = values[0][i];
            if (values[1][i] > yMax) yMax = values[1][i];
        }
        xMax *= 1.1;
        yMax *= 1.1;
        for (int i = 0; i <= values[0].length; i++) {
            double xTickPos = xOrigin+i*xTicksSpacing;
            double yTickPos = yOrigin+i*yTicksSpacing;
            StdDraw.line(xTickPos, xTickStart, xTickPos, yOrigin);
            StdDraw.line(yTickStart, yTickPos, xOrigin, yTickPos);
            if (i > 0) {
                String xTickValue = String.format("%.0f", i*xMax/values[0].length);
                String yTickValue = String.format("%.2f", i*yMax/values[0].length);
                StdDraw.text(xTickPos, xTickStart - TICK_VALUE_PADDING, xTickValue);
                StdDraw.textRight(yTickStart - TICK_VALUE_PADDING/2, yTickPos - Y_TICK_VALUE_CORRECTION, yTickValue);
            }
        }
        // Draw points
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        for (int i = 0; i < values[0].length; i++) {
            double xPos = xOrigin + xTicksTotalLength * values[0][i]/xMax;
            double yPos = yOrigin + yTicksTotalLength * values[1][i]/yMax;
            StdDraw.point(xPos, yPos);
        }
    }

    private static double scaledRowPos(int n) {
        return 1.0*n*ROW_HEIGHT/DEFAULT_SIZE;
    }

    private static double scaledColPos(int n) {
        return 1.0*n*COL_WIDTH/DEFAULT_SIZE;
    }
}

class Main {
    public static void main(String[] args) {
        DoublingTest.plotStandard(250, 2000);
        // DoublingTest.plotLogLog(250, 2000);
    }
}
