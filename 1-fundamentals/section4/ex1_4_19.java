/* TODO: add explanation
 *
 * 2N + 2(N/2) + 2(N/4) + 2(N/8) + ... = 2N (1 + 1/2 + 1/4 + 1/8 + ...)    geometric series
 *                                     = 2N * 2
 *                                     = 4N
 *                                     ~ N
 * */

import java.util.Arrays;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdRandom;

class LocalMinimum {

    private static double INF = Double.POSITIVE_INFINITY;

    private static int argmin(double[] a) {
        // Find the position of the lowest value
        if (a.length == 0)
            return -1;
        double currentMin = a[0];
        int currentMinPos = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] < currentMin) {
                currentMin = a[i];
                currentMinPos = i;
            }
        }
        return currentMinPos;
    }

    private static boolean isLocalMinimum(int[][] matrix, int i, int j) {
        // matrix is square
        int n = matrix.length;
        double center = matrix[i][j];
        double up    = i - 1 >= 0 ? matrix[i-1][j] : INF;
        double down  = i + 1 <  n ? matrix[i+1][j] : INF;
        double left  = j - 1 >= 0 ? matrix[i][j-1] : INF;
        double right = j + 1 <  n ? matrix[i][j+1] : INF;
        return argmin(new double[] {center, up, down, left, right}) == 0;
    }

    private static int argminAlongAxis(int[][] matrix, char dimension, int index, int start, int end) {
        if (start > end)
            throw new IllegalArgumentException("start cannot be greater than end");
        if (dimension == 'r') {
            int currentMin = matrix[index][start];
            int currentMinPos = start;
            for (int i = start + 1; i < end; i++) {
                if (matrix[index][i] < currentMin) {
                    currentMin = matrix[index][i];
                    currentMinPos = i;
                }
            }
            return currentMinPos;
        } else if (dimension == 'c') {
            int currentMin = matrix[start][index];
            int currentMinPos = start;
            for (int i = start + 1; i < end; i++) {
                if (matrix[i][index] < currentMin) {
                    currentMin = matrix[i][index];
                    currentMinPos = i;
                }
            }
            return currentMinPos;
        }
        throw new IllegalArgumentException("dimension can be either 'r' or 'c'");
    }

    private static class BestMin {
        int row = 0;
        int col = 0;
        double value = INF;
    }

    static int[] find(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++)
            if (matrix[i].length != matrix.length)
                throw new IllegalArgumentException("matrix must be square");
        if (matrix.length == 0)
            return new int[] {-1, -1};
        if (matrix.length == 1)
            return new int[] {0, 0};
        int n = matrix.length;
        int rowLow = 0;
        int rowHigh = n - 1;
        int colLow = 0;
        int colHigh = n - 1;
        BestMin bestMinSoFar = new BestMin();
        while (rowHigh >= rowLow && colHigh >= colLow) {
            int rowMid = rowLow + (rowHigh-rowLow)/2;
            int colMid = colLow + (colHigh-colLow)/2;
            int minRowPos = argminAlongAxis(matrix, 'r', rowMid, colLow, colHigh+1);
            int minColPos = argminAlongAxis(matrix, 'c', colMid, rowLow, rowHigh+1);
            int minRow = matrix[rowMid][minRowPos];
            int minCol = matrix[minColPos][colMid];
            if (minRow < minCol) {
                if (isLocalMinimum(matrix, rowMid, minRowPos))
                    return new int[] {rowMid, minRowPos};
                double up    = rowMid - 1 >= 0 ? matrix[rowMid-1][minRowPos] : INF;
                double down  = rowMid + 1 <  n ? matrix[rowMid+1][minRowPos] : INF;
                if (up < down) {
                    if (up < bestMinSoFar.value) {
                        bestMinSoFar.row = rowMid-1;
                        bestMinSoFar.col = minRowPos;
                        bestMinSoFar.value = up;
                    }
                } else {
                    if (down < bestMinSoFar.value) {
                        bestMinSoFar.row = rowMid+1;
                        bestMinSoFar.col = minRowPos;
                        bestMinSoFar.value = down;
                    }
                }
            } else {
                if (isLocalMinimum(matrix, minColPos, colMid))
                    return new int[] {minColPos, colMid};
                double left  = colMid - 1 >= 0 ? matrix[minColPos][colMid-1] : INF;
                double right = colMid + 1 <  n ? matrix[minColPos][colMid+1] : INF;
                if (left < right) {
                    if (left < bestMinSoFar.value) {
                        bestMinSoFar.col = colMid-1;
                        bestMinSoFar.row = minColPos;
                        bestMinSoFar.value = left;
                    }
                } else {
                    if (right < bestMinSoFar.value) {
                        bestMinSoFar.col = colMid+1;
                        bestMinSoFar.row = minColPos;
                        bestMinSoFar.value = right;
                    }
                }
            }
            if (bestMinSoFar.col < colMid)
                colHigh = colMid - 1;
            else
                colLow = colMid + 1;
            if (bestMinSoFar.row < rowMid)
                rowHigh = rowMid - 1;
            else
                rowLow = rowMid + 1;
        }
        return new int[] {bestMinSoFar.row, bestMinSoFar.col};
    }

    static int[][] findAll(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++)
            if (matrix[i].length != matrix.length)
                throw new IllegalArgumentException("matrix must be square");
        if (matrix.length == 0)
            return new int[][] {{-1, -1}};
        if (matrix.length == 1)
            return new int[][] {{0, 0}};
        double INF = Double.POSITIVE_INFINITY;
        int n = matrix.length;
        Queue<int[]> minima = new Queue<>();
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (isLocalMinimum(matrix, i, j))
                    minima.enqueue(new int[] {i, j});
        int[][] minimaArray = new int[minima.size()][2];
        int i = 0;
        while (!minima.isEmpty())
            minimaArray[i++] = minima.dequeue();
        return minimaArray;
    }
}

class Permutation {
    static int[][] nonNegIntegers(int start, int end) {
        if (start > end)
            throw new IllegalArgumentException("end must be greater than start");
        int[] array = new int[end - start];
        int j = 0;
        for (int i = start; i < end; i++)
            array[j++] = i;
        return permute(array);
    }

    static int[][] permute(int[] array) {
        if (array.length == 0)
            return new int[0][0];
        if (array.length == 1)
            return new int[][] {{array[0]}};
        if (array.length == 2)
            return new int[][] {{array[0], array[1]}, {array[1], array[0]}};
        Queue<int[]> perms = new Queue<>();
        for (int i = 0; i < array.length; i++) {
            int[] rest = circularCopy(array, i + 1, array.length - 1);
            for (int[] subPerm : permute(rest))
                perms.enqueue(concatArrays(new int[] {array[i]}, subPerm));
        }
        int[][] result = new int[perms.size()][];
        int i = 0;
        while (!perms.isEmpty())
            result[i++] = perms.dequeue();
        return result;
    }

    private static int[] circularCopy(int[] array, int start, int length) {
        int[] copy = new int[length];
        for (int i = 0; i < length; i++)
            copy[i] = array[(start+i) % array.length];
        return copy;
    }

    private static int[] concatArrays(int[] a, int[] b) {
        int[] result = new int[a.length + b.length];
        int i = 0;
        for (int j = 0; j < a.length; j++)
            result[i++] = a[j];
        for (int j = 0; j < b.length; j++)
            result[i++] = b[j];
        return result;
    }
}

class Main {
    static void test() {
        int[][][] testCases = {
            {},
            {{10}},
            {
                {3, 2},
                {4, 1}
            },
            {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            },
            {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3}
            },
            {
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1}
            },
            {
                {3, 6, 9},
                {2, 5, 8},
                {1, 4, 7}
            },
            {
                {99, 73, 60, 11},
                {81, 89, 82, 61},
                {97, 80, 79, 86},
                {10, 96, 75, 98}
            },
            {
                {1 ,  2,  3,  4,  5},
                {6 ,  7,  8,  9, 10},
                {11, 12,  0, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25},
            }
        };
        int[][] expectedResults = {
            {-1, -1},
            {0, 0},
            {1, 1},
            {0, 0},
            {0, 2},
            {2, 2},
            {2, 0},
            {0, 3},
            {2, 2}
        };
        for (int i = 0; i < testCases.length; i++) {
            // int[] result = localMinimum(testCases[i]);
            int[] result = LocalMinimum.find(testCases[i]);
            int[] expected = expectedResults[i];
            assert Arrays.equals(expected, result)
                : String.format(
                    "Case %d. Expected: (%d, %d). Got: (%d, %d)",
                    i, expected[0], expected[1],
                    result[0], result[1]
                );
        }
    }

    static void testRandom() {
        int trials = 2000000;
        int n = 30;
        for (int i = 0; i < trials; i++) {
            int[] a = new int[n*n];
            for (int j = 0; j < n*n; j++)
                a[j] = j;
            StdRandom.shuffle(a);
            int[][] matrix = new int[n][n];
            for (int j = 0; j < a.length; j++)
                matrix[j/n][j%n] = a[j];
            int[] result = LocalMinimum.find(matrix);
            int[][] allMinima = LocalMinimum.findAll(matrix);
            boolean found = false;
            for (int[] minimum : allMinima) {
                if (Arrays.equals(minimum, result)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                System.out.println("\nAll minima");
                for (int[] minimum : allMinima)
                    System.out.println(minimum[0] + " " + minimum[1]);
                System.out.printf("Result: (%d, %d)\n", result[0], result[1]);
                for (int[] row : matrix) {
                    for (int j = 0; j < row.length; j++)
                        System.out.printf("%3d ", row[j]);
                    System.out.println();
                }
                assert found;
            }
        }
    }

    static void testPermutations() {
        int n = 3;
        int[][] permutations = Permutation.nonNegIntegers(0, n*n);
        for (int[] permutation : permutations) {
            int[][] matrix = new int[n][n];
            for (int i = 0; i < permutation.length; i++)
                matrix[i/n][i%n] = permutation[i];
            int[] result = LocalMinimum.find(matrix);
            int[][] allMinima = LocalMinimum.findAll(matrix);
            boolean found = false;
            for (int[] minimum : allMinima) {
                if (Arrays.equals(minimum, result)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                System.out.println("\nAll minima");
                for (int[] minimum : allMinima)
                    System.out.println(minimum[0] + " " + minimum[1]);
                System.out.printf("Result: (%d, %d)\n", result[0], result[1]);
                for (int[] row : matrix) {
                    for (int j = 0; j < row.length; j++)
                        System.out.printf("%3d ", row[j]);
                    System.out.println();
                }
                assert found;
            }
        }
    }

    public static void main(String[] args) {
        test();
        testPermutations();
        testRandom();
    }
}
