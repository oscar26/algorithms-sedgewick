import java.util.Arrays;
import edu.princeton.cs.algs4.In;

class BinarySearch {
    // Reimplement using long to mitigate overflow
    public static int indexOf(long[] a, long key) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int mid = low + (high-low)/2;
            if (key < a[mid])
                high = mid - 1;
            else if (key > a[mid])
                low = mid + 1;
            else
                return mid;
        }
        return -1;
    }
}

class FourSum {
    public static int count(int[] a) {
        int n = a.length;
        int total = 0;
        // casting to long to mitigate overflow or use BigInteger to avoid
        // overflow
        long[] values = new long[n];
        for (int i = 0; i < n; i++)
            values[i] = a[i];
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    int l = BinarySearch.indexOf(values, -(values[i]+values[j]+values[k]));
                    if (l > k)
                        total++;
                }
            }
        }
        return total;
    }

    public static int countBruteForce(int[] a) {
        int n = a.length;
        int total = 0;
        // casting to long to mitigate overflow or use BigInteger to avoid
        // overflow
        long[] values = new long[n];
        for (int i = 0; i < n; i++)
            values[i] = a[i];
        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                for (int k = j + 1; k < n; k++)
                    for (int l = k + 1; l < n; l++)
                        if (values[i] + values[j] + values[k] + values[l] == 0)
                            total++;
        return total;
    }
}

class Main {
    public static void main(String[] args) {
        int[][] testCases = {
            {},
            {-6, 1, 2, 3, 4},
            {-9, -8, -7, -6, 1, 2, 3, 4}
        };
        for (int i = 0; i < testCases.length; i++) {
            Arrays.sort(testCases[i]);
            int result = FourSum.count(testCases[i]);
            int expected = FourSum.countBruteForce(testCases[i]);
            assert expected == result : String.format("expected %d, got %d", expected, result);
        }
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the file containing the numbers");
        int[] a = In.readInts(args[0]);
        Arrays.sort(a);
        System.out.println(FourSum.count(a));
        System.out.println(FourSum.countBruteForce(a));
    }
}
