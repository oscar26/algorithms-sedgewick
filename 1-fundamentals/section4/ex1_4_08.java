import java.util.Arrays;
import edu.princeton.cs.algs4.BinarySearch;

class Main {
    static int countPairs(int[] values) {
        Arrays.sort(values);
        int count = 0;
        for (int i = 0; i < values.length; i++) {
            int position = BinarySearch.indexOf(values, values[i]);
            if (position != -1 && position != i)
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int[][] testCases = {
            {},
            {0, 1, 2, 3, 4},
            {1, 1, 0, 3, 4},
            {1, 1, 1, 3, 4},
            {1, 2, 0, 2, 1, 0}
        };
        int[] expectedResult = {
            0,
            0,
            1,
            2,
            3
        };
        for (int i = 0; i < testCases.length; i++)
            assert expectedResult[i] == countPairs(testCases[i]);
    }
}
