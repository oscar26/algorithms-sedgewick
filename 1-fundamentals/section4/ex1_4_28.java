import edu.princeton.cs.algs4.Queue;

class OneQueueStack<T> {

    private Queue<T> queue;

    public OneQueueStack() {
        queue = new Queue<>();
    }

    public int size() {
        return queue.size();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void push(T item) {
        queue.enqueue(item);
    }

    public T pop() {
        if (isEmpty())
            throw new RuntimeException("stack is empty");
        for (int i = 0; i < size()-1; i++)
            queue.enqueue(queue.dequeue());
        return queue.dequeue();
    }

}

class Main {
    public static void main(String[] args) {
        OneQueueStack<Integer> stack = new OneQueueStack<>();
        assert stack.isEmpty();
        stack.push(1);
        assert !stack.isEmpty();
        assert stack.size() == 1;
        assert stack.pop() == 1;
        assert stack.isEmpty();
        for (int i = 0; i < 10; i++)
            stack.push(i);
        assert !stack.isEmpty();
        assert stack.size() == 10;
        for (int i = 9; i >= 0; i--)
            assert stack.pop() == i;
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        assert stack.pop() == 5;
        assert stack.pop() == 4;
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        assert stack.pop() == 9;
        assert stack.pop() == 8;
        assert stack.pop() == 7;
        assert stack.pop() == 6;
        stack.push(10);
        assert stack.pop() == 10;
        assert stack.pop() == 3;
        assert stack.pop() == 2;
        assert stack.pop() == 1;
        assert stack.pop() == 0;
        assert stack.isEmpty();
    }
}
