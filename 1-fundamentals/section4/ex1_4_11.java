import java.util.Arrays;
class StaticSetOfInts {
    private int[] a;

    public StaticSetOfInts(int[] keys) {
        a = new int[keys.length];
        for (int i = 0; i < keys.length; i++)
            a[i] = keys[i];
        Arrays.sort(a);
    }

    public boolean contains(int key) {
        return indexOf(key) != -1;
    }

    private int indexOf(int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi-lo)/2;
            if (key > a[mid])
                lo = mid + 1;
            else if (key < a[mid])
                hi = mid - 1;
            else
                return mid;
        }
        return -1;
    }

    public int howMany(int key) {
        int leftIndex = leftIndexOf(key);
        int rightIndex = rightIndexOf(key);
        if (leftIndex == -1) {
            assert rightIndex == -1;
            return 0;
        }
        return rightIndex - leftIndex + 1;
    }

    private int leftIndexOf(int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi-lo)/2;
            if (key > a[mid])
                lo = mid + 1;
            else if (key < a[mid])
                hi = mid - 1;
            else {
                if (mid == 0)
                    return mid;
                if (a[mid-1] == key)
                    hi = mid - 1;
                else
                    return mid;
            }
        }
        return -1;
    }

    private int rightIndexOf(int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi-lo)/2;
            if (key > a[mid])
                lo = mid + 1;
            else if (key < a[mid])
                hi = mid - 1;
            else {
                if (mid == a.length - 1)
                    return mid;
                if (a[mid+1] == key)
                    lo = mid + 1;
                else
                    return mid;
            }
        }
        return -1;
    }
}

class Main {
    public static void main(String[] args) {
        int[][] testCases = {
            {},
            {0, 1, 2, 3, 4},
            {0, 1, 1, 3, 4},
            {1, 1, 1, 3, 4},
            {0, 0, 1, 1, 2, 2},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1}
        };
        int[] keys = {
            1,
            1,
            1,
            1,
            2,
            1,
            2
        };
        int[] expectedResult = {
            0,
            1,
            2,
            3,
            2,
            8,
            0
        };
        for (int i = 0; i < testCases.length; i++) {
            int expected = expectedResult[i];
            int result = (new StaticSetOfInts(testCases[i])).howMany(keys[i]);
            assert expected == result
                : String.format("Case %d failed. Expected: %d, got: %d", i, expected, result);
        }
    }
}
