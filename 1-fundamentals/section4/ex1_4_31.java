import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.Stack;

class ThreeStackDeque<T> {

    private Stack<T> stack1;
    private Stack<T> stack2;
    private Stack<T> stack3;

    public ThreeStackDeque() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
        stack3 = new Stack<>();
    }

    public boolean isEmpty() {
        return stack1.isEmpty() && stack2.isEmpty() && stack3.isEmpty();
    }

    public int size() {
        return stack1.size() + stack2.size() + stack3.size();
    }

    public void pushLeft(T item) {
        stack1.push(item);
    }

    public void pushRight(T item) {
        stack3.push(item);
    }

    public T popLeft() {
        if (isEmpty())
            throw new NoSuchElementException("Deque is empty");
        if (stack1.isEmpty()) {
            int originalSize = stack3.size();
            for (int i = 0; i < originalSize/2; i++)
                stack2.push(stack3.pop());
            while (!stack3.isEmpty())
                stack1.push(stack3.pop());
            while (!stack2.isEmpty())
                stack3.push(stack2.pop());
        }
        return stack1.pop();
    }

    public T popRight() {
        if (isEmpty())
            throw new NoSuchElementException("Deque is empty");
        if (stack3.isEmpty()) {
            int originalSize = stack1.size();
            for (int i = 0; i < originalSize/2; i++)
                stack2.push(stack1.pop());
            while (!stack1.isEmpty())
                stack3.push(stack1.pop());
            while (!stack2.isEmpty())
                stack1.push(stack2.pop());
        }
        return stack3.pop();
    }

}

class Main {

    static void testPushLeft(ThreeStackDeque<Integer> deque) {
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushLeft(4);
        assert deque.size() == 1;
        assert !deque.isEmpty();

        for (int i = 3; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
    }

    static void testPushRight(ThreeStackDeque<Integer> deque) {
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushRight(4);
        assert deque.size() == 1;
        assert !deque.isEmpty();

        for (int i = 3; i >= 0; i--)
            deque.pushRight(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
    }

    static void testPopLeft(ThreeStackDeque<Integer> deque) {
        assert deque.size() == 0;
        boolean exceptionThrown = false;
        try {
            deque.popLeft();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
        for (int i = 0; i < 5; i++) {
            int item = deque.popLeft();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushRight(1);
        assert deque.popLeft() == 1;
        assert deque.size() == 0;
        assert deque.isEmpty();
    }

    static void testPopRight(ThreeStackDeque<Integer> deque) {
        assert deque.size() == 0;
        boolean exceptionThrown = false;
        try {
            deque.popRight();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
        for (int i = 4; i >= 0; i--) {
            int item = deque.popRight();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushLeft(1);
        assert deque.popRight() == 1;
        assert deque.size() == 0;
        assert deque.isEmpty();
    }

    public static void main(String[] args) {
        testPushLeft(new ThreeStackDeque<Integer>());
        testPushRight(new ThreeStackDeque<Integer>());
        testPopLeft(new ThreeStackDeque<Integer>());
        testPopRight(new ThreeStackDeque<Integer>());
    }
}
