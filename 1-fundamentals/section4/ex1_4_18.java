/*
 * The local minimum definition seems to imply that we need to consider triples
 * only, but this would render the suggested solution confusing as its key
 * assumption is that either one of the numbers at the extremes is a local
 * minimum, serving as a stopping condition in case no other local minimum is
 * found during the search.
 *
 * The solution for this exercise (and the next) assumes the aforementioned
 * property.
 *
 *
 * Explanation
 * -----------
 *
 * Let N be the length of the array. We start by checking if the element at N/2
 * is a local minimum (if it's less than its neighbours). If it is, we stop.
 * Otherwise, determine the smaller neighbour and continue searching in that
 * half recursively.
 *
 * The key insight in the recursive step is to realize that by choosing the
 * side corresponding to the smaller neighbour, we are selecting the side that
 * has at least one local minimum. That is because if the remaining half is
 * monotonically increasing/decreasing, the value at the extreme would be a
 * local minimum.  But if it is not monotonic, then at some point there is an
 * element that has two neighbours that are greater than itself.
 *
 * To illustrate better, look at the following figures.
 *
 *     *                           *
 *       *                           *
 *         *                           *       *
 *           *                           *   * ^
 *             *                           *   |
 *               *                             |
 *                 *                       last element
 *                 ^
 *             last element
 *
 * Asterisks denote any distinct number and their differences in height denote
 * their orders: if one asterisk is below the previous one, then it is smaller,
 * otherwise it is greater.
 *
 * Left figure shows a monotonically decreasing sequence in which the rightmost
 * element is a local minimum. The right figure is not monotonic and, as such,
 * there must be an element that breaks the monotonicity and that element is
 * smaller than its neighbours.
 *
 * */

import java.util.Arrays;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdRandom;

class Main {
    static int localMinimum(int a[]) {
        if (a.length == 0)
            return -1;
        if (a.length == 1)
            return 0;
        int low = 0;
        int high = a.length-1;
        while (high - low > 1) {
            int mid = low + (high-low)/2;
            if (a[mid] < a[mid-1] && a[mid] < a[mid+1])
                return mid;
            if (a[mid-1] < a[mid+1])
                high = mid - 1;
            else
                low = mid + 1;
        }
        // Either one of the extremes has been reached
        if (low == 0) {
            if (a[low] < a[low+1])
                return low;
            return low+1;
        }
        if (a[high] < a[high-1])
            return high;
        return high-1;
    }

    static int[] findAllLocalMinima(int a[]) {
        if (a.length == 0)
            return new int[0];
        if (a.length == 1)
            return new int[] {0};
        Queue<Integer> minima = new Queue<>();
        if (a[0] < a[1])
            minima.enqueue(0);
        for (int i = 1; i < a.length-1; i++)
            if (a[i] < a[i-1] && a[i] < a[i+1])
                minima.enqueue(i);
        if (a.length > 2 && a[a.length-1] < a[a.length-2])
            minima.enqueue(a.length-1);
        int[] minimaArray = new int[minima.size()];
        int i = 0;
        while (!minima.isEmpty())
            minimaArray[i++] = minima.dequeue();
        return minimaArray;

    }

    static void test() {
        int[][] testCases = {
            {0, 1, 2, 3, 4, 5, 6, 100, 8, 9},
            {10, 11, 12, 13, 14, 15, 16, 17},
            {10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
            {10, 11, 12, 13, 14, 9, 8, 7, 6},
            {19, 18, 17, 16, 20, 21, 22, 23}
        };
        int[] expectedResults = {
            0,
            0,
            9,
            8,
            3
        };
        for (int i = 0; i < testCases.length; i++)
            assert localMinimum(testCases[i]) == expectedResults[i] : String.format("case %d failed", i);
    }

    static void testRandom() {
        int trials = 1000;
        int n = 1000;
        for (int i = 0; i < trials; i++) {
            int[] a = new int[n];
            for (int j = 0; j < n; j++)
                a[j] = j;
            StdRandom.shuffle(a);
            int minimum = localMinimum(a);
            int[] allMinima = findAllLocalMinima(a);
            assert Arrays.binarySearch(allMinima, minimum) != -1;
        }
    }

    public static void main(String[] args) {
        test();
        testRandom();
    }
}
