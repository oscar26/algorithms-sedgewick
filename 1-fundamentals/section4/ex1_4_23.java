/*
 * This seems a problem similar to the Stern-Brocot tree [1] but the exercise
 * isn't more specific about the rational to find as there are an infinite
 * amount of rationals less than N for an arbitrary N > 0.
 *
 * [1] https://en.wikipedia.org/wiki/Stern%E2%80%93Brocot_tree
 *
 * */

class Main {
    public static void main(String[] args) {}
}
