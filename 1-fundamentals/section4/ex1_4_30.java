import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.Stack;
import stdlib.Steque;

class StackStequeDeque<T> {

    private Stack<T> stack;
    private Steque<T> steque;

    public StackStequeDeque() {
        stack = new Stack<>();
        steque = new Steque<>();
    }

    public boolean isEmpty() {
        return stack.isEmpty() && steque.isEmpty();
    }

    public int size() {
        return stack.size() + steque.size();
    }

    public void pushLeft(T item) {
        while (!stack.isEmpty())
            steque.push(stack.pop());
        steque.push(item);
    }

    public void pushRight(T item) {
        while (!stack.isEmpty())
            steque.push(stack.pop());
        steque.enqueue(item);
    }

    public T popLeft() {
        if (isEmpty())
            throw new NoSuchElementException("StackStequeDeque is empty");
        while (!stack.isEmpty())
            steque.push(stack.pop());
        return steque.pop();
    }

    public T popRight() {
        if (isEmpty())
            throw new NoSuchElementException("StackStequeDeque is empty");
        while (!steque.isEmpty())
            stack.push(steque.pop());
        return stack.pop();
    }

}

class Main {

    static void testPushLeft(StackStequeDeque<Integer> deque) {
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushLeft(4);
        assert deque.size() == 1;
        assert !deque.isEmpty();

        for (int i = 3; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
    }

    static void testPushRight(StackStequeDeque<Integer> deque) {
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushRight(4);
        assert deque.size() == 1;
        assert !deque.isEmpty();

        for (int i = 3; i >= 0; i--)
            deque.pushRight(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
    }

    static void testPopLeft(StackStequeDeque<Integer> deque) {
        assert deque.size() == 0;
        boolean exceptionThrown = false;
        try {
            deque.popLeft();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
        for (int i = 0; i < 5; i++) {
            int item = deque.popLeft();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushRight(1);
        assert deque.popLeft() == 1;
        assert deque.size() == 0;
        assert deque.isEmpty();
    }

    static void testPopRight(StackStequeDeque<Integer> deque) {
        assert deque.size() == 0;
        boolean exceptionThrown = false;
        try {
            deque.popRight();
        } catch (NoSuchElementException e) {
            exceptionThrown = true;
        }
        assert exceptionThrown;

        for (int i = 4; i >= 0; i--)
            deque.pushLeft(i);
        assert deque.size() == 5;
        assert !deque.isEmpty();
        for (int i = 4; i >= 0; i--) {
            int item = deque.popRight();
            assert item == i : String.format("Expected: %d, got: %d", i, item);
        }
        assert deque.size() == 0;
        assert deque.isEmpty();

        deque.pushLeft(1);
        assert deque.popRight() == 1;
        assert deque.size() == 0;
        assert deque.isEmpty();
    }

    public static void main(String[] args) {
        testPushLeft(new StackStequeDeque<Integer>());
        testPushRight(new StackStequeDeque<Integer>());
        testPopLeft(new StackStequeDeque<Integer>());
        testPopRight(new StackStequeDeque<Integer>());
    }
}
