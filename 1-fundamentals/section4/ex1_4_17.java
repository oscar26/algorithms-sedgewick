import java.util.Arrays;

class Main {
    static double[] farthestPair(double[] a) {
        if (a.length < 2)
            throw new IllegalArgumentException("at least two elements are required");
        // first element is the min and the second is the max
        double[] pair = {a[0], a[0]};
        for (int i = 1; i < a.length; i++) {
            if (a[i] < pair[0])
                pair[0] = a[i];
            if (a[i] > pair[1])
                pair[1] = a[i];
        }
        return pair;
    }

    public static void main(String[] args) {
        double[][] testCases = {
            {1.5, 2.0},
            {-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0},
            {1.0, 2.0, 3.0, 100.0}
        };
        double[][] expectedPairs = {
            {1.5, 2.0},
            {-3.0, 3.0},
            {1.0, 100.0}
        };
        for (int i = 0; i < testCases.length; i++) {
            double[] expected = expectedPairs[i];
            double[] result = farthestPair(testCases[i]);
            assert expected[0] == result[0]
                : String.format("case %d failed. Expected %.5f, got %.5f", i, expected[0], result[0]);
            assert expected[1] == result[1]
                : String.format("case %d failed. Expected %.5f, got %.5f", i, expected[1], result[1]);
        }
    }
}
