import edu.princeton.cs.algs4.In;

class ThreeSum {
    public static int count(int[] a) {
        int n = a.length;
        int total = 0;
        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                for (int k = j + 1; k < n; k++)
                    if (a[i] + a[j] + a[k] == 0)
                        total++;
        return total;
    }

    public static int countLessOverflow(int[] a) {
        int n = a.length;
        int total = 0;
        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                for (int k = j + 1; k < n; k++)
                    // Or use BigInteger
                    if ((long) a[i] + (long) a[j] == -a[k] )
                        total++;
        return total;
    }
}

class Main {
    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Pass the file containing the numbers");
        int[] a = In.readInts(args[0]);
        System.out.println(ThreeSum.count(a));
        System.out.println(ThreeSum.countLessOverflow(a));
    }
}
