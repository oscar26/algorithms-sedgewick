# Algorithms by Robert Sedgewick and Kevin Wayne

Repository for storing solutions to the exercises and projects in the book and
additional material found in the [booksite](https://algs4.cs.princeton.edu).

## Provided library and test data

The library provided by the book authors is small enough to include in this
repository unlike the test data files, which can be downloaded from
[here](https://algs4.cs.princeton.edu/code/).
